require("fbk.snippets")
local function map(m, k, v)
  vim.keymap.set(m, k, v, { silent = true })
end

local o = vim.opt
--fix indenting with F6
local function indent()
  vim.g.tabstop = 2
  vim.g.shiftwidth = 2
  local line = vim.api.nvim_win_get_cursor(0)
  vim.cmd([[
  normal gg=G
  :%s/\s\+$//e
  ]])
  vim.api.nvim_win_set_cursor(0, line)
end
vim.keymap.set("n", "<F6>", indent)

--fix indenting HTML with F7
local function indenthtml()
  vim.g.tabstop = 2
  vim.g.shiftwidth = 2
  local line = vim.api.nvim_win_get_cursor(0)
  vim.cmd([[
  set filetype=css
  normal gg=G
  set filetype=html
  normal gg=G
  set filetype=css
  normal gg=G
  set filetype=html
  :%s/\s\+$//e
  ]])
  vim.api.nvim_win_set_cursor(0, line)
end
vim.keymap.set("n", "<F7>", indenthtml)

--remember the last line you where on in a while at open
vim.cmd([[
if has("autocmd")
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
]])

-- vim.cmd([[ au BufEnter,BufNew *.php :set filetype=html ]])

--quit commands
vim.cmd([[
command! -bar -bang Q quit<bang>
command! Wq :wq
command! W :w
command! Q :q
command! Q1 :q
:cmap Q! q!
:cmap q1 q!
iab Wq <ESC>i<DEL><ESC>:wq<CR>
iab wq <ESC>i<DEL><ESC>:wq<CR>
]])

--Make sure markup code tags are visiable
vim.cmd([[set conceallevel=0]])

--type file name
vim.keymap.set("i", "<C-F>", "<C-X><C-F>")

-- replace in quotes
vim.keymap.set("n", "cq", 'ci"')

--Spell Check
vim.keymap.set("n", "<F9>", ":setlocal spell! spelllang=en_us<CR>")

--load templates
vim.keymap.set("n", "<leader>html", ":-1read $HOME/.config/nvim/lua/fbk/templates/index.html<CR>4j2f<i")
vim.keymap.set("n", "<leader>bash", ":-1read $HOME/.config/nvim/lua/fbk/templates/bash_header.sh<CR>Gi")
vim.keymap.set("n", "<leader>php", ":-1read $HOME/.config/nvim/lua/fbk/templates/php_header.php<CR>Gi")
--load default templates on new files
vim.cmd([[autocmd BufNewFile  *.html 0r $HOME/.config/nvim/lua/fbk/templates/index.html|normal 4j2f<]])
vim.cmd([[autocmd BufNewFile  *.sh 0r $HOME/.config/nvim/lua/fbk/templates/bash_header.sh|normal G]])
vim.cmd([[autocmd BufNewFile  *.php 0r $HOME/.config/nvim/lua/fbk/templates/php_header.php|normal Gkk]])
vim.cmd([[autocmd BufNewFile  zscript.zsc 0r $HOME/.config/nvim/lua/fbk/templates/zscript.zsc|normal Gk2f"]])

--show relative line number
o.relativenumber = true
vim.cmd([[set mouse=]])

o.expandtab = true
o.tabstop = 2
o.shiftwidth = 2
o.hlsearch = false
o.incsearch = true
o.scrolloff = 10

--move selected lines up and down when in Visual Mode
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv", { desc = "Move Selected Lines Down" })
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv", { desc = "Move Selected Lines Down" })

--keep cursor centered while searching
vim.keymap.set("n", "n", "nzzzv", { desc = "keep cursor centered while searching" })
vim.keymap.set("n", "N", "Nzzzv", { desc = "keep cursor centered while searching" })

--yank/copy into system clipboard
-- vim.keymap.set("v", "<leader>y", '"+y', { desc = "yank/copy into system clipboard" })
-- vim.keymap.set("v", "<leader>x", '"+x', { desc = "yank/copy into system clipboard" })
-- vim.keymap.set("n", "<leader>x", '"+x', { desc = "yank/copy into system clipboard" })
-- vim.cmd([[
-- set clipboard=
-- nnoremap c "_c
-- nnoremap C "_C
-- ]])

--replace every instance of current word
-- map("n", "<leader>sf", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "<leader>sf", [[:%s/<C-r><C-w>/]], { desc = "Find and Replace Current Word" })
vim.keymap.set("n", "<leader>sr", [[:%s/]], { desc = "Find and Replace Current Word" })

--format current json file
vim.keymap.set("n", "<leader>jq", "<cmd>%!jq .<CR>", { desc = "Format JSON" })

--make current file executable
vim.keymap.set("n", "<leader>e", "<cmd>!chmod +x %<CR>", { desc = "Make Current File Executeable" })
-- map("n", "<leader>r", "<cmd>!chmod +x %<cr>:w<cr>:!tmux split-window %:h%<cr>")
vim.keymap.set(
  "n",
  "<leader>rr",
  "<cmd>!chmod +x %<cr>:w<cr>:!tmux split-window '%:p;bash -c read'<cr>",
  { desc = "Execute Current File" }
)

--vim.keymap.set("n", "<leader>ff", ":lua require('telescope.builtin').find_files()<CR>")

--swicth buffers using > and <
vim.keymap.set("n", ">", ":bnext<CR>", { desc = "Next Buffer" })
vim.keymap.set("n", "<", ":bprev<CR>", { desc = "Prev Buffer" })

--paste without losing buffers
vim.keymap.set("x", "<leader>p", '"_dP')

vim.keymap.set("n", "<leader>?", require("telescope.builtin").oldfiles, { desc = "[?] Find recently opened files" })
vim.keymap.set("n", "<leader>ff", require("telescope.builtin").find_files, { desc = "[?] Find recently opened files" })
vim.keymap.set("n", "<leader><space>", require("telescope.builtin").buffers, { desc = "[ ] Find existing buffers" })

--surround selected text
vim.api.nvim_buf_set_keymap(0, "v", ",html", [[c<html><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",div", [[c<div><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",pre", [[c<pre><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",body", [[c<body><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",ul", [[c<ul><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",p", [[c<p><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",head", [[c<head><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",title", [[c<title><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",h1", [[c<h1><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",h2", [[c<h2><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",h3", [[c<h3><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",h4", [[c<h4><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",h5", [[c<h5><c-r>"<esc>]], { noremap = false, silent = true })
vim.api.nvim_buf_set_keymap(0, "v", ",h6", [[c<h6><c-r>"<esc>]], { noremap = false, silent = true })

-- Doom GZDoom slade syntax highlighting for zscript
--vim.opt.runtimepath = vim.opt.runtimepath:prepend("~/.config/nvim/lua/fbk/syntax")
--local syntax_folder = "~/.config/nvim/lua/fbk/syntax"
vim.filetype.add({ extension = { zsc = "zscript" } })
vim.filetype.add({ extension = { zs = "zscript" } })
vim.filetype.add({ extension = { zc = "zscript" } })
vim.filetype.add({ filename = { ["MAPINFO"] = "gameinfo" } })
vim.filetype.add({ filename = { ["MENUDEF"] = "gameinfo" } })
vim.filetype.add({ filename = { ["GAMEINFO"] = "gameinfo" } })
vim.filetype.add({ filename = { ["SNDINFO"] = "gameinfo" } })
vim.filetype.add({ filename = { ["SBARINFO"] = "gameinfo" } })

-- set comment strings for doom files
vim.cmd([[
  autocmd FileType zscript setlocal commentstring=//%s
  autocmd FileType gameinfo setlocal commentstring=//%s
  ]])

-- sets Normal Mode Cursor to transparent block
vim.cmd([[
  set guicursor=n-v-c:block-Cursor/lCursor,i-ci-ve:ver25-Cursor2/lCursor2,r-cr:hor20,o:hor50
]])
