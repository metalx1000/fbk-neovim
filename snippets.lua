-- followed this tutorial
-- https://www.youtube.com/watch?v=FmHhonPjvvA
--

local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local extras = require("luasnip.extras")
local rep = extras.rep
local fmt = require("luasnip.extras.fmt").fmt
local f = ls.function_node

local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }
keymap("i", "<c-j>", "<cmd>lua require'luasnip'.jump(1)<CR>", opts)
keymap("s", "<c-j>", "<cmd>lua require'luasnip'.jump(1)<CR>", opts)
keymap("i", "<c-k>", "<cmd>lua require'luasnip'.jump(-1)<CR>", opts)
keymap("s", "<c-k>", "<cmd>lua require'luasnip'.jump(-1)<CR>", opts)

local year = function()
  local y = os.date("%Y")
  return y
end



ls.add_snippets("all", {
s("fbksite", {
t({"https://filmsbykris.com"}),
}),
s("fbk", {
t({"Films By Kris"}),
}),
s("ko", {
t({"Kris Occhipinti"}),
}),
s("occhipinti", {
t({"Occhipinti"}),
}),
})
ls.add_snippets("css", {
s("buttons", {
t({"button {"}),
t({"",""}),t({"  background-color: #04AA6D; /* Green */"}),
t({"",""}),t({"  border: none;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"  padding: 15px 32px;"}),
t({"",""}),t({"  text-align: center;"}),
t({"",""}),t({"  text-decoration: none;"}),
t({"",""}),t({"  display: inline-block;"}),
t({"",""}),t({"  font-size: 16px;"}),
t({"",""}),t({"  margin: 4px 2px;"}),
t({"",""}),t({"  cursor: pointer;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".button-blue {background-color: #008CBA;}"}),
t({"",""}),t({".button-red {background-color: #f44336;}"}),
t({"",""}),t({".button-gray {background-color: #e7e7e7; color: black;}"}),
t({"",""}),t({".button-black {background-color: #555555;}"}),
}),
s("center", {
t({".center {"}),
t({"",""}),t({"  text-align: center;"}),
t({"",""}),t({"  margin: auto;"}),
t({"",""}),t({"  padding: 10px;"}),
t({"",""}),t({"}"}),
}),
s("glow_buttons", {
t({".button-glow {"}),
t({"",""}),t({"  animation: glowing 1300ms infinite;"}),
t({"",""}),t({"}"}),
t({"",""}),t({":root {"}),
t({"",""}),t({"  --glow-color1: red;"}),
t({"",""}),t({"  --glow-color2: blue;"}),
t({"",""}),t({"}"}),
t({"",""}),t({"@keyframes glowing {"}),
t({"",""}),t({"  0% {"}),
t({"",""}),t({"    background-color: var(--glow-color1);"}),
t({"",""}),t({"    box-shadow: 0 0 5px var(--glow-color1);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  50% {"}),
t({"",""}),t({"    background-color: var(--glow-color2);"}),
t({"",""}),t({"    box-shadow: 0 0 20px var(--glow-color2);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  100% {"}),
t({"",""}),t({"    background-color: var(--glow-color1);"}),
t({"",""}),t({"    box-shadow: 0 0 5px var(--glow-color1);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
}),
s("grid_1", {
t({".grid {"}),
t({"",""}),t({"  display: grid;"}),
t({"",""}),t({"  grid-row-gap: 1rem;"}),
t({"",""}),t({"  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));"}),
t({"",""}),t({"}"}),
}),
s("scale_image", {
t({".scale_img {"}),
t({"",""}),t({"  max-width: 100%;"}),
t({"",""}),t({"  max-height: 100%;"}),
t({"",""}),t({"}"}),
}),
s("wells", {
t({".well {"}),
t({"",""}),t({"  min-height: 20px;"}),
t({"",""}),t({"  padding: 19px;"}),
t({"",""}),t({"  margin-bottom: 20px;"}),
t({"",""}),t({"  background-color: #f5f5f5;"}),
t({"",""}),t({"  border: 1px solid #e3e3e3;"}),
t({"",""}),t({"  border-radius: 4px;"}),
t({"",""}),t({"  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);"}),
t({"",""}),t({"  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-red {"}),
t({"",""}),t({"  background-color: red;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-green {"}),
t({"",""}),t({"  background-color: #04AA6D;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-blue {"}),
t({"",""}),t({"  background-color: #008CBA;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-black {"}),
t({"",""}),t({"  background-color: #555555;"}),
t({"",""}),t({"  color: black;"}),
t({"",""}),t({"}"}),
}),
})
ls.add_snippets("html", {
s("buttons", {
t({"button {"}),
t({"",""}),t({"  background-color: #04AA6D; /* Green */"}),
t({"",""}),t({"  border: none;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"  padding: 15px 32px;"}),
t({"",""}),t({"  text-align: center;"}),
t({"",""}),t({"  text-decoration: none;"}),
t({"",""}),t({"  display: inline-block;"}),
t({"",""}),t({"  font-size: 16px;"}),
t({"",""}),t({"  margin: 4px 2px;"}),
t({"",""}),t({"  cursor: pointer;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".button-blue {background-color: #008CBA;}"}),
t({"",""}),t({".button-red {background-color: #f44336;}"}),
t({"",""}),t({".button-gray {background-color: #e7e7e7; color: black;}"}),
t({"",""}),t({".button-black {background-color: #555555;}"}),
}),
s("center", {
t({".center {"}),
t({"",""}),t({"  text-align: center;"}),
t({"",""}),t({"  margin: auto;"}),
t({"",""}),t({"  padding: 10px;"}),
t({"",""}),t({"}"}),
}),
s("glow_buttons", {
t({".button-glow {"}),
t({"",""}),t({"  animation: glowing 1300ms infinite;"}),
t({"",""}),t({"}"}),
t({"",""}),t({":root {"}),
t({"",""}),t({"  --glow-color1: red;"}),
t({"",""}),t({"  --glow-color2: blue;"}),
t({"",""}),t({"}"}),
t({"",""}),t({"@keyframes glowing {"}),
t({"",""}),t({"  0% {"}),
t({"",""}),t({"    background-color: var(--glow-color1);"}),
t({"",""}),t({"    box-shadow: 0 0 5px var(--glow-color1);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  50% {"}),
t({"",""}),t({"    background-color: var(--glow-color2);"}),
t({"",""}),t({"    box-shadow: 0 0 20px var(--glow-color2);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  100% {"}),
t({"",""}),t({"    background-color: var(--glow-color1);"}),
t({"",""}),t({"    box-shadow: 0 0 5px var(--glow-color1);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
}),
s("grid_1", {
t({".grid {"}),
t({"",""}),t({"  display: grid;"}),
t({"",""}),t({"  grid-row-gap: 1rem;"}),
t({"",""}),t({"  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));"}),
t({"",""}),t({"}"}),
}),
s("scale_image", {
t({".scale_img {"}),
t({"",""}),t({"  max-width: 100%;"}),
t({"",""}),t({"  max-height: 100%;"}),
t({"",""}),t({"}"}),
}),
s("wells", {
t({".well {"}),
t({"",""}),t({"  min-height: 20px;"}),
t({"",""}),t({"  padding: 19px;"}),
t({"",""}),t({"  margin-bottom: 20px;"}),
t({"",""}),t({"  background-color: #f5f5f5;"}),
t({"",""}),t({"  border: 1px solid #e3e3e3;"}),
t({"",""}),t({"  border-radius: 4px;"}),
t({"",""}),t({"  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);"}),
t({"",""}),t({"  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-red {"}),
t({"",""}),t({"  background-color: red;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-green {"}),
t({"",""}),t({"  background-color: #04AA6D;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-blue {"}),
t({"",""}),t({"  background-color: #008CBA;"}),
t({"",""}),t({"  color: white;"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({".well-black {"}),
t({"",""}),t({"  background-color: #555555;"}),
t({"",""}),t({"  color: black;"}),
t({"",""}),t({"}"}),
}),
})
ls.add_snippets("gameinfo", {
s("about", {
t({"about"}),
}),
s("accessed", {
t({"accessed"}),
}),
s("actual", {
t({"actual"}),
}),
s("AddEventHandlers", {
t({"AddEventHandlers = \""}),i(1),t({"\""}),
}),
s("advanced", {
t({"advanced"}),
}),
s("advisorytime", {
t({"advisorytime"}),
}),
s("AdvSoundOptions", {
t({"AdvSoundOptions"}),
}),
s("ALDevices", {
t({"ALDevices"}),
}),
s("allcheats", {
t({"allcheats"}),
}),
s("Alternative", {
t({"Alternative"}),
}),
s("AltHUDAmmo", {
t({"AltHUDAmmo"}),
}),
s("AltHUDAmmoOrder", {
t({"AltHUDAmmoOrder"}),
}),
s("AltHUDLag", {
t({"AltHUDLag"}),
}),
s("AltHudOptions", {
t({"AltHudOptions"}),
}),
s("AltHUDScale", {
t({"AltHUDScale"}),
}),
s("AltHUDTime", {
t({"AltHUDTime"}),
}),
s("AlwaysRun", {
t({"AlwaysRun"}),
}),
s("AMCoordinates", {
t({"AMCoordinates"}),
}),
s("ARM1A0", {
t({"ARM1A0"}),
}),
s("ARM2A0", {
t({"ARM2A0"}),
}),
s("armoricons", {
t({"armoricons"}),
}),
s("Audio", {
t({"Audio"}),
}),
s("Autoaim", {
t({"Autoaim"}),
}),
s("Automap", {
t({"Automap"}),
}),
s("AutomapOptions", {
t({"AutomapOptions"}),
}),
s("Autosave", {
t({"Autosave"}),
}),
s("autosavecount", {
t({"autosavecount"}),
}),
s("Backpack", {
t({"Backpack"}),
}),
s("backpacktype", {
t({"backpacktype"}),
}),
s("based", {
t({"based"}),
}),
s("between", {
t({"between"}),
}),
s("BFG9000", {
t({"BFG9000"}),
}),
s("BigFont", {
t({"BigFont"}),
}),
s("BigFont", {
t({"Font \"BigFont\", \""}),i(1),t({"\" //color"}),
}),
s("BloodTypes", {
t({"BloodTypes"}),
}),
s("border", {
t({"border"}),
}),
s("borderflat", {
t({"borderflat"}),
}),
s("BRICK", {
t({"BRICK"}),
}),
s("BufferCounts", {
t({"BufferCounts"}),
}),
s("BufferSizes", {
t({"BufferSizes"}),
}),
s("calling", {
t({"calling"}),
}),
s("CCMDs", {
t({"CCMDs"}),
}),
s("centermenu", {
t({"centermenu"}),
}),
s("centerview", {
t({"centerview"}),
}),
s("Chaingun", {
t({"Chaingun"}),
}),
s("Chainsaw", {
t({"Chainsaw"}),
}),
s("chase", {
t({"chase"}),
}),
s("chatsound", {
t({"chatsound"}),
}),
s("chexcurs", {
t({"chexcurs"}),
}),
s("class", {
t({"class"}),
}),
s("clearnodecache", {
t({"clearnodecache"}),
}),
s("CLOADNET", {
t({"CLOADNET"}),
}),
s("CNEWGAME", {
t({"CNEWGAME"}),
}),
s("Color", {
t({"Color"}),
}),
s("ColorPicker", {
t({"ColorPicker"}),
}),
s("ColorPickerMenu", {
t({"ColorPickerMenu"}),
}),
s("Colors", {
t({"Colors"}),
}),
s("ColumnMethods", {
t({"ColumnMethods"}),
}),
s("Command", {
t({"Command"}),
}),
s("Compatibility", {
t({"Compatibility"}),
}),
s("CompatibilityMenu", {
t({"CompatibilityMenu"}),
}),
s("CompatibilityOptions", {
t({"CompatibilityOptions"}),
}),
s("compatmode", {
t({"compatmode"}),
}),
s("CompatModes", {
t({"CompatModes"}),
}),
s("configurable", {
t({"configurable"}),
}),
s("ConsoleScaleValues", {
t({"ConsoleScaleValues"}),
}),
s("Contrast", {
t({"Contrast"}),
}),
s("Control", {
t({"Control"}),
}),
s("ControlMessage", {
t({"ControlMessage"}),
}),
s("Controls", {
t({"Controls"}),
}),
s("Corners", {
t({"Corners"}),
}),
s("created", {
t({"created"}),
}),
s("CREDIT", {
t({"CREDIT"}),
}),
s("creditpage", {
t({"creditpage"}),
}),
s("crosshair", {
t({"crosshair"}),
}),
s("crosshaircolor", {
t({"crosshaircolor"}),
}),
s("crosshairforce", {
t({"crosshairforce"}),
}),
s("crosshairgrow", {
t({"crosshairgrow"}),
}),
s("crosshairhealth", {
t({"crosshairhealth"}),
}),
s("Crosshairs", {
t({"Crosshairs"}),
}),
s("crosshairscale", {
t({"crosshairscale"}),
}),
s("crouch", {
t({"crouch"}),
}),
s("Cubic", {
t({"Cubic"}),
}),
s("cursor", {
t({"cursor"}),
}),
s("Cursors", {
t({"Cursors"}),
}),
s("CustomizeControls", {
t({"CustomizeControls"}),
}),
s("decent", {
t({"decent"}),
}),
s("Default", {
t({"Default"}),
}),
s("defaultbloodcolor", {
t({"defaultbloodcolor"}),
}),
s("defaultbloodparticlecolor", {
t({"defaultbloodparticlecolor"}),
}),
s("defaultdropstyle", {
t({"defaultdropstyle"}),
}),
s("DEFAULTLISTMENU", {
t({"DEFAULTLISTMENU"}),
}),
s("DefaultOptionMenu", {
t({"DefaultOptionMenu"}),
}),
s("defaultrespawntime", {
t({"defaultrespawntime"}),
}),
s("defined", {
t({"defined"}),
}),
s("definition", {
t({"definition"}),
}),
s("definventorymaxamount", {
t({"definventorymaxamount"}),
}),
s("defkickback", {
t({"defkickback"}),
}),
s("dehload", {
t({"dehload"}),
}),
s("dehopt", {
t({"dehopt"}),
}),
s("design", {
t({"design"}),
}),
s("developer", {
t({"developer"}),
}),
s("DevMessageLevels", {
t({"DevMessageLevels"}),
}),
s("differences", {
t({"differences"}),
}),
s("different", {
t({"different"}),
}),
s("dimamount", {
t({"dimamount"}),
}),
s("dimcolor", {
t({"dimcolor"}),
}),
s("DirectSound", {
t({"DirectSound"}),
}),
s("disableautosave", {
t({"disableautosave"}),
}),
s("displaynametags", {
t({"displaynametags"}),
}),
s("DisplayTagsTypes", {
t({"DisplayTagsTypes"}),
}),
s("DoomBorder", {
t({"DoomBorder"}),
}),
s("doomcurs", {
t({"doomcurs"}),
}),
s("DoomPlayer", {
t({"DoomPlayer"}),
}),
s("elements", {
t({"elements"}),
}),
s("empty", {
t({"empty"}),
}),
s("endoom", {
t({"endoom"}),
}),
s("Endoom", {
t({"Endoom"}),
}),
s("ENDOOM", {
t({"ENDOOM"}),
}),
s("episode", {
t({"episode"}),
}),
s("EpisodeMenu", {
t({"EpisodeMenu"}),
}),
s("ExtraTicMode", {
t({"ExtraTicMode"}),
}),
s("FallingDM", {
t({"FallingDM"}),
}),
s("files", {
t({"files"}),
}),
s("filled", {
t({"filled"}),
}),
s("finaleflat", {
t({"finaleflat"}),
}),
s("finalemusic", {
t({"finalemusic"}),
}),
s("finalepage", {
t({"finalepage"}),
}),
s("first", {
t({"first"}),
}),
s("Fist", {
t({"Fist"}),
}),
s("FMODSoundItems", {
t({"FMODSoundItems"}),
}),
s("following", {
t({"following"}),
}),
s("Font", {
t({"Font"}),
}),
s("Font", {
t({"Font \"BigFont\", \""}),i(1),t({"\" //color"}),
}),
s("fonts", {
t({"fonts"}),
}),
s("ForceRatios", {
t({"ForceRatios"}),
}),
s("freelook", {
t({"freelook"}),
}),
s("F_SKY1", {
t({"F_SKY1"}),
}),
s("fullscreen", {
t({"fullscreen"}),
}),
s("function", {
t({"function"}),
}),
s("Fuzziness", {
t({"Fuzziness"}),
}),
s("GameFilesMenu", {
t({"GameFilesMenu"}),
}),
s("gameinfo", {
t({"GameInfo{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("Gameplay", {
t({"Gameplay"}),
}),
s("GameplayMenu", {
t({"GameplayMenu"}),
}),
s("GameplayOptions", {
t({"GameplayOptions"}),
}),
s("games", {
t({"games"}),
}),
s("Gamma", {
t({"Gamma"}),
}),
s("Gender", {
t({"Gender"}),
}),
s("generation", {
t({"generation"}),
}),
s("gibfactor", {
t({"gibfactor"}),
}),
s("GOLD", {
t({"GOLD"}),
}),
s("GRAY", {
t({"GRAY"}),
}),
s("Green", {
t({"Green"}),
}),
s("GRNROCK", {
t({"GRNROCK"}),
}),
s("guaranteed", {
t({"guaranteed"}),
}),
s("GusMemory", {
t({"GusMemory"}),
}),
s("HELP", {
t({"HELP"}),
}),
s("herecurs", {
t({"herecurs"}),
}),
s("Heretic", {
t({"Heretic"}),
}),
s("Hexen", {
t({"Hexen"}),
}),
s("hexncurs", {
t({"hexncurs"}),
}),
s("HUDOptions", {
t({"HUDOptions"}),
}),
s("identically", {
t({"identically"}),
}),
s("IfGame", {
t({"IfGame"}),
}),
s("implementation", {
t({"implementation"}),
}),
s("important", {
t({"important"}),
}),
s("infopage", {
t({"infopage"}),
}),
s("intermissioncounter", {
t({"intermissioncounter"}),
}),
s("intermissionmusic", {
t({"intermissionmusic"}),
}),
s("internally", {
t({"internally"}),
}),
s("invdrop", {
t({"invdrop"}),
}),
s("Inversion", {
t({"Inversion"}),
}),
s("invertmouse", {
t({"invertmouse"}),
}),
s("invnext", {
t({"invnext"}),
}),
s("invprev", {
t({"invprev"}),
}),
s("invquery", {
t({"invquery"}),
}),
s("invuse", {
t({"invuse"}),
}),
s("invuseall", {
t({"invuseall"}),
}),
s("items", {
t({"items"}),
}),
s("JoyAxisMapNames", {
t({"JoyAxisMapNames"}),
}),
s("joystick", {
t({"joystick"}),
}),
s("JoystickConfigMenu", {
t({"JoystickConfigMenu"}),
}),
s("JoystickOptions", {
t({"JoystickOptions"}),
}),
s("JumpCrouchFreeLook", {
t({"JumpCrouchFreeLook"}),
}),
s("Leaving", {
t({"Leaving"}),
}),
s("linear", {
t({"linear"}),
}),
s("Linespacing", {
t({"Linespacing"}),
}),
s("links", {
t({"links"}),
}),
s("ListMenu", {
t({"ListMenu \""}),i(1),t({"\""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(2),t({""}),
t({"",""}),t({"}"}),
}),
s("LoadGameMenu", {
t({"LoadGameMenu"}),
}),
s("LOADNET", {
t({"LOADNET"}),
}),
s("longsavemessages", {
t({"longsavemessages"}),
}),
s("lookspring", {
t({"lookspring"}),
}),
s("lookstrafe", {
t({"lookstrafe"}),
}),
s("MainMenu", {
t({"MainMenu"}),
}),
s("MainMenu", {
t({"ListMenu \"MainMenu\""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("MapBackTypes", {
t({"MapBackTypes"}),
}),
s("MapColorMenu", {
t({"MapColorMenu"}),
}),
s("MapColorTypes", {
t({"MapColorTypes"}),
}),
s("MapControl", {
t({"MapControl"}),
}),
s("MapControlsMenu", {
t({"MapControlsMenu"}),
}),
s("mapinfo", {
t({"MapInfo{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("MAPINFO", {
t({"MAPINFO"}),
}),
s("MaplabelTypes", {
t({"MaplabelTypes"}),
}),
s("M_BACK_D", {
t({"M_BACK_D"}),
}),
s("mechanism", {
t({"mechanism"}),
}),
s("menubackbutton", {
t({"menubackbutton"}),
}),
s("menuconsole", {
t({"menuconsole"}),
}),
s("menufontcolor_action", {
t({"menufontcolor_action"}),
}),
s("menufontcolor_header", {
t({"menufontcolor_header"}),
}),
s("menufontcolor_highlight", {
t({"menufontcolor_highlight"}),
}),
s("menufontcolor_label", {
t({"menufontcolor_label"}),
}),
s("menufontcolor_selection", {
t({"menufontcolor_selection"}),
}),
s("menufontcolor_title", {
t({"menufontcolor_title"}),
}),
s("menufontcolor_value", {
t({"menufontcolor_value"}),
}),
s("MenuMouse", {
t({"MenuMouse"}),
}),
s("menus", {
t({"menus"}),
}),
s("MessageLevels", {
t({"MessageLevels"}),
}),
s("messagemode", {
t({"messagemode"}),
}),
s("MessageOptions", {
t({"MessageOptions"}),
}),
s("Messages", {
t({"Messages"}),
}),
s("MidiDevices", {
t({"MidiDevices"}),
}),
s("MiscOptions", {
t({"MiscOptions"}),
}),
s("ModQuality", {
t({"ModQuality"}),
}),
s("ModReplayerOptions", {
t({"ModReplayerOptions"}),
}),
s("ModReplayers", {
t({"ModReplayers"}),
}),
s("Module", {
t({"Module"}),
}),
s("ModVolumeRamps", {
t({"ModVolumeRamps"}),
}),
s("Mouse", {
t({"Mouse"}),
}),
s("MouseOptions", {
t({"MouseOptions"}),
}),
s("MouseWindow", {
t({"MouseWindow"}),
}),
s("movebob", {
t({"movebob"}),
}),
s("M_PAUSE", {
t({"M_PAUSE"}),
}),
s("msgmidcolor", {
t({"msgmidcolor"}),
}),
s("MUSIC_DM2INT", {
t({"MUSIC_DM2INT"}),
}),
s("MUSIC_DM2TTL", {
t({"MUSIC_DM2TTL"}),
}),
s("MUSIC_READ_M", {
t({"MUSIC_READ_M"}),
}),
s("named", {
t({"named"}),
}),
s("nametagcolor", {
t({"nametagcolor"}),
}),
s("NetgameMessage", {
t({"NetgameMessage"}),
}),
s("Network", {
t({"Network"}),
}),
s("NetworkOptions", {
t({"NetworkOptions"}),
}),
s("NEWGAME", {
t({"NEWGAME"}),
}),
s("NoInterp", {
t({"NoInterp"}),
}),
s("nomonsterinterpolation", {
t({"nomonsterinterpolation"}),
}),
s("NoYes", {
t({"NoYes"}),
}),
s("OffOn", {
t({"OffOn"}),
}),
s("OnOff", {
t({"OnOff"}),
}),
s("openal", {
t({"openal"}),
}),
s("OpenALSoundItems", {
t({"OpenALSoundItems"}),
}),
s("OplCores", {
t({"OplCores"}),
}),
s("option", {
t({"option"}),
}),
s("OptionMenu", {
t({"OptionMenu"}),
}),
s("OptionMenuSettings", {
t({"OptionMenuSettings"}),
}),
s("options", {
t({"options"}),
}),
s("OptionsMenu", {
t({"OptionsMenu"}),
}),
s("OptionString", {
t({"OptionString"}),
}),
s("OptionValue", {
t({"OptionValue"}),
}),
s("OutputFormats", {
t({"OutputFormats"}),
}),
s("OverlayTypes", {
t({"OverlayTypes"}),
}),
s("overridden", {
t({"overridden"}),
}),
s("pagetime", {
t({"pagetime"}),
}),
s("PatchItem", {
t({"PatchItem"}),
}),
s("pausesign", {
t({"pausesign"}),
}),
s("Picker", {
t({"Picker"}),
}),
s("pickupcolor", {
t({"pickupcolor"}),
}),
s("Pistol", {
t({"Pistol"}),
}),
s("PlasmaRifle", {
t({"PlasmaRifle"}),
}),
s("player", {
t({"player"}),
}),
s("Playerbox", {
t({"Playerbox"}),
}),
s("playerclasses", {
t({"PlayerClasses = \""}),i(1),t({"\""}),
}),
s("PlayerclassMenu", {
t({"PlayerclassMenu"}),
}),
s("PlayerDisplay", {
t({"PlayerDisplay"}),
}),
s("PlayerMenu", {
t({"PlayerMenu"}),
}),
s("PlayerNameBox", {
t({"PlayerNameBox"}),
}),
s("Position", {
t({"Position"}),
}),
s("Position", {
t({"Position "}),i(1),t({", "}),i(2),t({""}),
}),
s("Position_xy", {
t({"Position 136, 92"}),
}),
s("preferences", {
t({"preferences"}),
}),
s("Prologic", {
t({"Prologic"}),
}),
s("Proper", {
t({"Proper"}),
}),
s("Provide", {
t({"Provide"}),
}),
s("PuffTypes", {
t({"PuffTypes"}),
}),
s("PulseAudio", {
t({"PulseAudio"}),
}),
s("queryiwad", {
t({"queryiwad"}),
}),
s("QuitMenu", {
t({"QuitMenu"}),
}),
s("QuitMenu", {
t({"TextItem \"Quit\", \"q\", \"QuitMenu\""}),
}),
s("quitmessages", {
t({"quitmessages"}),
}),
s("QUITMSG", {
t({"QUITMSG"}),
}),
s("QUITMSG1", {
t({"QUITMSG1"}),
}),
s("QUITMSG10", {
t({"QUITMSG10"}),
}),
s("QUITMSG11", {
t({"QUITMSG11"}),
}),
s("QUITMSG12", {
t({"QUITMSG12"}),
}),
s("QUITMSG13", {
t({"QUITMSG13"}),
}),
s("QUITMSG14", {
t({"QUITMSG14"}),
}),
s("QUITMSG2", {
t({"QUITMSG2"}),
}),
s("QUITMSG3", {
t({"QUITMSG3"}),
}),
s("QUITMSG4", {
t({"QUITMSG4"}),
}),
s("QUITMSG5", {
t({"QUITMSG5"}),
}),
s("QUITMSG6", {
t({"QUITMSG6"}),
}),
s("QUITMSG7", {
t({"QUITMSG7"}),
}),
s("QUITMSG8", {
t({"QUITMSG8"}),
}),
s("QUITMSG9", {
t({"QUITMSG9"}),
}),
s("quitsound", {
t({"quitsound"}),
}),
s("Ratios", {
t({"Ratios"}),
}),
s("RatiosTFT", {
t({"RatiosTFT"}),
}),
s("ReadThisMenu", {
t({"ReadThisMenu"}),
}),
s("replacing", {
t({"replacing"}),
}),
s("Replayer", {
t({"Replayer"}),
}),
s("requires", {
t({"requires"}),
}),
s("Resamplers", {
t({"Resamplers"}),
}),
s("RocketLauncher", {
t({"RocketLauncher"}),
}),
s("RocketTrailTypes", {
t({"RocketTrailTypes"}),
}),
s("RotateTypes", {
t({"RotateTypes"}),
}),
s("runtime", {
t({"runtime"}),
}),
s("SafeCommand", {
t({"SafeCommand"}),
}),
s("SampleRates", {
t({"SampleRates"}),
}),
s("SaveGameMenu", {
t({"SaveGameMenu"}),
}),
s("saveloadconfirmation", {
t({"saveloadconfirmation"}),
}),
s("ScaleValues", {
t({"ScaleValues"}),
}),
s("Scoreboard", {
t({"Scoreboard"}),
}),
s("ScoreboardOptions", {
t({"ScoreboardOptions"}),
}),
s("screenblocks", {
t({"screenblocks"}),
}),
s("ScreenResolution", {
t({"ScreenResolution"}),
}),
s("screenshot", {
t({"screenshot"}),
}),
s("ScrollTop", {
t({"ScrollTop"}),
}),
s("SecretTypes", {
t({"SecretTypes"}),
}),
s("Selector", {
t({"Selector"}),
}),
s("Selector", {
t({"Selector \""}),i(1),t({"\", "}),i(2),t({", "}),i(3),t({"  //Skull Icon, x,y"}),
}),
s("settings", {
t({"settings"}),
}),
s("Shotgun", {
t({"Shotgun"}),
}),
s("showendoom", {
t({"showendoom"}),
}),
s("showpop", {
t({"showpop"}),
}),
s("skill", {
t({"skill"}),
}),
s("SkillMenu", {
t({"SkillMenu"}),
}),
s("SkillMenu", {
t({"ListMenu \"SkillMenu\""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("skyflatname", {
t({"skyflatname"}),
}),
s("SkyModes", {
t({"SkyModes"}),
}),
s("Slider", {
t({"Slider"}),
}),
s("SLIME16", {
t({"SLIME16"}),
}),
s("SmallFont", {
t({"SmallFont"}),
}),
s("SmallFont", {
t({"Font \"SmallFont\", \""}),i(1),t({"\" //color"}),
}),
s("SmartAim", {
t({"SmartAim"}),
}),
s("something", {
t({"something"}),
}),
s("sound", {
t({"sound"}),
}),
s("SoundBackends", {
t({"SoundBackends"}),
}),
s("SoundBackendsFModOnly", {
t({"SoundBackendsFModOnly"}),
}),
s("SoundBackendsOpenALOnly", {
t({"SoundBackendsOpenALOnly"}),
}),
s("SoundOptions", {
t({"SoundOptions"}),
}),
s("SoundOutputsMac", {
t({"SoundOutputsMac"}),
}),
s("SoundOutputsUnix", {
t({"SoundOutputsUnix"}),
}),
s("SoundOutputsWindows", {
t({"SoundOutputsWindows"}),
}),
s("SpeakerModes", {
t({"SpeakerModes"}),
}),
s("Spline", {
t({"Spline"}),
}),
s("spynext", {
t({"spynext"}),
}),
s("static", {
t({"static"}),
}),
s("StaticPatch", {
t({"StaticPatch"}),
}),
s("StaticPatchCentered", {
t({"StaticPatchCentered"}),
}),
s("StaticText", {
t({"StaticText "}),i(1),t({", "}),i(2),t({", \""}),i(3),t({"\" // [pos x], [pos y], [text]"}),
}),
s("StaticTextCentered", {
t({"StaticTextCentered"}),
}),
s("StaticText", {
t({"StaticText "}),i(1),t({", "}),i(2),t({", \""}),i(3),t({"\""}),
}),
s("StaticTextSwitchable", {
t({"StaticTextSwitchable"}),
}),
s("statusbar", {
t({"statusbar"}),
}),
s("Stereo", {
t({"Stereo"}),
}),
s("stillbob", {
t({"stillbob"}),
}),
s("strfcurs", {
t({"strfcurs"}),
}),
s("Strife", {
t({"Strife"}),
}),
s("structure", {
t({"structure"}),
}),
s("STSTypes", {
t({"STSTypes"}),
}),
s("stuff", {
t({"stuff"}),
}),
s("Submenu", {
t({"Submenu"}),
}),
s("submenus", {
t({"submenus"}),
}),
s("SuperShotgun", {
t({"SuperShotgun"}),
}),
s("Surround", {
t({"Surround"}),
}),
s("Switch", {
t({"Switch"}),
}),
s("system", {
t({"system"}),
}),
s("teamdamage", {
t({"teamdamage"}),
}),
s("teamplay", {
t({"teamplay"}),
}),
s("telefogheight", {
t({"telefogheight"}),
}),
s("telezoom", {
t({"telezoom"}),
}),
s("template", {
t({"template"}),
}),
s("TextColors", {
t({"TextColors"}),
}),
s("TextField", {
t({"TextField"}),
}),
s("TextItem", {
t({"TextItem \""}),i(1),t({"\", \""}),i(2),t({"\", \""}),i(3),t({"\" // [text], [shortcut key], [next screen]"}),
}),
s("These", {
t({"These"}),
}),
s("think", {
t({"think"}),
}),
s("Title", {
t({"Title"}),
}),
s("titlemusic", {
t({"titlemusic"}),
}),
s("titlepage", {
t({"titlepage"}),
}),
s("TITLEPIC", {
t({"TITLEPIC"}),
}),
s("titletime", {
t({"titletime"}),
}),
s("toggle", {
t({"toggle"}),
}),
s("toggleconsole", {
t({"toggleconsole"}),
}),
s("togglemap", {
t({"togglemap"}),
}),
s("togglescoreboard", {
t({"togglescoreboard"}),
}),
s("translator", {
t({"translator"}),
}),
s("transsouls", {
t({"transsouls"}),
}),
s("true", {
t({"true"}),
}),
s("uiscale", {
t({"uiscale"}),
}),
s("Untranslated", {
t({"Untranslated"}),
}),
s("UNTRANSLATED", {
t({"UNTRANSLATED"}),
}),
s("ValueText", {
t({"ValueText"}),
}),
s("Video", {
t({"Video"}),
}),
s("VideoModeMenu", {
t({"VideoModeMenu"}),
}),
s("VideoOptions", {
t({"VideoOptions"}),
}),
s("VMEnterText", {
t({"VMEnterText"}),
}),
s("VMTestText", {
t({"VMTestText"}),
}),
s("WASAPI", {
t({"WASAPI"}),
}),
s("WaveOut", {
t({"WaveOut"}),
}),
s("wbobspeed", {
t({"wbobspeed"}),
}),
s("weapdrop", {
t({"weapdrop"}),
}),
s("weapnext", {
t({"weapnext"}),
}),
s("weaponslot", {
t({"weaponslot"}),
}),
s("weapprev", {
t({"weapprev"}),
}),
s("Wipes", {
t({"Wipes"}),
}),
s("wipetype", {
t({"wipetype"}),
}),
s("XHAIRS", {
t({"XHAIRS"}),
}),
s("YELLOW", {
t({"YELLOW"}),
}),
s("YesNo", {
t({"YesNo"}),
}),
s("ZDoomHexen", {
t({"ZDoomHexen"}),
}),
s("ZDoomStrife", {
t({"ZDoomStrife"}),
}),
})
ls.add_snippets("html", {
s("buttons", {
t({"<button class=\"button button-"}),i(1),t({"\" id=\""}),i(2),t({"\">"}),i(3),t({"</button>"}),
}),
s("css", {
t({"<link rel=\"stylesheet\" href=\""}),i(1),t({"\">"}),
}),
s("data-list", {
t({"<label for=\""}),i(1),t({"\">"}),i(2),t({":</label>"}),
t({"",""}),t({"<input list=\""}),i(3),t({"\" id=\""}),rep(1),t({"\" name=\""}),rep(1),t({"\" />"}),
t({"",""}),t({""}),
t({"",""}),t({"<datalist id=\""}),rep(3),t({"\">"}),
t({"",""}),t({"  <option value=\""}),i(4),t({"\">"}),i(5),t({"</option>"}),
t({"",""}),t({"</datalist>"}),
}),
s("favicon_getter", {
t({"https://www.google.com/s2/favicons?domain="}),i(1),t({"&sz="}),i(2),t({""}),
}),
s("flex_app", {
t({"<!DOCTYPE html>"}),
t({"",""}),t({"<html lang=\"en\">"}),
t({"",""}),t({"  <head>"}),
t({"",""}),t({"    <title></title>"}),
t({"",""}),t({"    <meta charset=\"utf-8\">"}),
t({"",""}),t({"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"}),
t({"",""}),t({"    <style>"}),
t({"",""}),t({"      html *{"}),
t({"",""}),t({"        font-size: 25px;"}),
t({"",""}),t({"        margin: 2px;"}),
t({"",""}),t({"      }"}),
t({"",""}),t({""}),
t({"",""}),t({"      @media only screen and (min-width: 600px) {"}),
t({"",""}),t({"        body {"}),
t({"",""}),t({"          margin-right:200px;"}),
t({"",""}),t({"          margin-left:200px;"}),
t({"",""}),t({"          margin-top: 20px;"}),
t({"",""}),t({"        }"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"      .flex{"}),
t({"",""}),t({"        display: grid;"}),
t({"",""}),t({"        grid-row-gap: 1rem;"}),
t({"",""}),t({"        grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));"}),
t({"",""}),t({"      }"}),
t({"",""}),t({""}),
t({"",""}),t({"    </style>"}),
t({"",""}),t({"  </head>"}),
t({"",""}),t({"  <body>"}),
t({"",""}),t({"    <div class=\"flex\">"}),
t({"",""}),t({"      <input id=\"input\" type=\"text\" placeholder=\"Eneter Text Here\"/>"}),
t({"",""}),t({"    </div>"}),
t({"",""}),t({"    <div class=\"flex\">"}),
t({"",""}),t({"      <button class=\"button\">Button #1</button>"}),
t({"",""}),t({"      <button class=\"button\">Button #2</button>"}),
t({"",""}),t({"    </div>"}),
t({"",""}),t({"  </body>"}),
t({"",""}),t({"  <script>"}),
t({"",""}),t({"    var input = document.querySelector(\"#input\");"}),
t({"",""}),t({"    var buttons = document.querySelector(\".button\");"}),
t({"",""}),t({"  </script>"}),
t({"",""}),t({"</html>"}),
}),
s("form-dark", {
t({"<style>"}),
t({"",""}),t({"  body {"}),
t({"",""}),t({"    align-items: center;"}),
t({"",""}),t({"    background-color: #000;"}),
t({"",""}),t({"    display: flex;"}),
t({"",""}),t({"    justify-content: center;"}),
t({"",""}),t({"    height: 100vh;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .form {"}),
t({"",""}),t({"    background-color: #15172b;"}),
t({"",""}),t({"    border-radius: 20px;"}),
t({"",""}),t({"    box-sizing: border-box;"}),
t({"",""}),t({"    height: 500px;"}),
t({"",""}),t({"    padding: 20px;"}),
t({"",""}),t({"    width: 320px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .title {"}),
t({"",""}),t({"    color: #eee;"}),
t({"",""}),t({"    font-family: sans-serif;"}),
t({"",""}),t({"    font-size: 36px;"}),
t({"",""}),t({"    font-weight: 600;"}),
t({"",""}),t({"    margin-top: 30px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .subtitle {"}),
t({"",""}),t({"    color: #eee;"}),
t({"",""}),t({"    font-family: sans-serif;"}),
t({"",""}),t({"    font-size: 16px;"}),
t({"",""}),t({"    font-weight: 600;"}),
t({"",""}),t({"    margin-top: 10px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input-container {"}),
t({"",""}),t({"    height: 50px;"}),
t({"",""}),t({"    position: relative;"}),
t({"",""}),t({"    width: 100%;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .ic1 {"}),
t({"",""}),t({"    margin-top: 40px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .ic2 {"}),
t({"",""}),t({"    margin-top: 30px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input {"}),
t({"",""}),t({"    background-color: #303245;"}),
t({"",""}),t({"    border-radius: 12px;"}),
t({"",""}),t({"    border: 0;"}),
t({"",""}),t({"    box-sizing: border-box;"}),
t({"",""}),t({"    color: #eee;"}),
t({"",""}),t({"    font-size: 18px;"}),
t({"",""}),t({"    height: 100%;"}),
t({"",""}),t({"    outline: 0;"}),
t({"",""}),t({"    padding: 4px 20px 0;"}),
t({"",""}),t({"    width: 100%;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .cut {"}),
t({"",""}),t({"    background-color: #15172b;"}),
t({"",""}),t({"    border-radius: 10px;"}),
t({"",""}),t({"    height: 20px;"}),
t({"",""}),t({"    left: 20px;"}),
t({"",""}),t({"    position: absolute;"}),
t({"",""}),t({"    top: -20px;"}),
t({"",""}),t({"    transform: translateY(0);"}),
t({"",""}),t({"    transition: transform 200ms;"}),
t({"",""}),t({"    width: 76px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .cut-short {"}),
t({"",""}),t({"    width: 50px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input:focus~.cut,"}),
t({"",""}),t({"  .input:not(:placeholder-shown)~.cut {"}),
t({"",""}),t({"    transform: translateY(8px);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .placeholder {"}),
t({"",""}),t({"    color: #65657b;"}),
t({"",""}),t({"    font-family: sans-serif;"}),
t({"",""}),t({"    left: 20px;"}),
t({"",""}),t({"    line-height: 14px;"}),
t({"",""}),t({"    pointer-events: none;"}),
t({"",""}),t({"    position: absolute;"}),
t({"",""}),t({"    transform-origin: 0 50%;"}),
t({"",""}),t({"    transition: transform 200ms, color 200ms;"}),
t({"",""}),t({"    top: 20px;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input:focus~.placeholder,"}),
t({"",""}),t({"  .input:not(:placeholder-shown)~.placeholder {"}),
t({"",""}),t({"    transform: translateY(-30px) translateX(10px) scale(0.75);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input:not(:placeholder-shown)~.placeholder {"}),
t({"",""}),t({"    color: #808097;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input:focus~.placeholder {"}),
t({"",""}),t({"    color: #dc2f55;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .submit {"}),
t({"",""}),t({"    background-color: #08d;"}),
t({"",""}),t({"    border-radius: 12px;"}),
t({"",""}),t({"    border: 0;"}),
t({"",""}),t({"    box-sizing: border-box;"}),
t({"",""}),t({"    color: #eee;"}),
t({"",""}),t({"    cursor: pointer;"}),
t({"",""}),t({"    font-size: 18px;"}),
t({"",""}),t({"    height: 50px;"}),
t({"",""}),t({"    margin-top: 38px;"}),
t({"",""}),t({"    // outline: 0;"}),
t({"",""}),t({"    text-align: center;"}),
t({"",""}),t({"    width: 100%;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .submit:active {"}),
t({"",""}),t({"    background-color: #06b;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"</style>"}),
t({"",""}),t({""}),
t({"",""}),t({"<div class=\"form\">"}),
t({"",""}),t({"  <div class=\"title\">Welcome</div>"}),
t({"",""}),t({"  <div class=\"subtitle\">Let's create your account!</div>"}),
t({"",""}),t({"  <div class=\"input-container ic1\">"}),
t({"",""}),t({"    <input id=\"firstname\" class=\"input\" type=\"text\" placeholder=\" \" />"}),
t({"",""}),t({"    <div class=\"cut\"></div>"}),
t({"",""}),t({"    <label for=\"firstname\" class=\"placeholder\">First name</label>"}),
t({"",""}),t({"  </div>"}),
t({"",""}),t({"  <div class=\"input-container ic2\">"}),
t({"",""}),t({"    <input id=\"lastname\" class=\"input\" type=\"text\" placeholder=\" \" />"}),
t({"",""}),t({"    <div class=\"cut\"></div>"}),
t({"",""}),t({"    <label for=\"lastname\" class=\"placeholder\">Last name</label>"}),
t({"",""}),t({"  </div>"}),
t({"",""}),t({"  <div class=\"input-container ic2\">"}),
t({"",""}),t({"    <input id=\"email\" class=\"input\" type=\"text\" placeholder=\" \" />"}),
t({"",""}),t({"    <div class=\"cut cut-short\"></div>"}),
t({"",""}),t({"    <label for=\"email\" class=\"placeholder\">Email</>"}),
t({"",""}),t({"  </div>"}),
t({"",""}),t({"  <button type=\"text\" class=\"submit\">submit</button>"}),
t({"",""}),t({"</div>"}),
}),
s("form", {
t({"<form action=\""}),i(1),t({"\" method=\""}),i(2),t({"\">"}),
t({"",""}),t({"  "}),i(3),t({""}),
t({"",""}),t({"</form>"}),
}),
s("form-light", {
t({"<style>"}),
t({"",""}),t({"  *,"}),
t({"",""}),t({"  *:after,"}),
t({"",""}),t({"  *:before {"}),
t({"",""}),t({"    box-sizing: border-box;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  body {"}),
t({"",""}),t({"    font-family: \"DM Sans\", sans-serif;"}),
t({"",""}),t({"    line-height: 1.5;"}),
t({"",""}),t({"    background-color: #f1f3fb;"}),
t({"",""}),t({"    padding: 0 2rem;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  img {"}),
t({"",""}),t({"    max-width: 100%;"}),
t({"",""}),t({"    display: block;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({""}),
t({"",""}),t({"  // iOS Reset "}),
t({"",""}),t({"  input {"}),
t({"",""}),t({"    appearance: none;"}),
t({"",""}),t({"    border-radius: 0;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .card {"}),
t({"",""}),t({"    margin: 2rem auto;"}),
t({"",""}),t({"    display: flex;"}),
t({"",""}),t({"    flex-direction: column;"}),
t({"",""}),t({"    width: 100%;"}),
t({"",""}),t({"    max-width: 425px;"}),
t({"",""}),t({"    background-color: #FFF;"}),
t({"",""}),t({"    border-radius: 10px;"}),
t({"",""}),t({"    box-shadow: 0 10px 20px 0 rgba(#999, .25);"}),
t({"",""}),t({"    padding: .75rem;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .card-image {"}),
t({"",""}),t({"    border-radius: 8px;"}),
t({"",""}),t({"    overflow: hidden;"}),
t({"",""}),t({"    padding-bottom: 65%;"}),
t({"",""}),t({"    background-image: url('https://assets.codepen.io/285131/coffee_1.jpg');"}),
t({"",""}),t({"    background-repeat: no-repeat;"}),
t({"",""}),t({"    background-size: 150%;"}),
t({"",""}),t({"    background-position: 0 5%;"}),
t({"",""}),t({"    position: relative;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .card-heading {"}),
t({"",""}),t({"    position: absolute;"}),
t({"",""}),t({"    left: 10%;"}),
t({"",""}),t({"    top: 15%;"}),
t({"",""}),t({"    right: 10%;"}),
t({"",""}),t({"    font-size: 1.75rem;"}),
t({"",""}),t({"    font-weight: 700;"}),
t({"",""}),t({"    color: #735400;"}),
t({"",""}),t({"    line-height: 1.222;"}),
t({"",""}),t({""}),
t({"",""}),t({"    small {"}),
t({"",""}),t({"      display: block;"}),
t({"",""}),t({"      font-size: .75em;"}),
t({"",""}),t({"      font-weight: 400;"}),
t({"",""}),t({"      margin-top: .25em;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .card-form {"}),
t({"",""}),t({"    padding: 2rem 1rem 0;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input {"}),
t({"",""}),t({"    display: flex;"}),
t({"",""}),t({"    flex-direction: column-reverse;"}),
t({"",""}),t({"    position: relative;"}),
t({"",""}),t({"    padding-top: 1.5rem;"}),
t({"",""}),t({""}),
t({"",""}),t({"    &+.input {"}),
t({"",""}),t({"      margin-top: 1.5rem;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input-label {"}),
t({"",""}),t({"    color: #8597a3;"}),
t({"",""}),t({"    position: absolute;"}),
t({"",""}),t({"    top: 1.5rem;"}),
t({"",""}),t({"    transition: .25s ease;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .input-field {"}),
t({"",""}),t({"    border: 0;"}),
t({"",""}),t({"    z-index: 1;"}),
t({"",""}),t({"    background-color: transparent;"}),
t({"",""}),t({"    border-bottom: 2px solid #eee;"}),
t({"",""}),t({"    font: inherit;"}),
t({"",""}),t({"    font-size: 1.125rem;"}),
t({"",""}),t({"    padding: .25rem 0;"}),
t({"",""}),t({""}),
t({"",""}),t({"    &:focus,"}),
t({"",""}),t({"    &:valid {"}),
t({"",""}),t({"      outline: 0;"}),
t({"",""}),t({"      border-bottom-color: #6658d3;"}),
t({"",""}),t({""}),
t({"",""}),t({"      &+.input-label {"}),
t({"",""}),t({"        color: #6658d3;"}),
t({"",""}),t({"        transform: translateY(-1.5rem);"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .action {"}),
t({"",""}),t({"    margin-top: 2rem;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .action-button {"}),
t({"",""}),t({"    font: inherit;"}),
t({"",""}),t({"    font-size: 1.25rem;"}),
t({"",""}),t({"    padding: 1em;"}),
t({"",""}),t({"    width: 100%;"}),
t({"",""}),t({"    font-weight: 500;"}),
t({"",""}),t({"    background-color: #6658d3;"}),
t({"",""}),t({"    border-radius: 6px;"}),
t({"",""}),t({"    color: #FFF;"}),
t({"",""}),t({"    border: 0;"}),
t({"",""}),t({""}),
t({"",""}),t({"    &:focus {"}),
t({"",""}),t({"      outline: 0;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  .card-info {"}),
t({"",""}),t({"    padding: 1rem 1rem;"}),
t({"",""}),t({"    text-align: center;"}),
t({"",""}),t({"    font-size: .875rem;"}),
t({"",""}),t({"    color: #8597a3;"}),
t({"",""}),t({""}),
t({"",""}),t({"    a {"}),
t({"",""}),t({"      display: block;"}),
t({"",""}),t({"      color: #6658d3;"}),
t({"",""}),t({"      text-decoration: none;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"</style>"}),
t({"",""}),t({"<div class=\"container\">"}),
t({"",""}),t({"  <!-- code here -->"}),
t({"",""}),t({"  <div class=\"card\">"}),
t({"",""}),t({"    <div class=\"card-image\">"}),
t({"",""}),t({"      <h2 class=\"card-heading\">"}),
t({"",""}),t({"        Get started"}),
t({"",""}),t({"        <small>Let us create your account</small>"}),
t({"",""}),t({"      </h2>"}),
t({"",""}),t({"    </div>"}),
t({"",""}),t({"    <form class=\"card-form\">"}),
t({"",""}),t({"      <div class=\"input\">"}),
t({"",""}),t({"        <input type=\"text\" class=\"input-field\" value=\"\" required />"}),
t({"",""}),t({"        <label class=\"input-label\">Full name</label>"}),
t({"",""}),t({"      </div>"}),
t({"",""}),t({"      <div class=\"input\">"}),
t({"",""}),t({"        <input type=\"text\" class=\"input-field\" value=\"\" required />"}),
t({"",""}),t({"        <label class=\"input-label\">Email</label>"}),
t({"",""}),t({"      </div>"}),
t({"",""}),t({"      <div class=\"input\">"}),
t({"",""}),t({"        <input type=\"password\" class=\"input-field\" required />"}),
t({"",""}),t({"        <label class=\"input-label\">Password</label>"}),
t({"",""}),t({"      </div>"}),
t({"",""}),t({"      <div class=\"action\">"}),
t({"",""}),t({"        <button class=\"action-button\">Get started</button>"}),
t({"",""}),t({"      </div>"}),
t({"",""}),t({"    </form>"}),
t({"",""}),t({"    <div class=\"card-info\">"}),
t({"",""}),t({"      <p>By signing up you are agreeing to our <a href=\"#\">Terms and Conditions</a></p>"}),
t({"",""}),t({"    </div>"}),
t({"",""}),t({"  </div>"}),
t({"",""}),t({"</div>"}),
}),
s("login_screen", {
t({"<!DOCTYPE html>"}),
t({"",""}),t({"<html>"}),
t({"",""}),t({""}),
t({"",""}),t({"<head>"}),
t({"",""}),t({"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"}),
t({"",""}),t({"  <style>"}),
t({"",""}),t({"    @media only screen and (min-width: 600px) {"}),
t({"",""}),t({"      body {"}),
t({"",""}),t({"        margin-right: 200px;"}),
t({"",""}),t({"        margin-left: 200px;"}),
t({"",""}),t({"        margin-top: 20px;"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    body {"}),
t({"",""}),t({"      font-family: Arial, Helvetica, sans-serif;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    form {"}),
t({"",""}),t({"      border: 3px solid #f1f1f1;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    input[type=text],"}),
t({"",""}),t({"    input[type=password] {"}),
t({"",""}),t({"      width: 100%;"}),
t({"",""}),t({"      padding: 12px 20px;"}),
t({"",""}),t({"      margin: 8px 0;"}),
t({"",""}),t({"      display: inline-block;"}),
t({"",""}),t({"      border: 1px solid #ccc;"}),
t({"",""}),t({"      box-sizing: border-box;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    button {"}),
t({"",""}),t({"      background-color: #04AA6D;"}),
t({"",""}),t({"      color: white;"}),
t({"",""}),t({"      padding: 14px 20px;"}),
t({"",""}),t({"      margin: 8px 0;"}),
t({"",""}),t({"      border: none;"}),
t({"",""}),t({"      cursor: pointer;"}),
t({"",""}),t({"      width: 100%;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    button:hover {"}),
t({"",""}),t({"      opacity: 0.8;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    .cancelbtn {"}),
t({"",""}),t({"      width: auto;"}),
t({"",""}),t({"      padding: 10px 18px;"}),
t({"",""}),t({"      background-color: #f44336;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    .imgcontainer {"}),
t({"",""}),t({"      text-align: center;"}),
t({"",""}),t({"      margin: 24px 0 12px 0;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    img.avatar {"}),
t({"",""}),t({"      width: 40%;"}),
t({"",""}),t({"      border-radius: 50%;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    .container {"}),
t({"",""}),t({"      padding: 16px;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    span.psw {"}),
t({"",""}),t({"      float: right;"}),
t({"",""}),t({"      padding-top: 16px;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({""}),
t({"",""}),t({"    /* Change styles for span and cancel button on extra small screens */"}),
t({"",""}),t({"    @media screen and (max-width: 300px) {"}),
t({"",""}),t({"      span.psw {"}),
t({"",""}),t({"        display: block;"}),
t({"",""}),t({"        float: none;"}),
t({"",""}),t({"      }"}),
t({"",""}),t({""}),
t({"",""}),t({"      .cancelbtn {"}),
t({"",""}),t({"        width: 100%;"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  </style>"}),
t({"",""}),t({"</head>"}),
t({"",""}),t({""}),
t({"",""}),t({"<body>"}),
t({"",""}),t({""}),
t({"",""}),t({"  <h2>Login Form</h2>"}),
t({"",""}),t({""}),
t({"",""}),t({"  <form action=\""}),i(1),t({"\" method=\"post\">"}),
t({"",""}),t({"    <!-- <div class=\"imgcontainer\"> -->"}),
t({"",""}),t({"    <!--   <img src=\""}),i(2),t({"\" alt=\"Avatar\" class=\"avatar\"> -->"}),
t({"",""}),t({"    <!-- </div> -->"}),
t({"",""}),t({""}),
t({"",""}),t({"    <div class=\"container\">"}),
t({"",""}),t({"      <label for=\"uname\"><b>Username</b></label>"}),
t({"",""}),t({"      <input type=\"text\" placeholder=\"Enter Username\" name=\"uname\" required>"}),
t({"",""}),t({""}),
t({"",""}),t({"      <label for=\"psw\"><b>Password</b></label>"}),
t({"",""}),t({"      <input type=\"password\" placeholder=\"Enter Password\" name=\"psw\" required>"}),
t({"",""}),t({""}),
t({"",""}),t({"      <button type=\"submit\">Login</button>"}),
t({"",""}),t({"      <!--"}),
t({"",""}),t({"<label>"}),
t({"",""}),t({"<input type=\"checkbox\" checked=\"checked\" name=\"remember\"> Remember me"}),
t({"",""}),t({"</label>"}),
t({"",""}),t({"</div>"}),
t({"",""}),t({""}),
t({"",""}),t({"<div class=\"container\" style=\"background-color:#f1f1f1\">"}),
t({"",""}),t({"<button type=\"button\" class=\"cancelbtn\">Cancel</button>"}),
t({"",""}),t({"<span class=\"psw\">Forgot <a href=\"#\">password?</a></span>"}),
t({"",""}),t({"</div>"}),
t({"",""}),t({"-->"}),
t({"",""}),t({"  </form>"}),
t({"",""}),t({""}),
t({"",""}),t({"</body>"}),
t({"",""}),t({""}),
t({"",""}),t({"</html>"}),
}),
s("script", {
t({"<script src=\""}),i(0),t({"\"></script>"}),
}),
s("wells", {
t({"<div class=\"well well-"}),i(1),t({"\">"}),i(2),t({"</div>"}),
}),
})
ls.add_snippets("javascript", {
s("buttonEvent", {
t({"let "}),i(1),t({" = document.querySelector(\""}),rep(1),t({"\");"}),
t({"",""}),t({""}),rep(1),t({".addEventListener('click', (event) => {"}),
t({"",""}),t({"  const isButton = event.target.nodeName === 'BUTTON';"}),
t({"",""}),t({"  if (!isButton) {"}),
t({"",""}),t({"    return;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  let button = event.target;"}),
t({"",""}),t({"})"}),
}),
s("console", {
t({"console.log(\""}),i(0),t({"\");"}),
}),
s("data-list-force", {
t({""}),
t({"",""}),t({"force_list_selection();"}),
t({"",""}),t({""}),
t({"",""}),t({"function force_list_selection() {"}),
t({"",""}),t({"  // Find all inputs on the DOM which are bound to a datalist via their list attribute."}),
t({"",""}),t({"  let inputs = document.querySelectorAll('input[list]');"}),
t({"",""}),t({"  for (var i = 0; i < inputs.length; i++) {"}),
t({"",""}),t({"    // When the value of the input changes..."}),
t({"",""}),t({"    inputs[i].addEventListener('change', function () {"}),
t({"",""}),t({"      let optionFound = false,"}),
t({"",""}),t({"      datalist = this.list;"}),
t({"",""}),t({"      // Determine whether an option exists with the current value of the input."}),
t({"",""}),t({"      for (var j = 0; j < datalist.options.length; j++) {"}),
t({"",""}),t({"        if (this.value == datalist.options[j].value) {"}),
t({"",""}),t({"          optionFound = true;"}),
t({"",""}),t({"          break;"}),
t({"",""}),t({"        }"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"      // use the setCustomValidity function of the Validation API"}),
t({"",""}),t({"      // to provide an user feedback if the value does not exist in the datalist"}),
t({"",""}),t({"      if (optionFound) {"}),
t({"",""}),t({"        this.setCustomValidity('');"}),
t({"",""}),t({"      } else {"}),
t({"",""}),t({"        this.setCustomValidity('Please select a valid value.');"}),
t({"",""}),t({"        this.value = \"\";"}),
t({"",""}),t({"        this.placeholder = \"Please select a valid value\";"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    });"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
t({"",""}),t({"function validate() {"}),
t({"",""}),t({"  let form = document.querySelector(\"form\");"}),
t({"",""}),t({"  Array.from(form.elements).forEach((input) => {"}),
t({"",""}),t({"    if (input.value == \"\") {"}),
t({"",""}),t({"      alert(`${input.name} needs to be entered.`);"}),
t({"",""}),t({"      return false;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  });"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("data-list-js", {
t({"<label for=\""}),i(1),t({"\">"}),i(2),t({":</label>"}),
t({"",""}),t({"<input list=\""}),i(3),t({"\" id=\""}),rep(1),t({"\" name=\""}),rep(1),t({"\" />"}),
t({"",""}),t({""}),
t({"",""}),t({"<datalist id=\""}),rep(3),t({"\">"}),
t({"",""}),t({"</datalist>"}),
t({"",""}),t({""}),
t({"",""}),t({"<script>"}),
t({"",""}),t({"  function builddata(){"}),
t({"",""}),t({"    let list=document.querySelector(\"#"}),rep(3),t({"\");"}),
t({"",""}),t({"    let dataitems = [ \""}),i(4),t({"\",\""}),i(5),t({"\" ];"}),
t({"",""}),t({"    dataitems.forEach(function(item){"}),
t({"",""}),t({"      list.innerHTML += `<option value=\"${item}\">${item}</option>`;"}),
t({"",""}),t({"    });"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  builddata();"}),
t({"",""}),t({"</script>"}),
}),
s("document", {
t({"document"}),
}),
s("domain_from_url", {
t({"function getDomain(url, subdomain) {"}),
t({"",""}),t({"  subdomain = subdomain || false;"}),
t({"",""}),t({""}),
t({"",""}),t({"  url = url.replace(/(https?:\\/\\/)?(www.)?/i, '');"}),
t({"",""}),t({""}),
t({"",""}),t({"  if (!subdomain) {"}),
t({"",""}),t({"    url = url.split('.');"}),
t({"",""}),t({""}),
t({"",""}),t({"    url = url.slice(url.length - 2).join('.');"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  if (url.indexOf('/') !== -1) {"}),
t({"",""}),t({"    return url.split('/')[0];"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  return url;"}),
t({"",""}),t({"}"}),
}),
s("forof", {
t({"for (const "}),i(1),t({" of "}),i(2),t({") {"}),
t({"",""}),t({"  "}),i(3),t({""}),
t({"",""}),t({"}"}),
}),
s("get_function", {
t({"function get(url, success) {"}),
t({"",""}),t({"  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');"}),
t({"",""}),t({"  xhr.open('GET', url);"}),
t({"",""}),t({"  xhr.onreadystatechange = function() {"}),
t({"",""}),t({"  if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);"}),
t({"",""}),t({"  };"}),
t({"",""}),t({"  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');"}),
t({"",""}),t({"  xhr.send();"}),
t({"",""}),t({"  return xhr;"}),
t({"",""}),t({"}"}),
}),
s("get", {
t({"get(`"}),i(0),t({"`, function(data){"}),
t({"",""}),t({""}),
t({"",""}),t({"});"}),
}),
s("get_json", {
t({"get(`"}),i(0),t({"`, function(data){"}),
t({"",""}),t({"  let json = JSON.parse(data);"}),
t({"",""}),t({"  for( item of json ){"}),
t({"",""}),t({"    console.log(item);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"});"}),
}),
s("include", {
t({"<div w3-include-html=\""}),i(1),t({"\"></div>"}),
t({"",""}),t({"<script>"}),
t({"",""}),t({"  includeHTML();"}),
t({"",""}),t({""}),
t({"",""}),t({"  function includeHTML() {"}),
t({"",""}),t({"    var z, i, elmnt, file, xhttp;"}),
t({"",""}),t({"    /* Loop through a collection of all HTML elements: */"}),
t({"",""}),t({"    z = document.getElementsByTagName(\"*\");"}),
t({"",""}),t({"    for (i = 0; i < z.length; i++) {"}),
t({"",""}),t({"      elmnt = z[i];"}),
t({"",""}),t({"      /*search for elements with a certain atrribute:*/"}),
t({"",""}),t({"      file = elmnt.getAttribute(\"w3-include-html\");"}),
t({"",""}),t({"      if (file) {"}),
t({"",""}),t({"        /* Make an HTTP request using the attribute value as the file name: */"}),
t({"",""}),t({"        xhttp = new XMLHttpRequest();"}),
t({"",""}),t({"        xhttp.onreadystatechange = function() {"}),
t({"",""}),t({"          if (this.readyState == 4) {"}),
t({"",""}),t({"            if (this.status == 200) {elmnt.innerHTML = this.responseText;}"}),
t({"",""}),t({"            if (this.status == 404) {elmnt.innerHTML = \"Page not found.\";}"}),
t({"",""}),t({"            /* Remove the attribute, and call this function once more: */"}),
t({"",""}),t({"            elmnt.removeAttribute(\"w3-include-html\");"}),
t({"",""}),t({"            includeHTML();"}),
t({"",""}),t({"          }"}),
t({"",""}),t({"        }"}),
t({"",""}),t({"        xhttp.open(\"GET\", file, true);"}),
t({"",""}),t({"        xhttp.send();"}),
t({"",""}),t({"        /* Exit the function: */"}),
t({"",""}),t({"        return;"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"</script>"}),
t({"",""}),t({""}),
}),
s("innerHTML", {
t({"innerHTML = "}),i(1),t({";"}),
}),
s("let", {
t({"let "}),i(1),t({" = "}),i(2),t({";"}),
}),
s("querySelectorAll", {
t({"document.querySelectorAll(\""}),i(1),t({"\");"}),
}),
s("querySelectorAll_var", {
t({"let "}),i(1),t({" = document.querySelectorAll(\"."}),rep(1),t({"\");"}),
}),
s("querySelector", {
t({"document.querySelector(\""}),i(1),t({"\");"}),
}),
s("querySelector_var", {
t({"let "}),i(1),t({" = document.querySelector(\"#"}),rep(1),t({"\");"}),
}),
s("random_array", {
t({""}),i(1),t({"[(Math.floor(Math.random() * "}),rep(1),t({".length))];"}),
}),
s("reverse", {
t({"reverse();"}),
}),
s("shuffle_array", {
t({"function shuffle(array) {"}),
t({"",""}),t({"  let currentIndex = array.length;"}),
t({"",""}),t({""}),
t({"",""}),t({"  // While there remain elements to shuffle..."}),
t({"",""}),t({"  while (currentIndex != 0) {"}),
t({"",""}),t({""}),
t({"",""}),t({"    // Pick a remaining element..."}),
t({"",""}),t({"    let randomIndex = Math.floor(Math.random() * currentIndex);"}),
t({"",""}),t({"    currentIndex--;"}),
t({"",""}),t({""}),
t({"",""}),t({"    // And swap it with the current element."}),
t({"",""}),t({"    [array[currentIndex], array[randomIndex]] = ["}),
t({"",""}),t({"      array[randomIndex], array[currentIndex]];"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("sort", {
t({"sort();"}),
}),
s("toSorted", {
t({"toSorted();"}),
}),
s("urlvar", {
t({"function urlvar(v){"}),
t({"",""}),t({"  const queryString = window.location.search;"}),
t({"",""}),t({"  const urlParams = new URLSearchParams(queryString);"}),
t({"",""}),t({"  let value = urlParams.get(v);"}),
t({"",""}),t({"  return value;"}),
t({"",""}),t({"}"}),
}),
})
ls.add_snippets("html", {
s("buttonEvent", {
t({"let "}),i(1),t({" = document.querySelector(\""}),rep(1),t({"\");"}),
t({"",""}),t({""}),rep(1),t({".addEventListener('click', (event) => {"}),
t({"",""}),t({"  const isButton = event.target.nodeName === 'BUTTON';"}),
t({"",""}),t({"  if (!isButton) {"}),
t({"",""}),t({"    return;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  let button = event.target;"}),
t({"",""}),t({"})"}),
}),
s("console", {
t({"console.log(\""}),i(0),t({"\");"}),
}),
s("data-list-force", {
t({""}),
t({"",""}),t({"force_list_selection();"}),
t({"",""}),t({""}),
t({"",""}),t({"function force_list_selection() {"}),
t({"",""}),t({"  // Find all inputs on the DOM which are bound to a datalist via their list attribute."}),
t({"",""}),t({"  let inputs = document.querySelectorAll('input[list]');"}),
t({"",""}),t({"  for (var i = 0; i < inputs.length; i++) {"}),
t({"",""}),t({"    // When the value of the input changes..."}),
t({"",""}),t({"    inputs[i].addEventListener('change', function () {"}),
t({"",""}),t({"      let optionFound = false,"}),
t({"",""}),t({"      datalist = this.list;"}),
t({"",""}),t({"      // Determine whether an option exists with the current value of the input."}),
t({"",""}),t({"      for (var j = 0; j < datalist.options.length; j++) {"}),
t({"",""}),t({"        if (this.value == datalist.options[j].value) {"}),
t({"",""}),t({"          optionFound = true;"}),
t({"",""}),t({"          break;"}),
t({"",""}),t({"        }"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"      // use the setCustomValidity function of the Validation API"}),
t({"",""}),t({"      // to provide an user feedback if the value does not exist in the datalist"}),
t({"",""}),t({"      if (optionFound) {"}),
t({"",""}),t({"        this.setCustomValidity('');"}),
t({"",""}),t({"      } else {"}),
t({"",""}),t({"        this.setCustomValidity('Please select a valid value.');"}),
t({"",""}),t({"        this.value = \"\";"}),
t({"",""}),t({"        this.placeholder = \"Please select a valid value\";"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    });"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
t({"",""}),t({"function validate() {"}),
t({"",""}),t({"  let form = document.querySelector(\"form\");"}),
t({"",""}),t({"  Array.from(form.elements).forEach((input) => {"}),
t({"",""}),t({"    if (input.value == \"\") {"}),
t({"",""}),t({"      alert(`${input.name} needs to be entered.`);"}),
t({"",""}),t({"      return false;"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  });"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("data-list-js", {
t({"<label for=\""}),i(1),t({"\">"}),i(2),t({":</label>"}),
t({"",""}),t({"<input list=\""}),i(3),t({"\" id=\""}),rep(1),t({"\" name=\""}),rep(1),t({"\" />"}),
t({"",""}),t({""}),
t({"",""}),t({"<datalist id=\""}),rep(3),t({"\">"}),
t({"",""}),t({"</datalist>"}),
t({"",""}),t({""}),
t({"",""}),t({"<script>"}),
t({"",""}),t({"  function builddata(){"}),
t({"",""}),t({"    let list=document.querySelector(\"#"}),rep(3),t({"\");"}),
t({"",""}),t({"    let dataitems = [ \""}),i(4),t({"\",\""}),i(5),t({"\" ];"}),
t({"",""}),t({"    dataitems.forEach(function(item){"}),
t({"",""}),t({"      list.innerHTML += `<option value=\"${item}\">${item}</option>`;"}),
t({"",""}),t({"    });"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  builddata();"}),
t({"",""}),t({"</script>"}),
}),
s("document", {
t({"document"}),
}),
s("domain_from_url", {
t({"function getDomain(url, subdomain) {"}),
t({"",""}),t({"  subdomain = subdomain || false;"}),
t({"",""}),t({""}),
t({"",""}),t({"  url = url.replace(/(https?:\\/\\/)?(www.)?/i, '');"}),
t({"",""}),t({""}),
t({"",""}),t({"  if (!subdomain) {"}),
t({"",""}),t({"    url = url.split('.');"}),
t({"",""}),t({""}),
t({"",""}),t({"    url = url.slice(url.length - 2).join('.');"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  if (url.indexOf('/') !== -1) {"}),
t({"",""}),t({"    return url.split('/')[0];"}),
t({"",""}),t({"  }"}),
t({"",""}),t({""}),
t({"",""}),t({"  return url;"}),
t({"",""}),t({"}"}),
}),
s("forof", {
t({"for (const "}),i(1),t({" of "}),i(2),t({") {"}),
t({"",""}),t({"  "}),i(3),t({""}),
t({"",""}),t({"}"}),
}),
s("get_function", {
t({"function get(url, success) {"}),
t({"",""}),t({"  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');"}),
t({"",""}),t({"  xhr.open('GET', url);"}),
t({"",""}),t({"  xhr.onreadystatechange = function() {"}),
t({"",""}),t({"  if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);"}),
t({"",""}),t({"  };"}),
t({"",""}),t({"  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');"}),
t({"",""}),t({"  xhr.send();"}),
t({"",""}),t({"  return xhr;"}),
t({"",""}),t({"}"}),
}),
s("get", {
t({"get(`"}),i(0),t({"`, function(data){"}),
t({"",""}),t({""}),
t({"",""}),t({"});"}),
}),
s("get_json", {
t({"get(`"}),i(0),t({"`, function(data){"}),
t({"",""}),t({"  let json = JSON.parse(data);"}),
t({"",""}),t({"  for( item of json ){"}),
t({"",""}),t({"    console.log(item);"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"});"}),
}),
s("include", {
t({"<div w3-include-html=\""}),i(1),t({"\"></div>"}),
t({"",""}),t({"<script>"}),
t({"",""}),t({"  includeHTML();"}),
t({"",""}),t({""}),
t({"",""}),t({"  function includeHTML() {"}),
t({"",""}),t({"    var z, i, elmnt, file, xhttp;"}),
t({"",""}),t({"    /* Loop through a collection of all HTML elements: */"}),
t({"",""}),t({"    z = document.getElementsByTagName(\"*\");"}),
t({"",""}),t({"    for (i = 0; i < z.length; i++) {"}),
t({"",""}),t({"      elmnt = z[i];"}),
t({"",""}),t({"      /*search for elements with a certain atrribute:*/"}),
t({"",""}),t({"      file = elmnt.getAttribute(\"w3-include-html\");"}),
t({"",""}),t({"      if (file) {"}),
t({"",""}),t({"        /* Make an HTTP request using the attribute value as the file name: */"}),
t({"",""}),t({"        xhttp = new XMLHttpRequest();"}),
t({"",""}),t({"        xhttp.onreadystatechange = function() {"}),
t({"",""}),t({"          if (this.readyState == 4) {"}),
t({"",""}),t({"            if (this.status == 200) {elmnt.innerHTML = this.responseText;}"}),
t({"",""}),t({"            if (this.status == 404) {elmnt.innerHTML = \"Page not found.\";}"}),
t({"",""}),t({"            /* Remove the attribute, and call this function once more: */"}),
t({"",""}),t({"            elmnt.removeAttribute(\"w3-include-html\");"}),
t({"",""}),t({"            includeHTML();"}),
t({"",""}),t({"          }"}),
t({"",""}),t({"        }"}),
t({"",""}),t({"        xhttp.open(\"GET\", file, true);"}),
t({"",""}),t({"        xhttp.send();"}),
t({"",""}),t({"        /* Exit the function: */"}),
t({"",""}),t({"        return;"}),
t({"",""}),t({"      }"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"</script>"}),
t({"",""}),t({""}),
}),
s("innerHTML", {
t({"innerHTML = "}),i(1),t({";"}),
}),
s("let", {
t({"let "}),i(1),t({" = "}),i(2),t({";"}),
}),
s("querySelectorAll", {
t({"document.querySelectorAll(\""}),i(1),t({"\");"}),
}),
s("querySelectorAll_var", {
t({"let "}),i(1),t({" = document.querySelectorAll(\"."}),rep(1),t({"\");"}),
}),
s("querySelector", {
t({"document.querySelector(\""}),i(1),t({"\");"}),
}),
s("querySelector_var", {
t({"let "}),i(1),t({" = document.querySelector(\"#"}),rep(1),t({"\");"}),
}),
s("random_array", {
t({""}),i(1),t({"[(Math.floor(Math.random() * "}),rep(1),t({".length))];"}),
}),
s("reverse", {
t({"reverse();"}),
}),
s("shuffle_array", {
t({"function shuffle(array) {"}),
t({"",""}),t({"  let currentIndex = array.length;"}),
t({"",""}),t({""}),
t({"",""}),t({"  // While there remain elements to shuffle..."}),
t({"",""}),t({"  while (currentIndex != 0) {"}),
t({"",""}),t({""}),
t({"",""}),t({"    // Pick a remaining element..."}),
t({"",""}),t({"    let randomIndex = Math.floor(Math.random() * currentIndex);"}),
t({"",""}),t({"    currentIndex--;"}),
t({"",""}),t({""}),
t({"",""}),t({"    // And swap it with the current element."}),
t({"",""}),t({"    [array[currentIndex], array[randomIndex]] = ["}),
t({"",""}),t({"      array[randomIndex], array[currentIndex]];"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("sort", {
t({"sort();"}),
}),
s("toSorted", {
t({"toSorted();"}),
}),
s("urlvar", {
t({"function urlvar(v){"}),
t({"",""}),t({"  const queryString = window.location.search;"}),
t({"",""}),t({"  const urlParams = new URLSearchParams(queryString);"}),
t({"",""}),t({"  let value = urlParams.get(v);"}),
t({"",""}),t({"  return value;"}),
t({"",""}),t({"}"}),
}),
})
ls.add_snippets("php", {
s("get_form", {
t({"$_GET = array_map('strip_tags', $_GET);"}),
t({"",""}),t({"$_GET = array_map('htmlspecialchars', $_GET);"}),
}),
s("ip_address", {
t({"$ip = $_SERVER['REMOTE_ADDR'];"}),
}),
s("mysql_json", {
t({"// 1: user  2:password 3:database"}),
t({"",""}),t({"$con=mysqli_connect(\"localhost\",\""}),i(1),t({"\",\""}),i(2),t({"\",\""}),i(3),t({"\");"}),
t({"",""}),t({""}),
t({"",""}),t({"// Check connection"}),
t({"",""}),t({"if (mysqli_connect_errno($con))"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"  echo \"Failed to connect to MySQL: \" . mysqli_connect_error();"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({"$table = "}),i(4),t({";"}),
t({"",""}),t({"$result = mysqli_query($con,\"SELECT * FROM $table ORDER BY store\");"}),
t({"",""}),t({"$rows = array();"}),
t({"",""}),t({"while($row = mysqli_fetch_array($result)) {"}),
t({"",""}),t({"    $rows[] = $row;"}),
t({"",""}),t({"}"}),
t({"",""}),t({"print json_encode($rows);"}),
t({"",""}),t({"mysqli_close($con);"}),
}),
s("mysql_submit", {
t({"$_POST = array_map('strip_tags', $_POST);"}),
t({"",""}),t({"$_POST = array_map('htmlspecialchars', $_POST);"}),
t({"",""}),t({""}),
t({"",""}),t({"$_GET = array_map('strip_tags', $_GET);"}),
t({"",""}),t({"$_GET = array_map('htmlspecialchars', $_GET);"}),
t({"",""}),t({""}),
t({"",""}),t({"// 1: user  2:password 3:database"}),
t({"",""}),t({"$con=mysqli_connect(\"localhost\",\""}),i(1),t({"\",\""}),i(2),t({"\",\""}),i(3),t({"\");"}),
t({"",""}),t({""}),
t({"",""}),t({"// Check connection"}),
t({"",""}),t({"if (mysqli_connect_errno($con))"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"  echo \"Failed to connect to MySQL: \" . mysqli_connect_error();"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({"$table = "}),i(4),t({";"}),
t({"",""}),t({"$result = mysqli_query($con,\"SELECT * FROM $table WHERE item='$item' \");"}),
t({"",""}),t({"if( mysqli_num_rows($result) > 0) {"}),
t({"",""}),t({"    print \"updating...\";"}),
t({"",""}),t({"}"}),
t({"",""}),t({"else"}),
t({"",""}),t({"{"}),
t({"",""}),t({"    $sql=\"INSERT INTO $table (timestamp,store,item) VALUES ('$date','$store','$item')\";"}),
t({"",""}),t({"    if (!mysqli_query($con,$sql)) {"}),
t({"",""}),t({"      die('Error: ' . mysqli_error($con));"}),
t({"",""}),t({"    }"}),
t({"",""}),t({"}"}),
t({"",""}),t({"foreach($_POST as $key => $value) {"}),
t({"",""}),t({"    echo 'Current value in $_POST[\"' . $key . '\"] is : ' . $value . '<br>';"}),
t({"",""}),t({"    $entry = mysqli_real_escape_string($con, $value);"}),
t({"",""}),t({"    //$sql=\"UPDATE WHERE pid='$pid' $table ($key) VALUES ('$entry')\";"}),
t({"",""}),t({"    $sql=\"UPDATE $table SET $key='$entry' WHERE item='$item'\";"}),
t({"",""}),t({"    mysqli_query($con,$sql);"}),
t({"",""}),t({"    //if (!mysqli_query($con,$sql)) {"}),
t({"",""}),t({"    //  die('Error: ' . mysqli_error($con));"}),
t({"",""}),t({"    //}"}),
t({"",""}),t({"}"}),
t({"",""}),t({"mysqli_close($con);"}),
t({"",""}),t({""}),
t({"",""}),t({""}),
}),
s("redirect", {
t({"header(\"Location: "}),i(1),t({"\");"}),
t({"",""}),t({"die();"}),
}),
})
ls.add_snippets("sh", {
s("args", {
t({"while getopts "}),i(1),t({" flag; do"}),
t({"",""}),t({"\tcase \"${flag}\" in"}),
t({"",""}),t({"\t"}),i(2),t({") "}),i(3),t({"=${OPTARG} ;;"}),
t({"",""}),t({"\th) help=\"true\" ;;"}),
t({"",""}),t({"\tesac"}),
t({"",""}),t({"done"}),
}),
s("array_loop", {
t({""}),i(1),t({"=(\""}),i(2),t({"\" \""}),i(3),t({"\")"}),
t({"",""}),t({"for "}),i(4),t({" in ${"}),rep(1),t({"[@]}; do"}),
t({"",""}),t({"  echo \"$"}),rep(4),t({"\""}),
t({"",""}),t({"done"}),
}),
s("array", {
t({""}),i(1),t({"=(\""}),i(2),t({"\" \""}),i(3),t({"\")"}),
}),
s("cgi_get_form", {
t({"eval \"$(busybox httpd -d $QUERY_STRING | sed 's/=/=\"/g;s/&/\"\\n/g')\\\"\""}),
}),
s("cgi_header", {
t({"echo -e \"Content-type: text/html\\n\""}),
}),
s("cgi_query", {
t({"$QUERY_STRING"}),
}),
s("colors", {
t({"bold=$(echo -en \"\\e[1m\")"}),
t({"",""}),t({"underline=$(echo -en \"\\e[4m\")"}),
t({"",""}),t({"dim=$(echo -en \"\\e[2m\")"}),
t({"",""}),t({"strickthrough=$(echo -en \"\\e[9m\")"}),
t({"",""}),t({"blink=$(echo -en \"\\e[5m\")"}),
t({"",""}),t({"reverse=$(echo -en \"\\e[7m\")"}),
t({"",""}),t({"hidden=$(echo -en \"\\e[8m\")"}),
t({"",""}),t({"normal=$(echo -en \"\\e[0m\")"}),
t({"",""}),t({"black=$(echo -en \"\\e[30m\")"}),
t({"",""}),t({"red=$(echo -en \"\\e[31m\")"}),
t({"",""}),t({"green=$(echo -en \"\\e[32m\")"}),
t({"",""}),t({"orange=$(echo -en \"\\e[33m\")"}),
t({"",""}),t({"blue=$(echo -en \"\\e[34m\")"}),
t({"",""}),t({"purple=$(echo -en \"\\e[35m\")"}),
t({"",""}),t({"aqua=$(echo -en \"\\e[36m\")"}),
t({"",""}),t({"gray=$(echo -en \"\\e[37m\")"}),
t({"",""}),t({"darkgray=$(echo -en \"\\e[90m\")"}),
t({"",""}),t({"lightred=$(echo -en \"\\e[91m\")"}),
t({"",""}),t({"lightgreen=$(echo -en \"\\e[92m\")"}),
t({"",""}),t({"lightyellow=$(echo -en \"\\e[93m\")"}),
t({"",""}),t({"lightblue=$(echo -en \"\\e[94m\")"}),
t({"",""}),t({"lightpurple=$(echo -en \"\\e[95m\")"}),
t({"",""}),t({"lightaqua=$(echo -en \"\\e[96m\")"}),
t({"",""}),t({"white=$(echo -en \"\\e[97m\")"}),
t({"",""}),t({"default=$(echo -en \"\\e[39m\")"}),
t({"",""}),t({"BLACK=$(echo -en \"\\e[40m\")"}),
t({"",""}),t({"RED=$(echo -en \"\\e[41m\")"}),
t({"",""}),t({"GREEN=$(echo -en \"\\e[42m\")"}),
t({"",""}),t({"ORANGE=$(echo -en \"\\e[43m\")"}),
t({"",""}),t({"BLUE=$(echo -en \"\\e[44m\")"}),
t({"",""}),t({"PURPLE=$(echo -en \"\\e[45m\")"}),
t({"",""}),t({"AQUA=$(echo -en \"\\e[46m\")"}),
t({"",""}),t({"GRAY=$(echo -en \"\\e[47m\")"}),
t({"",""}),t({"DARKGRAY=$(echo -en \"\\e[100m\")"}),
t({"",""}),t({"LIGHTRED=$(echo -en \"\\e[101m\")"}),
t({"",""}),t({"LIGHTGREEN=$(echo -en \"\\e[102m\")"}),
t({"",""}),t({"LIGHTYELLOW=$(echo -en \"\\e[103m\")"}),
t({"",""}),t({"LIGHTBLUE=$(echo -en \"\\e[104m\")"}),
t({"",""}),t({"LIGHTPURPLE=$(echo -en \"\\e[105m\")"}),
t({"",""}),t({"LIGHTAQUA=$(echo -en \"\\e[106m\")"}),
t({"",""}),t({"WHITE=$(echo -en \"\\e[107m\")"}),
t({"",""}),t({"DEFAULT=$(echo -en \"\\e[49m\")"}),
}),
s("do", {
t({"do"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"done"}),
}),
s("error", {
t({"# fatal uses SIGUSR1 to allow clean fatal errors"}),
t({"",""}),t({"trap \"exit 1\" 10"}),
t({"",""}),t({"PROC=$$"}),
t({"",""}),t({""}),
t({"",""}),t({"function error() {"}),
t({"",""}),t({"\tred=$(echo -en \"\\e[31m\")"}),
t({"",""}),t({"\tnormal=$(echo -en \"\\e[0m\")"}),
t({"",""}),t({""}),
t({"",""}),t({"\techo -e \"${red}$@${normal}\" >&2"}),
t({"",""}),t({"\t#  exit 1"}),
t({"",""}),t({"\tkill -10 $PROC"}),
t({"",""}),t({"}"}),
}),
s("fix_strings", {
t({"jq -Ra ."}),
}),
s("function", {
t({"function "}),i(1),t({"() {"}),
t({"",""}),t({"\t"}),i(2),t({""}),
t({"",""}),t({"}"}),
}),
s("gpl", {
t({"######################################################################"}),
t({"",""}),t({"#Copyright (C) "}),f(year),t({" "}),i(1),t({""}),
t({"",""}),t({"#"}),i(2),t({""}),
t({"",""}),t({"#This program is free software: you can redistribute it and/or modify"}),
t({"",""}),t({"#it under the terms of the GNU General Public License as published by"}),
t({"",""}),t({"#the Free Software Foundation version 3 of the License."}),
t({"",""}),t({""}),
t({"",""}),t({"#This program is distributed in the hope that it will be useful,"}),
t({"",""}),t({"#but WITHOUT ANY WARRANTY; without even the implied warranty of"}),
t({"",""}),t({"#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"}),
t({"",""}),t({"#GNU General Public License for more details."}),
t({"",""}),t({""}),
t({"",""}),t({"#You should have received a copy of the GNU General Public License"}),
t({"",""}),t({"#along with this program.  If not, see <http://www.gnu.org/licenses/>."}),
t({"",""}),t({"######################################################################"}),
}),
s("history_zsh", {
t({"echo \": $(date +%s):0;"}),i(1),t({"\" >>$HOME/.zsh_history"}),
}),
s("ifroot", {
t({"if [ \"$EUID\" -ne 0 ]; then"}),
t({"",""}),t({"\techo \"Please run as root\""}),
t({"",""}),t({"\texit"}),
t({"",""}),t({"fi"}),
}),
s("line", {
t({"function line() {"}),
t({"",""}),t({"\tif [[ $# < 1 ]]; then"}),
t({"",""}),t({"\t\tl=\"=\""}),
t({"",""}),t({"\telse"}),
t({"",""}),t({"\t\tl=\"${1:0:1}\""}),
t({"",""}),t({"\tfi"}),
t({"",""}),t({""}),
t({"",""}),t({"\teval printf %.0s$l '{1..'\"${COLUMNS:-$(tput cols)}\"\\}"}),
t({"",""}),t({"\techo \"\""}),
t({"",""}),t({"}"}),
}),
s("rootcheck", {
t({"if [ \"$EUID\" -ne 0 ]; then"}),
t({"",""}),t({"\techo \"Please run as root\""}),
t({"",""}),t({"\texit"}),
t({"",""}),t({"fi"}),
}),
s("wget2file", {
t({"wget \""}),i(1),t({"\" -O \""}),i(2),t({"\""}),
}),
s("wget", {
t({"wget -qO- \""}),i(0),t({"\""}),
}),
})
ls.add_snippets("zscript", {
s("$FN_ZOMBIE", {
t({"$FN_ZOMBIE"}),
}),
s("$OB_ZOMBIE", {
t({"$OB_ZOMBIE"}),
}),
s("AABBCCDD", {
t({"AABBCCDD"}),
}),
s("A_ActiveAndUnblock", {
t({"A_ActiveAndUnblock"}),
}),
s("A_ActiveSound", {
t({"A_ActiveSound"}),
}),
s("AAPTR_DEFAULT", {
t({"AAPTR_DEFAULT"}),
}),
s("A_AttachLight", {
t({"A_AttachLight"}),
}),
s("A_AttachLightDef", {
t({"A_AttachLightDef"}),
}),
s("AB", {
t({"AB"}),
}),
s("A_BossDeath", {
t({"A_BossDeath"}),
}),
s("AbsAngle", {
t({"AbsAngle"}),
}),
s("absolute", {
t({"Level.absolute"}),
}),
s("A_BulletAttack", {
t({"A_BulletAttack"}),
}),
s("A_Burst", {
t({"A_Burst"}),
}),
s("A_CallSpecial", {
t({"A_CallSpecial"}),
}),
s("AccuracyFactor", {
t({"AccuracyFactor"}),
}),
s("A_ChangeCountFlags", {
t({"A_ChangeCountFlags"}),
}),
s("A_ChangeFlag", {
t({"A_ChangeFlag"}),
}),
s("A_ChangeLinkFlags", {
t({"A_ChangeLinkFlags"}),
}),
s("A_Chase", {
t({"A_Chase"}),
}),
s("A_CheckForResurrection", {
t({"A_CheckForResurrection"}),
}),
s("A_CheckPlayerDone", {
t({"A_CheckPlayerDone"}),
}),
s("A_CheckTerrain", {
t({"A_CheckTerrain"}),
}),
s("A_ClassBossHealth", {
t({"A_ClassBossHealth"}),
}),
s("A_ClearLastHeard", {
t({"A_ClearLastHeard"}),
}),
s("A_ClearOverlays", {
t({"A_ClearOverlays"}),
}),
s("A_ClearTarget", {
t({"A_ClearTarget"}),
}),
s("A_CopyFriendliness", {
t({"A_CopyFriendliness"}),
}),
s("A_CopySpriteFrame", {
t({"A_CopySpriteFrame"}),
}),
s("A_CountdownArg", {
t({"A_CountdownArg"}),
}),
s("action", {
t({"action"}),
}),
s("ActiveAndUnblock", {
t({"A_ActiveAndUnblock "}),i(1),t({"; //https://zdoom.org/wiki/A_ActiveAndUnblock"}),
}),
s("ActiveSound", {
t({"A_ActiveSound "}),i(1),t({"; //https://zdoom.org/wiki/A_ActiveSound"}),
}),
s("Actor", {
t({"Actor"}),
}),
s("actorsonly", {
t({"actorsonly"}),
}),
s("ActOwnSpecial", {
t({"Level.ActOwnSpecial"}),
}),
s("A_CustomBulletAttack", {
t({"A_CustomBulletAttack"}),
}),
s("A_CustomComboAttack", {
t({"A_CustomComboAttack"}),
}),
s("A_CustomMeleeAttack", {
t({"A_CustomMeleeAttack"}),
}),
s("A_CustomRailgun", {
t({"A_CustomRailgun"}),
}),
s("A_DamageChildren", {
t({"A_DamageChildren"}),
}),
s("A_DamageMaster", {
t({"A_DamageMaster"}),
}),
s("A_DamageSelf", {
t({"A_DamageSelf"}),
}),
s("A_DamageSiblings", {
t({"A_DamageSiblings"}),
}),
s("A_DamageTarget", {
t({"A_DamageTarget"}),
}),
s("A_DamageTracer", {
t({"A_DamageTracer"}),
}),
s("A_DequeueCorpse", {
t({"A_DequeueCorpse"}),
}),
s("A_Die", {
t({"A_Die"}),
}),
s("AdjustFloorClip", {
t({"AdjustFloorClip"}),
}),
s("AdjustPlayerAngle", {
t({"AdjustPlayerAngle"}),
}),
s("A_DoChase", {
t({"A_DoChase"}),
}),
s("A_DropInventory", {
t({"A_DropInventory"}),
}),
s("A_DropItem", {
t({"A_DropItem"}),
}),
s("A_ExtChase", {
t({"A_ExtChase"}),
}),
s("A_Face", {
t({"A_Face"}),
}),
s("A_FaceConsolePlayer", {
t({"A_FaceConsolePlayer"}),
}),
s("A_FaceMaster", {
t({"A_FaceMaster"}),
}),
s("A_FaceMovementDirection", {
t({"A_FaceMovementDirection"}),
}),
s("A_FaceTarget", {
t({"A_FaceTarget"}),
}),
s("A_FaceTracer", {
t({"A_FaceTracer"}),
}),
s("A_FadeIn", {
t({"A_FadeIn"}),
}),
s("A_FadeOut", {
t({"A_FadeOut"}),
}),
s("A_FadeTo", {
t({"A_FadeTo"}),
}),
s("A_Fall", {
t({"A_Fall"}),
}),
s("A_FastChase", {
t({"A_FastChase"}),
}),
s("A_FreezeDeathChunks;", {
t({"A_FreezeDeathChunks;"}),
}),
s("A_GenericFreezeDeath;", {
t({"A_GenericFreezeDeath;"}),
}),
s("A_GiveInventory", {
t({"A_GiveInventory"}),
}),
s("A_GiveToChildren", {
t({"A_GiveToChildren"}),
}),
s("A_GiveToSiblings", {
t({"A_GiveToSiblings"}),
}),
s("A_GiveToTarget", {
t({"A_GiveToTarget"}),
}),
s("A_Gravity", {
t({"A_Gravity"}),
}),
s("A_HideThing", {
t({"A_HideThing"}),
}),
s("AimBulletMissile", {
t({"AimBulletMissile"}),
}),
s("aimflags", {
t({"aimflags"}),
}),
s("AimLineAttack", {
t({"AimLineAttack"}),
}),
s("aimpitch", {
t({"aimpitch"}),
}),
s("AimTarget", {
t({"AimTarget"}),
}),
s("AirControl", {
t({"Level.AirControl"}),
}),
s("AirFriction", {
t({"Level.AirFriction"}),
}),
s("AirSupply", {
t({"Level.AirSupply"}),
}),
s("A_Jump", {
t({"A_Jump"}),
}),
s("A_JumpIf", {
t({"A_JumpIf"}),
}),
s("A_JumpIfCloser", {
t({"A_JumpIfCloser"}),
}),
s("A_JumpIfHealthLower", {
t({"A_JumpIfHealthLower"}),
}),
s("A_JumpIfMasterCloser", {
t({"A_JumpIfMasterCloser"}),
}),
s("A_JumpIfTargetInsideMeleeRange", {
t({"A_JumpIfTargetInsideMeleeRange"}),
}),
s("A_JumpIfTargetOutsideMeleeRange", {
t({"A_JumpIfTargetOutsideMeleeRange"}),
}),
s("A_JumpIfTracerCloser", {
t({"A_JumpIfTracerCloser"}),
}),
s("A_KillChildren", {
t({"A_KillChildren"}),
}),
s("A_KillMaster", {
t({"A_KillMaster"}),
}),
s("A_KillSiblings", {
t({"A_KillSiblings"}),
}),
s("A_KillTarget", {
t({"A_KillTarget"}),
}),
s("A_KillTracer", {
t({"A_KillTracer"}),
}),
s("alert", {
t({"alert"}),
}),
s("AlertMonsters", {
t({"A_AlertMonsters "}),i(1),t({"; //https://zdoom.org/wiki/A_AlertMonsters"}),
}),
s("A_Light", {
t({"A_Light"}),
}),
s("A_Light0", {
t({"A_Light0"}),
}),
s("A_Light1", {
t({"A_Light1"}),
}),
s("A_Light2", {
t({"A_Light2"}),
}),
s("A_LightInverse", {
t({"A_LightInverse"}),
}),
s("A_LineEffect", {
t({"A_LineEffect"}),
}),
s("allaround", {
t({"allaround"}),
}),
s("Allmap", {
t({"Allmap"}),
}),
s("AllMap", {
t({"Level.AllMap"}),
}),
s("Allmap", {
t({"class Allmap : MapRevealer "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.FANCYPICKUPSOUND"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\tInventory.MaxAmount 0;"}),
t({"",""}),t({"\t\tInventory.PickupSound \"misc/p_pkup\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTMAP\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPMAP ABCDCB 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("AllowRespawn", {
t({"Level.AllowRespawn"}),
}),
s("A_Log", {
t({"A_Log"}),
}),
s("A_LogFloat", {
t({"A_LogFloat"}),
}),
s("A_LogInt", {
t({"A_LogInt"}),
}),
s("A_Look", {
t({"A_Look"}),
}),
s("A_Look2", {
t({"A_Look2"}),
}),
s("A_LookEx", {
t({"A_LookEx"}),
}),
s("A_LookEx", {
t({"A_LookEx."}),
}),
s("A_LowGravity", {
t({"A_LowGravity"}),
}),
s("A_MonsterRail", {
t({"A_MonsterRail"}),
}),
s("A_MonsterReFire", {
t({"A_MonsterReFire"}),
}),
s("amount", {
t({"amount"}),
}),
s("angle", {
t({"angle"}),
}),
s("AngleTo", {
t({"AngleTo"}),
}),
s("AngleToVector", {
t({"AngleToVector"}),
}),
s("ang_offset", {
t({"ang_offset"}),
}),
s("A_NoBlocking", {
t({"A_NoBlocking"}),
}),
s("A_NoBlocking", {
t({"A_NoBlocking."}),
}),
s("A_NoGravity", {
t({"A_NoGravity"}),
}),
s("A_Overlay", {
t({"A_Overlay"}),
}),
s("A_OverlayAlpha", {
t({"A_OverlayAlpha"}),
}),
s("A_OverlayFlags", {
t({"A_OverlayFlags"}),
}),
s("A_OverlayOffset", {
t({"A_OverlayOffset"}),
}),
s("A_OverlayPivot", {
t({"A_OverlayPivot"}),
}),
s("A_OverlayPivotAlign", {
t({"A_OverlayPivotAlign"}),
}),
s("A_OverlayRenderStyle", {
t({"A_OverlayRenderStyle"}),
}),
s("A_OverlayRotate", {
t({"A_OverlayRotate"}),
}),
s("A_OverlayScale", {
t({"A_OverlayScale"}),
}),
s("A_OverlayTranslation", {
t({"A_OverlayTranslation"}),
}),
s("A_OverlayVertexOffset", {
t({"A_OverlayVertexOffset"}),
}),
s("A_Pain", {
t({"A_Pain"}),
}),
s("A_PlayerScream", {
t({"A_PlayerScream"}),
}),
s("A_PlaySound", {
t({"A_PlaySound"}),
}),
s("A_PlaySoundEx", {
t({"A_PlaySoundEx"}),
}),
s("A_PlayWeaponSound", {
t({"A_PlayWeaponSound"}),
}),
s("ApplyDamageFactor", {
t({"ApplyDamageFactor"}),
}),
s("ApplyDamageFactors", {
t({"ApplyDamageFactors"}),
}),
s("A_Print", {
t({"A_Print"}),
}),
s("A_PrintBold", {
t({"A_PrintBold"}),
}),
s("A_Quake", {
t({"A_Quake"}),
}),
s("A_QuakeEx", {
t({"A_QuakeEx"}),
}),
s("A_QueueCorpse", {
t({"A_QueueCorpse"}),
}),
s("Arachnotron", {
t({"Arachnotron"}),
}),
s("ArachnotronPlasma", {
t({"ArachnotronPlasma"}),
}),
s("Arachnotron", {
t({"class Arachnotron : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 500;"}),
t({"",""}),t({"\t\tRadius 64;"}),
t({"",""}),t({"\t\tHeight 64;"}),
t({"",""}),t({"\t\tMass 600;"}),
t({"",""}),t({"\t\tSpeed 12;"}),
t({"",""}),t({"\t\tPainChance 128;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\t+MAP07BOSS2"}),
t({"",""}),t({"\t\tSeeSound \"baby/sight\";"}),
t({"",""}),t({"\t\tPainSound \"baby/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"baby/death\";"}),
t({"",""}),t({"\t\tActiveSound \"baby/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_BABY\";"}),
t({"",""}),t({"\t\tTag \"$FN_ARACH\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBSPI AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tBSPI A 20;"}),
t({"",""}),t({"\t\tBSPI A 3 A_BabyMetal;"}),
t({"",""}),t({"\t\tBSPI ABBCC 3 A_Chase;"}),
t({"",""}),t({"\t\tBSPI D 3 A_BabyMetal;"}),
t({"",""}),t({"\t\tBSPI DEEFF 3 A_Chase;"}),
t({"",""}),t({"\t\tGoto See+1;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tBSPI A 20 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tBSPI G 4 BRIGHT A_BspiAttack;"}),
t({"",""}),t({"\t\tBSPI H 4 BRIGHT;"}),
t({"",""}),t({"\t\tBSPI H 1 BRIGHT A_SpidRefire;"}),
t({"",""}),t({"\t\tGoto Missile+1;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tBSPI I 3;"}),
t({"",""}),t({"\t\tBSPI I 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See+1;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBSPI J 20 A_Scream;"}),
t({"",""}),t({"\t\tBSPI K 7 A_NoBlocking;"}),
t({"",""}),t({"\t\tBSPI LMNO 7;"}),
t({"",""}),t({"\t\tBSPI P -1 A_BossDeath;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"    Raise:"}),
t({"",""}),t({"\t\tBSPI P 5;"}),
t({"",""}),t({"\t\tBSPI ONMLKJ 5;"}),
t({"",""}),t({"\t\tGoto See+1;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("A_RadiusDamageSelf", {
t({"A_RadiusDamageSelf"}),
}),
s("A_RadiusGive", {
t({"A_RadiusGive"}),
}),
s("A_RaiseChildren", {
t({"A_RaiseChildren"}),
}),
s("A_RaiseMaster", {
t({"A_RaiseMaster"}),
}),
s("A_RaiseSelf", {
t({"A_RaiseSelf"}),
}),
s("A_RaiseSiblings", {
t({"A_RaiseSiblings"}),
}),
s("Archvile", {
t({"Archvile"}),
}),
s("ArchVile", {
t({"ArchVile"}),
}),
s("ArchvileFire", {
t({"ArchvileFire"}),
}),
s("ArchvileFire", {
t({"class ArchvileFire : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+NOBLOCKMAP +NOGRAVITY +ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t\tAlpha 1;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tFIRE A 2 BRIGHT  A_StartFire;"}),
t({"",""}),t({"\t\tFIRE BAB 2 BRIGHT  A_Fire;"}),
t({"",""}),t({"\t\tFIRE C 2 BRIGHT  A_FireCrackle;"}),
t({"",""}),t({"\t\tFIRE BCBCDCDCDEDED 2 BRIGHT  A_Fire;"}),
t({"",""}),t({"\t\tFIRE E 2 BRIGHT  A_FireCrackle;"}),
t({"",""}),t({"\t\tFIRE FEFEFGHGHGH 2 BRIGHT  A_Fire;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Archvile", {
t({"class Archvile : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 700;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 500;"}),
t({"",""}),t({"\t\tSpeed 15;"}),
t({"",""}),t({"\t\tPainChance 10;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\tMaxTargetRange 896;"}),
t({"",""}),t({"\t\t+QUICKTORETALIATE "}),
t({"",""}),t({"\t\t+FLOORCLIP "}),
t({"",""}),t({"\t\t+NOTARGET"}),
t({"",""}),t({"\t\tSeeSound \"vile/sight\";"}),
t({"",""}),t({"\t\tPainSound \"vile/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"vile/death\";"}),
t({"",""}),t({"\t\tActiveSound \"vile/active\";"}),
t({"",""}),t({"\t\tMeleeSound \"vile/stop\";"}),
t({"",""}),t({"\t\tObituary \"$OB_VILE\";"}),
t({"",""}),t({"\t\tTag \"$FN_ARCH\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tVILE AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tVILE AABBCCDDEEFF 2 A_VileChase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tVILE G 0 BRIGHT A_VileStart;"}),
t({"",""}),t({"\t\tVILE G 10 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tVILE H 8 BRIGHT A_VileTarget;"}),
t({"",""}),t({"\t\tVILE IJKLMN 8 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tVILE O 8 BRIGHT A_VileAttack;"}),
t({"",""}),t({"\t\tVILE P 20 BRIGHT;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tHeal:"}),
t({"",""}),t({"\t\tVILE [\\] 10 BRIGHT;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tVILE Q 5;"}),
t({"",""}),t({"\t\tVILE Q 5 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tVILE Q 7;"}),
t({"",""}),t({"\t\tVILE R 7 A_Scream;"}),
t({"",""}),t({"\t\tVILE S 7 A_NoBlocking;"}),
t({"",""}),t({"\t\tVILE TUVWXY 7;"}),
t({"",""}),t({"\t\tVILE Z -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("A_RearrangePointers", {
t({"A_RearrangePointers"}),
}),
s("A_Recoil", {
t({"A_Recoil"}),
}),
s("A_Remove", {
t({"A_Remove"}),
}),
s("A_RemoveChildren", {
t({"A_RemoveChildren"}),
}),
s("A_RemoveLight", {
t({"A_RemoveLight"}),
}),
s("A_RemoveMaster", {
t({"A_RemoveMaster"}),
}),
s("A_RemoveSiblings", {
t({"A_RemoveSiblings"}),
}),
s("A_RemoveTarget", {
t({"A_RemoveTarget"}),
}),
s("A_RemoveTracer", {
t({"A_RemoveTracer"}),
}),
s("A_ResetHealth", {
t({"A_ResetHealth"}),
}),
s("A_Respawn", {
t({"A_Respawn"}),
}),
s("A_RestoreSpecialPosition", {
t({"A_RestoreSpecialPosition"}),
}),
s("arg1", {
t({"Level.arg1"}),
}),
s("arg2", {
t({"Level.arg2"}),
}),
s("arg3", {
t({"Level.arg3"}),
}),
s("arg4", {
t({"Level.arg4"}),
}),
s("arg5", {
t({"Level.arg5"}),
}),
s("ArmorBonus", {
t({"ArmorBonus"}),
}),
s("ArmorBonus", {
t({"class ArmorBonus : BasicArmorBonus "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 16;"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTARMBONUS\";"}),
t({"",""}),t({"\t\tInventory.Icon \"BON2A0\";"}),
t({"",""}),t({"\t\tArmor.Savepercent 33.335;"}),
t({"",""}),t({"\t\tArmor.Saveamount 1;"}),
t({"",""}),t({"\t\tArmor.Maxsaveamount 200;"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBON2 ABCDCB 6;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("A_Scream", {
t({"A_Scream"}),
}),
s("A_ScreamAndUnblock", {
t({"A_ScreamAndUnblock"}),
}),
s("A_SeekerMissile", {
t({"A_SeekerMissile"}),
}),
s("A_SelectWeapon", {
t({"A_SelectWeapon"}),
}),
s("A_SetAngle", {
t({"A_SetAngle"}),
}),
s("A_SetArg", {
t({"A_SetArg"}),
}),
s("A_SetBlend", {
t({"A_SetBlend"}),
}),
s("A_SetChaseThreshold", {
t({"A_SetChaseThreshold"}),
}),
s("A_SetDamageType", {
t({"A_SetDamageType"}),
}),
s("A_SetFloat", {
t({"A_SetFloat"}),
}),
s("A_SetFloatBobPhase", {
t({"A_SetFloatBobPhase"}),
}),
s("A_SetFloatSpeed", {
t({"A_SetFloatSpeed"}),
}),
s("A_SetFloorClip", {
t({"A_SetFloorClip"}),
}),
s("A_SetGravity", {
t({"A_SetGravity"}),
}),
s("A_SetHealth", {
t({"A_SetHealth"}),
}),
s("A_SetInventory", {
t({"A_SetInventory"}),
}),
s("A_SetInvulnerable", {
t({"A_SetInvulnerable"}),
}),
s("A_SetMass", {
t({"A_SetMass"}),
}),
s("A_SetMugShotState", {
t({"A_SetMugShotState"}),
}),
s("A_SetPainThreshold", {
t({"A_SetPainThreshold"}),
}),
s("A_SetPitch", {
t({"A_SetPitch"}),
}),
s("A_SetReflective", {
t({"A_SetReflective"}),
}),
s("A_SetReflectiveInvulnerable", {
t({"A_SetReflectiveInvulnerable"}),
}),
s("A_SetRenderStyle", {
t({"A_SetRenderStyle"}),
}),
s("A_SetRipMax", {
t({"A_SetRipMax"}),
}),
s("A_SetRipMin", {
t({"A_SetRipMin"}),
}),
s("A_SetRipperLevel", {
t({"A_SetRipperLevel"}),
}),
s("A_SetRoll", {
t({"A_SetRoll"}),
}),
s("A_SetScale", {
t({"A_SetScale"}),
}),
s("A_SetShootable", {
t({"A_SetShootable"}),
}),
s("A_SetSize", {
t({"A_SetSize"}),
}),
s("A_SetSolid", {
t({"A_SetSolid"}),
}),
s("A_SetSpecial", {
t({"A_SetSpecial"}),
}),
s("A_SetSpecies", {
t({"A_SetSpecies"}),
}),
s("A_SetSpeed", {
t({"A_SetSpeed"}),
}),
s("A_SetSpriteAngle", {
t({"A_SetSpriteAngle"}),
}),
s("A_SetSpriteRotation", {
t({"A_SetSpriteRotation"}),
}),
s("A_SetTeleFog", {
t({"A_SetTeleFog"}),
}),
s("A_SetTics", {
t({"A_SetTics"}),
}),
s("A_SetTranslation", {
t({"A_SetTranslation"}),
}),
s("A_SetTranslucent", {
t({"A_SetTranslucent"}),
}),
s("A_SetUserArray", {
t({"A_SetUserArray"}),
}),
s("A_SetUserArrayFloat", {
t({"A_SetUserArrayFloat"}),
}),
s("A_SetUserVar", {
t({"A_SetUserVar"}),
}),
s("A_SetUserVarFloat", {
t({"A_SetUserVarFloat"}),
}),
s("A_SetViewAngle", {
t({"A_SetViewAngle"}),
}),
s("A_SetViewPitch", {
t({"A_SetViewPitch"}),
}),
s("A_SetViewRoll", {
t({"A_SetViewRoll"}),
}),
s("A_SetVisibleRotation", {
t({"A_SetVisibleRotation"}),
}),
s("A_SoundPitch", {
t({"A_SoundPitch"}),
}),
s("A_SoundVolume", {
t({"A_SoundVolume"}),
}),
s("A_SpawnDebris", {
t({"A_SpawnDebris"}),
}),
s("A_SpawnParticle", {
t({"A_SpawnParticle"}),
}),
s("A_SpawnProjectile", {
t({"A_SpawnProjectile"}),
}),
s("A_SprayDecal", {
t({"A_SprayDecal"}),
}),
s("A_StartSound", {
t({"A_StartSound"}),
}),
s("A_StopAllSounds", {
t({"A_StopAllSounds"}),
}),
s("A_StopSound", {
t({"A_StopSound"}),
}),
s("A_StopSoundEx", {
t({"A_StopSoundEx"}),
}),
s("A_StopSounds", {
t({"A_StopSounds"}),
}),
s("A_SwapTeleFog", {
t({"A_SwapTeleFog"}),
}),
s("A_TakeFromChildren", {
t({"A_TakeFromChildren"}),
}),
s("A_TakeFromSiblings", {
t({"A_TakeFromSiblings"}),
}),
s("A_TakeFromTarget", {
t({"A_TakeFromTarget"}),
}),
s("A_TakeInventory", {
t({"A_TakeInventory"}),
}),
s("A_Teleport", {
t({"A_Teleport"}),
}),
s("A_TransferPointer", {
t({"A_TransferPointer"}),
}),
s("AttachLight", {
t({"A_AttachLight "}),i(1),t({"; //https://zdoom.org/wiki/A_AttachLight"}),
}),
s("AttachLightDef", {
t({"A_AttachLightDef "}),i(1),t({"; //https://zdoom.org/wiki/A_AttachLightDef"}),
}),
s("Attack", {
t({"Attack"}),
}),
s("AttackSound", {
t({"AttackSound"}),
}),
s("attenuation", {
t({"attenuation"}),
}),
s("ATTN_NORM", {
t({"ATTN_NORM"}),
}),
s("A_Turn", {
t({"A_Turn"}),
}),
s("atZ", {
t({"Level.atZ"}),
}),
s("A_UnHideThing", {
t({"A_UnHideThing"}),
}),
s("A_UnSetFloat", {
t({"A_UnSetFloat"}),
}),
s("A_UnSetFloorClip", {
t({"A_UnSetFloorClip"}),
}),
s("A_UnSetInvulnerable", {
t({"A_UnSetInvulnerable"}),
}),
s("A_UnSetReflective", {
t({"A_UnSetReflective"}),
}),
s("A_UnSetReflectiveInvulnerable", {
t({"A_UnSetReflectiveInvulnerable"}),
}),
s("A_UnSetShootable", {
t({"A_UnSetShootable"}),
}),
s("A_UnSetSolid", {
t({"A_UnSetSolid"}),
}),
s("AuthorName", {
t({"Level.AuthorName"}),
}),
s("A_VileChase", {
t({"A_VileChase"}),
}),
s("A_Wander", {
t({"A_Wander"}),
}),
s("A_Warp", {
t({"A_Warp"}),
}),
s("A_WeaponOffset", {
t({"A_WeaponOffset"}),
}),
s("A_Weave", {
t({"A_Weave"}),
}),
s("A_WolfAttack", {
t({"A_WolfAttack"}),
}),
s("A_XScream", {
t({"A_XScream"}),
}),
s("BabyMetal", {
t({"A_BabyMetal "}),i(1),t({"; //https://zdoom.org/wiki/A_BabyMetal"}),
}),
s("Backpack", {
t({"Backpack"}),
}),
s("Backpack", {
t({"class Backpack : BackpackItem "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHeight 26;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTBACKPACK\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBPAK A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("BaronBall", {
t({"BaronBall"}),
}),
s("BaronBall", {
t({"class BaronBall : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 6;"}),
t({"",""}),t({"\t\tHeight 16;"}),
t({"",""}),t({"\t\tSpeed 15;"}),
t({"",""}),t({"\t\tFastSpeed 20;"}),
t({"",""}),t({"\t\tDamage 8;"}),
t({"",""}),t({"\t\tProjectile ;"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t\tAlpha 1;"}),
t({"",""}),t({"\t\tSeeSound \"baron/attack\";"}),
t({"",""}),t({"\t\tDeathSound \"baron/shotx\";"}),
t({"",""}),t({"\t\tDecal \"BaronScorch\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBAL7 AB 4 BRIGHT;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBAL7 CDE 6 BRIGHT;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BaronofHell", {
t({"BaronofHell"}),
}),
s("BaronOfHell", {
t({"BaronOfHell"}),
}),
s("BaronOfHell", {
t({"class BaronOfHell : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 1000;"}),
t({"",""}),t({"\t\tRadius 24;"}),
t({"",""}),t({"\t\tHeight 64;"}),
t({"",""}),t({"\t\tMass 1000;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 50;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\t+E1M8BOSS"}),
t({"",""}),t({"\t\tSeeSound \"baron/sight\";"}),
t({"",""}),t({"\t\tPainSound \"baron/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"baron/death\";"}),
t({"",""}),t({"\t\tActiveSound \"baron/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_BARON\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_BARONHIT\";"}),
t({"",""}),t({"\t\tTag \"$FN_BARON\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBOSS AB 10 A_Look ;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tBOSS AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tBOSS EF 8 A_FaceTarget;"}),
t({"",""}),t({"\t\tBOSS G 8 A_BruisAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tBOSS H  2;"}),
t({"",""}),t({"\t\tBOSS H  2 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBOSS I  8;"}),
t({"",""}),t({"\t\tBOSS J  8 A_Scream;"}),
t({"",""}),t({"\t\tBOSS K  8;"}),
t({"",""}),t({"\t\tBOSS L  8 A_NoBlocking;"}),
t({"",""}),t({"\t\tBOSS MN 8;"}),
t({"",""}),t({"\t\tBOSS O -1 A_BossDeath;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tBOSS O 8;"}),
t({"",""}),t({"\t\tBOSS NMLKJI  8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BarrelDestroy", {
t({"A_BarrelDestroy "}),i(1),t({"; //https://zdoom.org/wiki/A_BarrelDestroy"}),
}),
s("barron", {
t({"class "}),i(1),t({" : Actor Replaces BaronOfHell  "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 1000;"}),
t({"",""}),t({"\t\tRadius 24;"}),
t({"",""}),t({"\t\tHeight 64;"}),
t({"",""}),t({"\t\tMass 1000;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 50;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\tSeeSound \"baron/sight\";"}),
t({"",""}),t({"\t\tPainSound \"baron/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"baron/death\";"}),
t({"",""}),t({"\t\tActiveSound \"baron/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_BARON\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_BARONHIT\";"}),
t({"",""}),t({"\t\tTag \"$FN_BARON\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBOSS AB 10 A_Look ;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tBOSS AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tBOSS EF 8 A_FaceTarget;"}),
t({"",""}),t({"\t\tBOSS G 8 A_BruisAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tBOSS H  2;"}),
t({"",""}),t({"\t\tBOSS H  2 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBOSS I  8;"}),
t({"",""}),t({"\t\tBOSS J  8 A_Scream;"}),
t({"",""}),t({"\t\tBOSS K  8;"}),
t({"",""}),t({"\t\tBOSS L  8 A_NoBlocking;"}),
t({"",""}),t({"\t\tBOSS MN 8;"}),
t({"",""}),t({"\t\tBOSS O -1 A_BossDeath;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tBOSS O 8;"}),
t({"",""}),t({"\t\tBOSS NMLKJI 8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({"extend class Actor"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tvoid A_BruisAttack()"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tlet targ = target;"}),
t({"",""}),t({"\t\tif (targ)"}),
t({"",""}),t({"\t\t{"}),
t({"",""}),t({"\t\t\tif (CheckMeleeRange())"}),
t({"",""}),t({"\t\t\t{"}),
t({"",""}),t({"\t\t\t\tint damage = random[pr_bruisattack](1, 8) * 10;"}),
t({"",""}),t({"\t\t\t\tA_StartSound (\"baron/melee\", CHAN_WEAPON);"}),
t({"",""}),t({"\t\t\t\tint newdam = target.DamageMobj (self, self, damage, \"Melee\");"}),
t({"",""}),t({"\t\t\t\ttarg.TraceBleed (newdam > 0 ? newdam : damage, self);"}),
t({"",""}),t({"\t\t\t}"}),
t({"",""}),t({"\t\t\telse"}),
t({"",""}),t({"\t\t\t{"}),
t({"",""}),t({"\t\t\t\t// launch a missile"}),
t({"",""}),t({"\t\t\t\tSpawnMissile (target, \"BaronBall\");"}),
t({"",""}),t({"\t\t\t}"}),
t({"",""}),t({"\t\t}"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BasicAttack", {
t({"A_BasicAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_BasicAttack"}),
}),
s("beforeTele", {
t({"beforeTele"}),
}),
s("Berserk", {
t({"Berserk"}),
}),
s("Berserk", {
t({"class Berserk : CustomInventory "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\t+INVENTORY.ISHEALTH"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTBERSERK\";"}),
t({"",""}),t({"\t\tInventory.PickupSound \"misc/p_pkup\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPSTR A -1 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tPickup:"}),
t({"",""}),t({"\t\tTNT1 A 0 A_GiveInventory(\"PowerStrength\");"}),
t({"",""}),t({"\t\tTNT1 A 0 HealThing(100, 0);"}),
t({"",""}),t({"\t\tTNT1 A 0 A_SelectWeapon(\"Fist\");"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BetaSkullAttack", {
t({"A_BetaSkullAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_BetaSkullAttack"}),
}),
s("beyondMax", {
t({"beyondMax"}),
}),
s("BFG9000", {
t({"BFG9000"}),
}),
s("BFG9000", {
t({"class BFG9000 : DoomWeapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHeight 20;"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 2800;"}),
t({"",""}),t({"\t\tWeapon.AmmoUse 40;"}),
t({"",""}),t({"\t\tWeapon.AmmoGive 40;"}),
t({"",""}),t({"\t\tWeapon.AmmoType \"Cell\";"}),
t({"",""}),t({"\t\t+WEAPON.NOAUTOFIRE;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTBFG9000\";"}),
t({"",""}),t({"\t\tTag \"$TAG_BFG9000\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tBFGG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tBFGG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tBFGG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tBFGG A 20 A_BFGsound;"}),
t({"",""}),t({"\t\tBFGG B 10 A_GunFlash;"}),
t({"",""}),t({"\t\tBFGG B 10 A_FireBFG;"}),
t({"",""}),t({"\t\tBFGG B 20 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tFlash:"}),
t({"",""}),t({"\t\tBFGF A 11 Bright A_Light1;"}),
t({"",""}),t({"\t\tBFGF B 6 Bright A_Light2;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBFUG A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tOldFire:"}),
t({"",""}),t({"\t\tBFGG A 10 A_BFGsound;"}),
t({"",""}),t({"\t\tBFGG BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB 1 A_FireOldBFG;"}),
t({"",""}),t({"\t\tBFGG B 0 A_Light0;"}),
t({"",""}),t({"\t\tBFGG B 20 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BFGBall", {
t({"BFGBall"}),
}),
s("BFGExtra", {
t({"BFGExtra"}),
}),
s("BFGSound", {
t({"A_BFGSound "}),i(1),t({"; //https://zdoom.org/wiki/A_BFGSound"}),
}),
s("BFGSpray", {
t({"A_BFGSpray "}),i(1),t({"; //https://zdoom.org/wiki/A_BFGSpray"}),
}),
s("BigTree", {
t({"BigTree"}),
}),
s("BishopMissileWeave", {
t({"A_BishopMissileWeave "}),i(1),t({"; //https://zdoom.org/wiki/A_BishopMissileWeave"}),
}),
s("blocking", {
t({"blocking"}),
}),
s("blockmap", {
t({"blockmap"}),
}),
s("BloodSplatter", {
t({"BloodSplatter"}),
}),
s("BloodyTwitch", {
t({"BloodyTwitch"}),
}),
s("BlueArmor", {
t({"BlueArmor"}),
}),
s("BlueArmorForMegasphere", {
t({"class BlueArmorForMegasphere : BlueArmor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tArmor.SavePercent 50;"}),
t({"",""}),t({"\t\tArmor.SaveAmount 200;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BlueArmor", {
t({" "}),i(1),t({""}),
t({"",""}),t({"class BlueArmor : BasicArmorPickup"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 16;"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTMEGA\";"}),
t({"",""}),t({"\t\tInventory.Icon \"ARM2A0\";"}),
t({"",""}),t({"\t\tArmor.Savepercent 50;"}),
t({"",""}),t({"\t\tArmor.Saveamount 200;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tARM2 A 6;"}),
t({"",""}),t({"\t\tARM2 B 6 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BlueCard", {
t({"BlueCard"}),
}),
s("BlueCard", {
t({"class BlueCard : DoomKey "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTBLUECARD\";"}),
t({"",""}),t({"\t\tInventory.Icon \"STKEYS0\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBKEY A 10;"}),
t({"",""}),t({"\t\tBKEY B 10 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("BlueSkull", {
t({"BlueSkull"}),
}),
s("BlueSkull", {
t({"class BlueSkull : DoomKey "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTBLUESKUL\";"}),
t({"",""}),t({"\t\tInventory.Icon \"STKEYS3\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBSKU A 10;"}),
t({"",""}),t({"\t\tBSKU B 10 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("BlueTorch", {
t({"BlueTorch"}),
}),
s("BlurSphere", {
t({"BlurSphere"}),
}),
s("BlurSphere", {
t({" "}),i(1),t({""}),
t({"",""}),t({"class BlurSphere : PowerupGiver"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+VISIBILITYPULSE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\t+INVENTORY.AUTOACTIVATE"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\t+INVENTORY.BIGPOWERUP"}),
t({"",""}),t({"\t\tInventory.MaxAmount 0;"}),
t({"",""}),t({"\t\tPowerup.Type \"PowerInvisibility\";"}),
t({"",""}),t({"\t\tRenderStyle \"Translucent\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTINVIS\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPINS ABCD 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}\t"}),
}),
s("BobSin", {
t({"BobSin"}),
}),
s("BossBrain", {
t({"BossBrain"}),
}),
s("BossBrain", {
t({"class BossBrain : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 250;"}),
t({"",""}),t({"\t\tMass 10000000;"}),
t({"",""}),t({"\t\tPainChance 255;"}),
t({"",""}),t({"\t\tRadius 16;"}),
t({"",""}),t({"\t\t+SOLID +SHOOTABLE"}),
t({"",""}),t({"\t\t+NOICEDEATH"}),
t({"",""}),t({"\t\t+OLDRADIUSDMG"}),
t({"",""}),t({"\t\tPainSound \"brain/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"brain/death\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBBRN A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tBBRN B 36 A_BrainPain;"}),
t({"",""}),t({"\t\tGoto Spawn;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBBRN A 100 A_BrainScream;"}),
t({"",""}),t({"\t\tBBRN AA 10;"}),
t({"",""}),t({"\t\tBBRN A -1 A_BrainDie;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("BossDeath", {
t({"A_BossDeath "}),i(1),t({"; //https://zdoom.org/wiki/A_BossDeath"}),
}),
s("BossEye", {
t({"BossEye"}),
}),
s("BossEye", {
t({"class BossEye : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHeight 32;"}),
t({"",""}),t({"\t\t+NOBLOCKMAP"}),
t({"",""}),t({"\t\t+NOSECTOR"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSSWV A 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSSWV A 181 A_BrainAwake;"}),
t({"",""}),t({"\t\tSSWV A 150 A_BrainSpit;"}),
t({"",""}),t({"\t\tWait;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BossTarget", {
t({"BossTarget"}),
}),
s("BossTarget", {
t({"class BossTarget : SpecialSpot "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHeight 32;"}),
t({"",""}),t({"\t\t+NOBLOCKMAP;"}),
t({"",""}),t({"\t\t+NOSECTOR;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BounceActor", {
t({"BounceActor"}),
}),
s("BouncePlane", {
t({"BouncePlane"}),
}),
s("BounceWall", {
t({"BounceWall"}),
}),
s("BrainAwake", {
t({"A_BrainAwake "}),i(1),t({"; //https://zdoom.org/wiki/A_BrainAwake"}),
}),
s("BrainDie", {
t({"A_BrainDie "}),i(1),t({"; //https://zdoom.org/wiki/A_BrainDie"}),
}),
s("BrainExplode", {
t({"A_BrainExplode "}),i(1),t({"; //https://zdoom.org/wiki/A_BrainExplode"}),
}),
s("BrainPain", {
t({"A_BrainPain "}),i(1),t({"; //https://zdoom.org/wiki/A_BrainPain"}),
}),
s("BrainScream", {
t({"A_BrainScream "}),i(1),t({"; //https://zdoom.org/wiki/A_BrainScream"}),
}),
s("BrainSpit", {
t({"A_BrainSpit "}),i(1),t({"; //https://zdoom.org/wiki/A_BrainSpit"}),
}),
s("BrainStem", {
t({"BrainStem"}),
}),
s("BruisAttack", {
t({"A_BruisAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_BruisAttack"}),
}),
s("BspiAttack", {
t({"A_BspiAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_BspiAttack"}),
}),
s("BulletAttack", {
t({"A_BulletAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_BulletAttack"}),
}),
s("BulletPuff", {
t({"BulletPuff"}),
}),
s("BulletPuff", {
t({" "}),i(1),t({""}),
t({"",""}),t({"class BulletPuff : Actor"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+NOBLOCKMAP"}),
t({"",""}),t({"\t\t+NOGRAVITY"}),
t({"",""}),t({"\t\t+ALLOWPARTICLES"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Translucent\";"}),
t({"",""}),t({"\t\tAlpha 0.5;"}),
t({"",""}),t({"\t\tVSpeed 1;"}),
t({"",""}),t({"\t\tMass 5;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPUFF A 4 Bright;"}),
t({"",""}),t({"\t\tPUFF B 4;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\t\tPUFF CD 4;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("BulletSlope", {
t({"BulletSlope"}),
}),
s("BurningBarrel", {
t({"BurningBarrel"}),
}),
s("Burst", {
t({"A_Burst "}),i(1),t({"; //https://zdoom.org/wiki/A_Burst"}),
}),
s("Cacodemon", {
t({"Cacodemon"}),
}),
s("CacodemonBall", {
t({"CacodemonBall"}),
}),
s("CacodemonBall", {
t({"class CacodemonBall : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 6;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 10;"}),
t({"",""}),t({"\t\tFastSpeed 20;"}),
t({"",""}),t({"\t\tDamage 5;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t\tAlpha 1;"}),
t({"",""}),t({"\t\tSeeSound \"caco/attack\";"}),
t({"",""}),t({"\t\tDeathSound \"caco/shotx\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBAL2 AB 4 BRIGHT;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBAL2 CDE 6 BRIGHT;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Cacodemon", {
t({"class Cacodemon : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 400;"}),
t({"",""}),t({"\t\tRadius 31;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 400;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 128;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOAT +NOGRAVITY"}),
t({"",""}),t({"\t\tSeeSound \"caco/sight\";"}),
t({"",""}),t({"\t\tPainSound \"caco/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"caco/death\";"}),
t({"",""}),t({"\t\tActiveSound \"caco/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_CACO\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_CACOHIT\";"}),
t({"",""}),t({"\t\tTag \"$FN_CACO\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tHEAD A 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tHEAD A 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tHEAD B 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tHEAD C 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tHEAD D 5 BRIGHT A_HeadAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tHEAD E 3;"}),
t({"",""}),t({"\t\tHEAD E 3 A_Pain;"}),
t({"",""}),t({"\t\tHEAD F 6;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tHEAD G 8;"}),
t({"",""}),t({"\t\tHEAD H 8 A_Scream;"}),
t({"",""}),t({"\t\tHEAD I 8;"}),
t({"",""}),t({"\t\tHEAD J 8;"}),
t({"",""}),t({"\t\tHEAD K 8 A_NoBlocking;"}),
t({"",""}),t({"\t\tHEAD L -1 A_SetFloorClip;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tHEAD L 8 A_UnSetFloorClip;"}),
t({"",""}),t({"\t\tHEAD KJIHG 8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Candelabra", {
t({"Candelabra"}),
}),
s("Candlestick", {
t({"Candlestick"}),
}),
s("CanRaise", {
t({"CanRaise"}),
}),
s("CanSeek", {
t({"CanSeek"}),
}),
s("Cell", {
t({"Cell"}),
}),
s("CellPack", {
t({"CellPack"}),
}),
s("CellPack", {
t({"class CellPack : Cell "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTCELLBOX\";"}),
t({"",""}),t({"\t\tInventory.Amount 100;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCELP A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Cell", {
t({"class Cell : Ammo "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTCELL\";"}),
t({"",""}),t({"\t\tInventory.Amount 20;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 300;"}),
t({"",""}),t({"\t\tAmmo.BackpackAmount 20;"}),
t({"",""}),t({"\t\tAmmo.BackpackMaxAmount 600;"}),
t({"",""}),t({"\t\tInventory.Icon \"CELLA0\";"}),
t({"",""}),t({"\t\tTag \"$AMMO_CELLS\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCELL A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("CentaurDefend", {
t({"A_CentaurDefend "}),i(1),t({"; //https://zdoom.org/wiki/A_CentaurDefend"}),
}),
s("Chaingun", {
t({"Chaingun"}),
}),
s("ChaingunGuy", {
t({"ChaingunGuy"}),
}),
s("chaingunguy", {
t({"class "}),i(1),t({" : Actor Replaces ChaingunGuy"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 70;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 170;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"chainguy/sight\";"}),
t({"",""}),t({"\t\tPainSound \"chainguy/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"chainguy/death\";"}),
t({"",""}),t({"\t\tActiveSound \"chainguy/active\";"}),
t({"",""}),t({"\t\tAttackSound \"chainguy/attack\";"}),
t({"",""}),t({"\t\tObituary \"$OB_CHAINGUY\";"}),
t({"",""}),t({"\t\tTag \"$FN_HEAVY\";"}),
t({"",""}),t({"\t\tDropitem \"Chaingun\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCPOS AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tCPOS AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tCPOS E 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tCPOS FE 4 BRIGHT A_CPosAttack;"}),
t({"",""}),t({"\t\tCPOS F 1 A_CPosRefire;"}),
t({"",""}),t({"\t\tGoto Missile+1;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tCPOS G 3;"}),
t({"",""}),t({"\t\tCPOS G 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tCPOS H 5;"}),
t({"",""}),t({"\t\tCPOS I 5 A_Scream;"}),
t({"",""}),t({"\t\tCPOS J 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tCPOS KLM 5;"}),
t({"",""}),t({"\t\tCPOS N -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tCPOS O 5;"}),
t({"",""}),t({"\t\tCPOS P 5 A_XScream;"}),
t({"",""}),t({"\t\tCPOS Q 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tCPOS RS 5;"}),
t({"",""}),t({"\t\tCPOS T -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tCPOS N 5;"}),
t({"",""}),t({"\t\tCPOS MLKJIH 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("ChaingunGuy", {
t({"class ChaingunGuy : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 70;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 170;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"chainguy/sight\";"}),
t({"",""}),t({"\t\tPainSound \"chainguy/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"chainguy/death\";"}),
t({"",""}),t({"\t\tActiveSound \"chainguy/active\";"}),
t({"",""}),t({"\t\tAttackSound \"chainguy/attack\";"}),
t({"",""}),t({"\t\tObituary \"$OB_CHAINGUY\";"}),
t({"",""}),t({"\t\tTag \"$FN_HEAVY\";"}),
t({"",""}),t({"\t\tDropitem \"Chaingun\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCPOS AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tCPOS AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tCPOS E 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tCPOS FE 4 BRIGHT A_CPosAttack;"}),
t({"",""}),t({"\t\tCPOS F 1 A_CPosRefire;"}),
t({"",""}),t({"\t\tGoto Missile+1;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tCPOS G 3;"}),
t({"",""}),t({"\t\tCPOS G 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tCPOS H 5;"}),
t({"",""}),t({"\t\tCPOS I 5 A_Scream;"}),
t({"",""}),t({"\t\tCPOS J 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tCPOS KLM 5;"}),
t({"",""}),t({"\t\tCPOS N -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tCPOS O 5;"}),
t({"",""}),t({"\t\tCPOS P 5 A_XScream;"}),
t({"",""}),t({"\t\tCPOS Q 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tCPOS RS 5;"}),
t({"",""}),t({"\t\tCPOS T -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tCPOS N 5;"}),
t({"",""}),t({"\t\tCPOS MLKJIH 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Chaingun", {
t({"class Chaingun : DoomWeapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 700;"}),
t({"",""}),t({"\t\tWeapon.AmmoUse 1;"}),
t({"",""}),t({"\t\tWeapon.AmmoGive 20;"}),
t({"",""}),t({"\t\tWeapon.AmmoType \"Clip\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTCHAINGUN\";"}),
t({"",""}),t({"\t\tObituary \"$OB_MPCHAINGUN\";"}),
t({"",""}),t({"\t\tTag \"$TAG_CHAINGUN\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tCHGG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tCHGG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tCHGG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tCHGG AB 4 A_FireCGun;"}),
t({"",""}),t({"\t\tCHGG B 0 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tFlash:"}),
t({"",""}),t({"\t\tCHGF A 5 Bright A_Light1;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\t\tCHGF B 5 Bright A_Light2;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tMGUN A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Chainsaw", {
t({"Chainsaw"}),
}),
s("Chainsaw", {
t({"class Chainsaw : Weapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.Kickback 0;"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 2200;"}),
t({"",""}),t({"\t\tWeapon.UpSound \"weapons/sawup\";"}),
t({"",""}),t({"\t\tWeapon.ReadySound \"weapons/sawidle\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTCHAINSAW\";"}),
t({"",""}),t({"\t\tObituary \"$OB_MPCHAINSAW\";"}),
t({"",""}),t({"\t\tTag \"$TAG_CHAINSAW\";"}),
t({"",""}),t({"\t\t+WEAPON.MELEEWEAPON\t\t"}),
t({"",""}),t({"\t\t+WEAPON.NOAUTOSWITCHTO"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tSAWG CD 4 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tSAWG C 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tSAWG C 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tSAWG AB 4 A_Saw;"}),
t({"",""}),t({"\t\tSAWG B 0 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCSAW A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({"\t"}),
}),
s("CHAN_BODY", {
t({"CHAN_BODY"}),
}),
s("change", {
t({"Level.change"}),
}),
s("ChangeCountFlags", {
t({"A_ChangeCountFlags "}),i(1),t({"; //https://zdoom.org/wiki/A_ChangeCountFlags"}),
}),
s("ChangeFlag", {
t({"A_ChangeFlag "}),i(1),t({"; //https://zdoom.org/wiki/A_ChangeFlag"}),
}),
s("ChangeModel", {
t({"A_ChangeModel "}),i(1),t({"; //https://zdoom.org/wiki/A_ChangeModel"}),
}),
s("changeTarget", {
t({"changeTarget"}),
}),
s("ChangeTid", {
t({"ChangeTid"}),
}),
s("ChangeVelocity", {
t({"A_ChangeVelocity "}),i(1),t({"; //https://zdoom.org/wiki/A_ChangeVelocity"}),
}),
s("channel", {
t({"channel"}),
}),
s("Chase", {
t({"A_Chase "}),i(1),t({"; //https://zdoom.org/wiki/A_Chase"}),
}),
s("checkabove", {
t({"checkabove"}),
}),
s("CheckArmorType", {
t({"CheckArmorType"}),
}),
s("CheckBlock", {
t({"CheckBlock"}),
}),
s("CheckBossDeath", {
t({"CheckBossDeath"}),
}),
s("CheckCeiling", {
t({"A_CheckCeiling "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckCeiling"}),
}),
s("CheckFakeFloorTriggers", {
t({"CheckFakeFloorTriggers"}),
}),
s("CheckFlag", {
t({"A_CheckFlag "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckFlag"}),
}),
s("CheckFloor", {
t({"A_CheckFloor "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckFloor"}),
}),
s("CheckFor3DCeilingHit", {
t({"CheckFor3DCeilingHit"}),
}),
s("CheckFor3DFloorHit", {
t({"CheckFor3DFloorHit"}),
}),
s("CheckForReload", {
t({"A_CheckForReload "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckForReload"}),
}),
s("CheckForResurrection", {
t({"A_CheckForResurrection "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckForResurrection"}),
}),
s("CheckFOV", {
t({"CheckFOV"}),
}),
s("CheckIfCloser", {
t({"CheckIfCloser"}),
}),
s("CheckIfInTargetLOS", {
t({"CheckIfInTargetLOS"}),
}),
s("CheckIfSeen", {
t({"CheckIfSeen"}),
}),
s("CheckIfTargetInLOS", {
t({"CheckIfTargetInLOS"}),
}),
s("CheckInventory", {
t({"CheckInventory"}),
}),
s("CheckKeys", {
t({"CheckKeys"}),
}),
s("CheckLocalView", {
t({"CheckLocalView"}),
}),
s("CheckLOF", {
t({"CheckLOF"}),
}),
s("CheckMeleeRange", {
t({"CheckMeleeRange"}),
}),
s("CheckMeleeRange2", {
t({"CheckMeleeRange2"}),
}),
s("CheckMissileRange", {
t({"CheckMissileRange"}),
}),
s("CheckMissileSpawn", {
t({"CheckMissileSpawn"}),
}),
s("CheckMonsterUseSpecials", {
t({"CheckMonsterUseSpecials"}),
}),
s("CheckMove", {
t({"CheckMove"}),
}),
s("CheckNoDelay", {
t({"CheckNoDelay"}),
}),
s("CheckPlayerDone", {
t({"A_CheckPlayerDone "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckPlayerDone"}),
}),
s("CheckPortalTransition", {
t({"CheckPortalTransition"}),
}),
s("CheckPosition", {
t({"CheckPosition"}),
}),
s("CheckProximity", {
t({"CheckProximity"}),
}),
s("CheckRailReload", {
t({"A_CheckRailReload "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckRailReload"}),
}),
s("CheckRange", {
t({"CheckRange"}),
}),
s("CheckReload", {
t({"A_CheckReload "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckReload"}),
}),
s("Checks", {
t({"Checks"}),
}),
s("CheckSight", {
t({"CheckSight"}),
}),
s("CheckSightOrRange", {
t({"CheckSightOrRange"}),
}),
s("checkspawn", {
t({"checkspawn"}),
}),
s("CheckSpecies", {
t({"A_CheckSpecies "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckSpecies"}),
}),
s("CheckSplash", {
t({"CheckSplash"}),
}),
s("CheckSwitchRange", {
t({"Level.CheckSwitchRange"}),
}),
s("CheckTerrain", {
t({"A_CheckTerrain "}),i(1),t({"; //https://zdoom.org/wiki/A_CheckTerrain"}),
}),
s("class", {
t({"class"}),
}),
s("class<Actor>", {
t({"class<Actor>"}),
}),
s("class<Inventory>", {
t({"class<Inventory>"}),
}),
s("classname", {
t({"classname"}),
}),
s("Class", {
t({"Class "}),i(1),t({" : "}),i(2),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(3),t({""}),
t({"",""}),t({"}"}),
}),
s("ClearBounce", {
t({"ClearBounce"}),
}),
s("ClearCounters", {
t({"ClearCounters"}),
}),
s("ClearFOVInterpolation", {
t({"ClearFOVInterpolation"}),
}),
s("ClearInterpolation", {
t({"ClearInterpolation"}),
}),
s("ClearLastHeard", {
t({"A_ClearLastHeard "}),i(1),t({"; //https://zdoom.org/wiki/A_ClearLastHeard"}),
}),
s("ClearModelFlag", {
t({"ClearModelFlag"}),
}),
s("ClearOverlays", {
t({"A_ClearOverlays "}),i(1),t({"; //https://zdoom.org/wiki/A_ClearOverlays"}),
}),
s("ClearReFire", {
t({"A_ClearReFire "}),i(1),t({"; //https://zdoom.org/wiki/A_ClearReFire"}),
}),
s("clearscope", {
t({"clearscope"}),
}),
s("ClearShadow", {
t({"A_ClearShadow "}),i(1),t({"; //https://zdoom.org/wiki/A_ClearShadow"}),
}),
s("ClearSoundTarget", {
t({"A_ClearSoundTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_ClearSoundTarget"}),
}),
s("ClearTarget", {
t({"A_ClearTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_ClearTarget"}),
}),
s("Clip", {
t({"Clip"}),
}),
s("ClipBox", {
t({"ClipBox"}),
}),
s("ClipBox", {
t({"class ClipBox : Clip "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTCLIPBOX\";"}),
t({"",""}),t({"\t\tInventory.Amount 50;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tAMMO A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Clip", {
t({"class Clip : Ammo "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTCLIP\";"}),
t({"",""}),t({"\t\tInventory.Amount 10;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 200;"}),
t({"",""}),t({"\t\tAmmo.BackpackAmount 10;"}),
t({"",""}),t({"\t\tAmmo.BackpackMaxAmount 400;"}),
t({"",""}),t({"\t\tInventory.Icon \"CLIPA0\";"}),
t({"",""}),t({"\t\tTag \"$AMMO_CLIP\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCLIP A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Cluster", {
t({"Level.Cluster"}),
}),
s("ClusterFlags", {
t({"Level.ClusterFlags"}),
}),
s("CLUSTERHUB", {
t({"Level.CLUSTERHUB"}),
}),
s("ColonGibs", {
t({"ColonGibs"}),
}),
s("Column", {
t({"Column"}),
}),
s("ComboAttack", {
t({"A_ComboAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_ComboAttack"}),
}),
s("CommanderKeen", {
t({"CommanderKeen"}),
}),
s("CommanderKeen", {
t({"class CommanderKeen : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 100;"}),
t({"",""}),t({"\t\tRadius 16;"}),
t({"",""}),t({"\t\tHeight 72;"}),
t({"",""}),t({"\t\tMass 10000000;"}),
t({"",""}),t({"\t\tPainChance 256;"}),
t({"",""}),t({"\t\t+SOLID "}),
t({"",""}),t({"\t\t+SPAWNCEILING "}),
t({"",""}),t({"\t\t+NOGRAVITY "}),
t({"",""}),t({"\t\t+SHOOTABLE "}),
t({"",""}),t({"\t\t+COUNTKILL "}),
t({"",""}),t({"\t\t+NOICEDEATH"}),
t({"",""}),t({"\t\t+ISMONSTER"}),
t({"",""}),t({"\t\tPainSound \"keen/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"keen/death\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tKEEN A -1;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tKEEN AB\t6;"}),
t({"",""}),t({"\t\tKEEN C 6 A_Scream;"}),
t({"",""}),t({"\t\tKEEN DEFGH\t6;"}),
t({"",""}),t({"\t\tKEEN I 6;"}),
t({"",""}),t({"\t\tKEEN J 6;"}),
t({"",""}),t({"\t\tKEEN K 6 A_KeenDie;"}),
t({"",""}),t({"\t\tKEEN L -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tKEEN M\t4;"}),
t({"",""}),t({"\t\tKEEN M\t8 A_Pain;"}),
t({"",""}),t({"\t\tGoto Spawn;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("CompatFlags", {
t({"Level.CompatFlags"}),
}),
s("CompatFlags2", {
t({"Level.CompatFlags2"}),
}),
s("consoleplayer", {
t({"consoleplayer"}),
}),
s("Console_Printf", {
t({"Console.Printf(TEXTCOLOR_"}),i(1),t({" .. \""}),i(2),t({"\\n\");"}),
}),
s("const", {
t({"const"}),
}),
s("Control", {
t({"Control"}),
}),
s("CopyBloodColor", {
t({"CopyBloodColor"}),
}),
s("CopyFriendliness", {
t({"CopyFriendliness"}),
}),
s("CopySpriteFrame", {
t({"A_CopySpriteFrame "}),i(1),t({"; //https://zdoom.org/wiki/A_CopySpriteFrame"}),
}),
s("count", {
t({"count"}),
}),
s("Countdown", {
t({"A_Countdown "}),i(1),t({"; //https://zdoom.org/wiki/A_Countdown"}),
}),
s("CountdownArg", {
t({"A_CountdownArg "}),i(1),t({"; //https://zdoom.org/wiki/A_CountdownArg"}),
}),
s("CountInv", {
t({"CountInv"}),
}),
s("CountsAsKill", {
t({"CountsAsKill"}),
}),
s("CPosAttack", {
t({"A_CPosAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_CPosAttack"}),
}),
s("CPosRefire", {
t({"A_CPosRefire "}),i(1),t({"; //https://zdoom.org/wiki/A_CPosRefire"}),
}),
s("create", {
t({"Level.create"}),
}),
s("CreateCeilingSector", {
t({"Level.CreateCeilingSector"}),
}),
s("CreateFloorSector", {
t({"Level.CreateFloorSector"}),
}),
s("crush", {
t({"Level.crush"}),
}),
s("crushMode", {
t({"Level.crushMode"}),
}),
s("CStaffMissileSlither", {
t({"A_CStaffMissileSlither "}),i(1),t({"; //https://zdoom.org/wiki/A_CStaffMissileSlither"}),
}),
s("CustomBulletAttack", {
t({"A_CustomBulletAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_CustomBulletAttack"}),
}),
s("CustomComboAttack", {
t({"A_CustomComboAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_CustomComboAttack"}),
}),
s("CustomMeleeAttack", {
t({"A_CustomMeleeAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_CustomMeleeAttack"}),
}),
s("CustomMissile", {
t({"A_CustomMissile "}),i(1),t({"; //https://zdoom.org/wiki/A_CustomMissile"}),
}),
s("CustomPunch", {
t({"A_CustomPunch "}),i(1),t({"; //https://zdoom.org/wiki/A_CustomPunch"}),
}),
s("CustomRailgun", {
t({"A_CustomRailgun "}),i(1),t({"; //https://zdoom.org/wiki/A_CustomRailgun"}),
}),
s("CyberAttack", {
t({"A_CyberAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_CyberAttack"}),
}),
s("Cyberdemon", {
t({"Cyberdemon"}),
}),
s("Cyberdemon", {
t({"class Cyberdemon : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 4000;"}),
t({"",""}),t({"\t\tRadius 40;"}),
t({"",""}),t({"\t\tHeight 110;"}),
t({"",""}),t({"\t\tMass 1000;"}),
t({"",""}),t({"\t\tSpeed 16;"}),
t({"",""}),t({"\t\tPainChance 20;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\tMinMissileChance 160;"}),
t({"",""}),t({"\t\t+BOSS "}),
t({"",""}),t({"\t\t+MISSILEMORE"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+NORADIUSDMG"}),
t({"",""}),t({"\t\t+DONTMORPH"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\t+E2M8BOSS"}),
t({"",""}),t({"\t\t+E4M6BOSS"}),
t({"",""}),t({"\t\tSeeSound \"cyber/sight\";"}),
t({"",""}),t({"\t\tPainSound \"cyber/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"cyber/death\";"}),
t({"",""}),t({"\t\tActiveSound \"cyber/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_CYBORG\";"}),
t({"",""}),t({"\t\tTag \"$FN_CYBER\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tCYBR AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tCYBR A 3 A_Hoof;"}),
t({"",""}),t({"\t\tCYBR ABBCC 3 A_Chase;"}),
t({"",""}),t({"\t\tCYBR D 3 A_Metal;"}),
t({"",""}),t({"\t\tCYBR D 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tCYBR E 6 A_FaceTarget;"}),
t({"",""}),t({"\t\tCYBR F 12 A_CyberAttack;"}),
t({"",""}),t({"\t\tCYBR E 12 A_FaceTarget;"}),
t({"",""}),t({"\t\tCYBR F 12 A_CyberAttack;"}),
t({"",""}),t({"\t\tCYBR E 12 A_FaceTarget;"}),
t({"",""}),t({"\t\tCYBR F 12 A_CyberAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tCYBR G 10 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tCYBR H 10;"}),
t({"",""}),t({"\t\tCYBR I 10 A_Scream;"}),
t({"",""}),t({"\t\tCYBR JKL 10;"}),
t({"",""}),t({"\t\tCYBR M 10 A_NoBlocking;"}),
t({"",""}),t({"\t\tCYBR NO 10;"}),
t({"",""}),t({"\t\tCYBR P 30;"}),
t({"",""}),t({"\t\tCYBR P -1 A_BossDeath;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DaggerAlert", {
t({"DaggerAlert"}),
}),
s("damage", {
t({"damage"}),
}),
s("DamageChildren", {
t({"A_DamageChildren "}),i(1),t({"; //https://zdoom.org/wiki/A_DamageChildren"}),
}),
s("DamageMaster", {
t({"A_DamageMaster "}),i(1),t({"; //https://zdoom.org/wiki/A_DamageMaster"}),
}),
s("DamageSelf", {
t({"A_DamageSelf "}),i(1),t({"; //https://zdoom.org/wiki/A_DamageSelf"}),
}),
s("DamageSiblings", {
t({"A_DamageSiblings "}),i(1),t({"; //https://zdoom.org/wiki/A_DamageSiblings"}),
}),
s("DamageTarget", {
t({"A_DamageTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_DamageTarget"}),
}),
s("DamageTracer", {
t({"A_DamageTracer "}),i(1),t({"; //https://zdoom.org/wiki/A_DamageTracer"}),
}),
s("damagetype", {
t({"damagetype"}),
}),
s("DeadCacodemon", {
t({"DeadCacodemon"}),
}),
s("DeadCacodemon", {
t({"class DeadCacodemon : Cacodemon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tSkip_Super;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tGoto Super::Death+5;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DeadDemon", {
t({"DeadDemon"}),
}),
s("DeadDemon", {
t({"class DeadDemon : Demon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tSkip_Super;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tGoto Super::Death+5;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DeadDoomImp", {
t({"DeadDoomImp"}),
}),
s("DeadDoomImp", {
t({"class DeadDoomImp : DoomImp "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tSkip_Super;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tGoto Super::Death+4;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DeadLostSoul", {
t({"DeadLostSoul"}),
}),
s("DeadMarine", {
t({"DeadMarine"}),
}),
s("DeadMarine", {
t({"class DeadMarine : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLAY N -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DeadShotgunGuy", {
t({"DeadShotgunGuy"}),
}),
s("DeadShotgunGuy", {
t({"class DeadShotgunGuy : ShotgunGuy "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tSkip_Super;"}),
t({"",""}),t({"\t\tDropItem \"None\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tGoto Super::Death+4;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DeadStick", {
t({"DeadStick"}),
}),
s("DeadZombieMan", {
t({"DeadZombieMan"}),
}),
s("DeadZombieMan", {
t({"class DeadZombieMan : ZombieMan "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tSkip_Super;"}),
t({"",""}),t({"\t\tDropItem \"None\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tGoto Super::Death+4;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Death", {
t({"Death"}),
}),
s("DeathSound", {
t({"DeathSound"}),
}),
s("Default", {
t({"Default"}),
}),
s("Default", {
t({"Default"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("defstr", {
t({"defstr"}),
}),
s("DeltaAngle", {
t({"DeltaAngle"}),
}),
s("Demon", {
t({"Demon"}),
}),
s("Demon", {
t({"class Demon : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 150;"}),
t({"",""}),t({"\t\tPainChance 180;"}),
t({"",""}),t({"\t\tSpeed 10;"}),
t({"",""}),t({"\t\tRadius 30;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 400;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"demon/sight\";"}),
t({"",""}),t({"\t\tAttackSound \"demon/melee\";"}),
t({"",""}),t({"\t\tPainSound \"demon/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"demon/death\";"}),
t({"",""}),t({"\t\tActiveSound \"demon/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_DEMONHIT\";"}),
t({"",""}),t({"\t\tTag \"$FN_DEMON\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSARG AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSARG AABBCCDD 2 Fast A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\t\tSARG EF 8 Fast A_FaceTarget;"}),
t({"",""}),t({"\t\tSARG G 8 Fast A_SargAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSARG H 2 Fast;"}),
t({"",""}),t({"\t\tSARG H 2 Fast A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSARG I 8;"}),
t({"",""}),t({"\t\tSARG J 8 A_Scream;"}),
t({"",""}),t({"\t\tSARG K 4;"}),
t({"",""}),t({"\t\tSARG L 4 A_NoBlocking;"}),
t({"",""}),t({"\t\tSARG M 4;"}),
t({"",""}),t({"\t\tSARG N -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tSARG N 5;"}),
t({"",""}),t({"\t\tSARG MLKJI 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("DeQueueCorpse", {
t({"A_DeQueueCorpse "}),i(1),t({"; //https://zdoom.org/wiki/A_DeQueueCorpse"}),
}),
s("Detonate", {
t({"A_Detonate "}),i(1),t({"; //https://zdoom.org/wiki/A_Detonate"}),
}),
s("Die", {
t({"A_Die "}),i(1),t({"; //https://zdoom.org/wiki/A_Die"}),
}),
s("DI_ITEM_BOTTOM", {
t({"DI_ITEM_BOTTOM"}),
}),
s("DI_ITEM_CENTER", {
t({"DI_ITEM_CENTER"}),
}),
s("DI_ITEM_LEFT_BOTTOM", {
t({"DI_ITEM_LEFT_BOTTOM"}),
}),
s("DI_ITEM_LEFT_TOP", {
t({"DI_ITEM_LEFT_TOP"}),
}),
s("DI_ITEM_LEFT", {
t({"DI_ITEM_LEFT"}),
}),
s("DI_ITEM_OFFSETS", {
t({"DI_ITEM_OFFSETS"}),
}),
s("DI_ITEM_RIGHT_BOTTOM", {
t({"DI_ITEM_RIGHT_BOTTOM"}),
}),
s("DI_ITEM_RIGHT_TOP", {
t({"DI_ITEM_RIGHT_TOP"}),
}),
s("DI_ITEM_RIGHT", {
t({"DI_ITEM_RIGHT"}),
}),
s("DI_ITEM_TOP", {
t({"DI_ITEM_TOP"}),
}),
s("DI_SCREEN_BOTTOM", {
t({"DI_SCREEN_BOTTOM"}),
}),
s("DI_SCREEN_CENTER_BOTTOM", {
t({"DI_SCREEN_CENTER_BOTTOM"}),
}),
s("DI_SCREEN_CENTER", {
t({"DI_SCREEN_CENTER"}),
}),
s("DI_SCREEN_LEFT_BOTTOM", {
t({"DI_SCREEN_LEFT_BOTTOM"}),
}),
s("DI_SCREEN_LEFT_CENTER", {
t({"DI_SCREEN_LEFT_CENTER"}),
}),
s("DI_SCREEN_LEFT_TOP", {
t({"DI_SCREEN_LEFT_TOP"}),
}),
s("DI_SCREEN_RIGHT_BOTTOM", {
t({"DI_SCREEN_RIGHT_BOTTOM"}),
}),
s("DI_SCREEN_RIGHT_CENTER", {
t({"DI_SCREEN_RIGHT_CENTER"}),
}),
s("DI_SCREEN_RIGHT_TOP", {
t({"DI_SCREEN_RIGHT_TOP"}),
}),
s("DI_SCREEN_TOP", {
t({"DI_SCREEN_TOP"}),
}),
s("DisplayNameTag", {
t({"DisplayNameTag"}),
}),
s("distance", {
t({"distance"}),
}),
s("Distance2D", {
t({"Distance2D"}),
}),
s("Distance2DSquared", {
t({"Distance2DSquared"}),
}),
s("Distance3D", {
t({"Distance3D"}),
}),
s("Distance3DSquared", {
t({"Distance3DSquared"}),
}),
s("DistanceBySpeed", {
t({"DistanceBySpeed"}),
}),
s("dist_close", {
t({"dist_close"}),
}),
s("dist_max", {
t({"dist_max"}),
}),
s("DI_TEXT_ALIGN_CENTER", {
t({"DI_TEXT_ALIGN_CENTER"}),
}),
s("DI_TEXT_ALIGN_LEFT", {
t({"DI_TEXT_ALIGN_LEFT"}),
}),
s("DI_TEXT_ALIGN_RIGHT", {
t({"DI_TEXT_ALIGN_RIGHT"}),
}),
s("DoGiveInventory", {
t({"DoGiveInventory"}),
}),
s("DoMissileDamage", {
t({"DoMissileDamage"}),
}),
s("DoomImp", {
t({"DoomImp"}),
}),
s("DoomImpBall", {
t({"DoomImpBall"}),
}),
s("DoomImpBall", {
t({"class DoomImpBall : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 6;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 10;"}),
t({"",""}),t({"\t\tFastSpeed 20;"}),
t({"",""}),t({"\t\tDamage 3;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t\tAlpha 1;"}),
t({"",""}),t({"\t\tSeeSound \"imp/attack\";"}),
t({"",""}),t({"\t\tDeathSound \"imp/shotx\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBAL1 AB 4 BRIGHT;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBAL1 CDE 6 BRIGHT;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DoomImp", {
t({"class DoomImp : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 60;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 200;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"imp/sight\";"}),
t({"",""}),t({"\t\tPainSound \"imp/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"imp/death\";"}),
t({"",""}),t({"\t\tActiveSound \"imp/active\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_IMPHIT\";"}),
t({"",""}),t({"\t\tObituary \"$OB_IMP\";"}),
t({"",""}),t({"\t\tTag \"$FN_IMP\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tTROO AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tTROO AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tTROO EF 8 A_FaceTarget;"}),
t({"",""}),t({"\t\tTROO G 6 A_TroopAttack ;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tTROO H 2;"}),
t({"",""}),t({"\t\tTROO H 2 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tTROO I 8;"}),
t({"",""}),t({"\t\tTROO J 8 A_Scream;"}),
t({"",""}),t({"\t\tTROO K 6;"}),
t({"",""}),t({"\t\tTROO L 6 A_NoBlocking;"}),
t({"",""}),t({"\t\tTROO M -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tTROO N 5;"}),
t({"",""}),t({"\t\tTROO O 5 A_XScream;"}),
t({"",""}),t({"\t\tTROO P 5;"}),
t({"",""}),t({"\t\tTROO Q 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tTROO RST 5;"}),
t({"",""}),t({"\t\tTROO U -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tTROO ML 8;"}),
t({"",""}),t({"\t\tTROO KJI 6;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("DoomKey", {
t({"DoomKey"}),
}),
s("DoomKey", {
t({"class DoomKey : Key "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 16;"}),
t({"",""}),t({"\t\t+NOTDMATCH"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("DoomPlayer", {
t({"DoomPlayer"}),
}),
s("DoomPlayer", {
t({"//=========================================================================== "}),i(1),t({""}),
t({"",""}),t({"//"}),
t({"",""}),t({"// Player"}),
t({"",""}),t({"//"}),
t({"",""}),t({"//==========================================================================="}),
t({"",""}),t({"class DoomPlayer : PlayerPawn"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tSpeed 1;"}),
t({"",""}),t({"\t\tHealth 100;"}),
t({"",""}),t({"\t\tRadius 16;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tPainChance 255;"}),
t({"",""}),t({"\t\tPlayer.DisplayName \"Marine\";"}),
t({"",""}),t({"\t\tPlayer.CrouchSprite \"PLYC\";"}),
t({"",""}),t({"\t\tPlayer.StartItem \"Pistol\";"}),
t({"",""}),t({"\t\tPlayer.StartItem \"Fist\";"}),
t({"",""}),t({"\t\tPlayer.StartItem \"Clip\", 50;"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 1, \"Fist\", \"Chainsaw\";"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 2, \"Pistol\";"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 3, \"Shotgun\", \"SuperShotgun\";"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 4, \"Chaingun\";"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 5, \"RocketLauncher\";"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 6, \"PlasmaRifle\";"}),
t({"",""}),t({"\t\tPlayer.WeaponSlot 7, \"BFG9000\";"}),
t({"",""}),t({"\t\t"}),
t({"",""}),t({"\t\tPlayer.ColorRange 112, 127;"}),
t({"",""}),t({"\t\tPlayer.Colorset 0, \"$TXT_COLOR_GREEN\",\t\t0x70, 0x7F,  0x72;"}),
t({"",""}),t({"\t\tPlayer.Colorset 1, \"$TXT_COLOR_GRAY\",\t\t0x60, 0x6F,  0x62;"}),
t({"",""}),t({"\t\tPlayer.Colorset 2, \"$TXT_COLOR_BROWN\",\t\t0x40, 0x4F,  0x42;"}),
t({"",""}),t({"\t\tPlayer.Colorset 3, \"$TXT_COLOR_RED\",\t\t0x20, 0x2F,  0x22;"}),
t({"",""}),t({"\t\t// Doom Legacy additions"}),
t({"",""}),t({"\t\tPlayer.Colorset 4, \"$TXT_COLOR_LIGHTGRAY\",\t0x58, 0x67,  0x5A;"}),
t({"",""}),t({"\t\tPlayer.Colorset 5, \"$TXT_COLOR_LIGHTBROWN\",\t0x38, 0x47,  0x3A;"}),
t({"",""}),t({"\t\tPlayer.Colorset 6, \"$TXT_COLOR_LIGHTRED\",\t0xB0, 0xBF,  0xB2;"}),
t({"",""}),t({"\t\tPlayer.Colorset 7, \"$TXT_COLOR_LIGHTBLUE\",\t0xC0, 0xCF,  0xC2;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({""}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLAY A -1;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tPLAY ABCD 4;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tPLAY E 12;"}),
t({"",""}),t({"\t\tGoto Spawn;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\t\tPLAY F 6 BRIGHT;"}),
t({"",""}),t({"\t\tGoto Missile;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tPLAY G 4;"}),
t({"",""}),t({"\t\tPLAY G 4 A_Pain;"}),
t({"",""}),t({"\t\tGoto Spawn;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tPLAY H 0 A_PlayerSkinCheck(\"AltSkinDeath\");"}),
t({"",""}),t({"\tDeath1:"}),
t({"",""}),t({"\t\tPLAY H 10;"}),
t({"",""}),t({"\t\tPLAY I 10 A_PlayerScream;"}),
t({"",""}),t({"\t\tPLAY J 10 A_NoBlocking;"}),
t({"",""}),t({"\t\tPLAY KLM 10;"}),
t({"",""}),t({"\t\tPLAY N -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tPLAY O 0 A_PlayerSkinCheck(\"AltSkinXDeath\");"}),
t({"",""}),t({"\tXDeath1:"}),
t({"",""}),t({"\t\tPLAY O 5;"}),
t({"",""}),t({"\t\tPLAY P 5 A_XScream;"}),
t({"",""}),t({"\t\tPLAY Q 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tPLAY RSTUV 5;"}),
t({"",""}),t({"\t\tPLAY W -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tAltSkinDeath:"}),
t({"",""}),t({"\t\tPLAY H 6;"}),
t({"",""}),t({"\t\tPLAY I 6 A_PlayerScream;"}),
t({"",""}),t({"\t\tPLAY JK 6;"}),
t({"",""}),t({"\t\tPLAY L 6 A_NoBlocking;"}),
t({"",""}),t({"\t\tPLAY MNO 6;"}),
t({"",""}),t({"\t\tPLAY P -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tAltSkinXDeath:"}),
t({"",""}),t({"\t\tPLAY Q 5 A_PlayerScream;"}),
t({"",""}),t({"\t\tPLAY R 0 A_NoBlocking;"}),
t({"",""}),t({"\t\tPLAY R 5 A_SkullPop;"}),
t({"",""}),t({"\t\tPLAY STUVWX 5;"}),
t({"",""}),t({"\t\tPLAY Y -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("DoomUnusedStates", {
t({"DoomUnusedStates"}),
}),
s("DoSpecialDamage", {
t({"DoSpecialDamage"}),
}),
s("DoTakeInventory", {
t({"DoTakeInventory"}),
}),
s("double", {
t({"double"}),
}),
s("DrawSplash", {
t({"DrawSplash"}),
}),
s("DropInventory", {
t({"DropInventory"}),
}),
s("DropItem", {
t({"DropItem"}),
}),
s("DropItem", {
t({"DropItem \""}),i(1),t({"\";"}),
}),
s("dropoff", {
t({"dropoff"}),
}),
s("DualPainAttack", {
t({"A_DualPainAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_DualPainAttack"}),
}),
s("duration", {
t({"duration"}),
}),
s("eventhandler", {
t({"class "}),i(1),t({" : EventHandler {"}),
t({"",""}),t({"  "}),i(2),t({""}),
t({"",""}),t({"}"}),
}),
s("EvilEye", {
t({"EvilEye"}),
}),
s("exact", {
t({"exact"}),
}),
s("Explode", {
t({"A_Explode "}),i(1),t({"; //https://zdoom.org/wiki/A_Explode"}),
}),
s("ExplodeMissile", {
t({"ExplodeMissile"}),
}),
s("ExplosiveBarrel", {
t({"ExplosiveBarrel"}),
}),
s("ExplosiveBarrel", {
t({"class ExplosiveBarrel : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 20;"}),
t({"",""}),t({"\t\tRadius 10;"}),
t({"",""}),t({"\t\tHeight 42;"}),
t({"",""}),t({"\t\t+SOLID"}),
t({"",""}),t({"\t\t+SHOOTABLE"}),
t({"",""}),t({"\t\t+NOBLOOD"}),
t({"",""}),t({"\t\t+ACTIVATEMCROSS"}),
t({"",""}),t({"\t\t+DONTGIB"}),
t({"",""}),t({"\t\t+NOICEDEATH"}),
t({"",""}),t({"\t\t+OLDRADIUSDMG"}),
t({"",""}),t({"\t\tDeathSound \"world/barrelx\";"}),
t({"",""}),t({"\t\tObituary \"$OB_BARREL\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBAR1 AB 6;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBEXP A 5 BRIGHT;"}),
t({"",""}),t({"\t\tBEXP B 5 BRIGHT A_Scream;"}),
t({"",""}),t({"\t\tBEXP C 5 BRIGHT;"}),
t({"",""}),t({"\t\tBEXP D 10 BRIGHT A_Explode;"}),
t({"",""}),t({"\t\tBEXP E 10 BRIGHT;"}),
t({"",""}),t({"\t\tTNT1 A 1050 BRIGHT A_BarrelDestroy;"}),
t({"",""}),t({"\t\tTNT1 A 5 A_Respawn;"}),
t({"",""}),t({"\t\tWait;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("ExtChase", {
t({"A_ExtChase "}),i(1),t({"; //https://zdoom.org/wiki/A_ExtChase"}),
}),
s("F1Pic", {
t({"Level.F1Pic"}),
}),
s("FaceMaster", {
t({"A_FaceMaster "}),i(1),t({"; //https://zdoom.org/wiki/A_FaceMaster"}),
}),
s("FaceMovementDirection", {
t({"A_FaceMovementDirection "}),i(1),t({"; //https://zdoom.org/wiki/A_FaceMovementDirection"}),
}),
s("FaceTarget", {
t({"A_FaceTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_FaceTarget"}),
}),
s("faceto", {
t({"faceto"}),
}),
s("FaceTracer", {
t({"A_FaceTracer "}),i(1),t({"; //https://zdoom.org/wiki/A_FaceTracer"}),
}),
s("FadeIn", {
t({"A_FadeIn "}),i(1),t({"; //https://zdoom.org/wiki/A_FadeIn"}),
}),
s("FadeOut", {
t({"A_FadeOut "}),i(1),t({"; //https://zdoom.org/wiki/A_FadeOut"}),
}),
s("FadeTo", {
t({"A_FadeTo "}),i(1),t({"; //https://zdoom.org/wiki/A_FadeTo"}),
}),
s("Fall", {
t({"A_Fall "}),i(1),t({"; //https://zdoom.org/wiki/A_Fall"}),
}),
s("false", {
t({"false"}),
}),
s("FastChase", {
t({"A_FastChase "}),i(1),t({"; //https://zdoom.org/wiki/A_FastChase"}),
}),
s("FatAttack1", {
t({"A_FatAttack1 "}),i(1),t({"; //https://zdoom.org/wiki/A_FatAttack1"}),
}),
s("FatAttack2", {
t({"A_FatAttack2 "}),i(1),t({"; //https://zdoom.org/wiki/A_FatAttack2"}),
}),
s("FatAttack3", {
t({"A_FatAttack3 "}),i(1),t({"; //https://zdoom.org/wiki/A_FatAttack3"}),
}),
s("FatRaise", {
t({"A_FatRaise "}),i(1),t({"; //https://zdoom.org/wiki/A_FatRaise"}),
}),
s("FatShot", {
t({"FatShot"}),
}),
s("FatShot", {
t({"class FatShot : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 6;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 20;"}),
t({"",""}),t({"\t\tDamage 8;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t\tAlpha 1;"}),
t({"",""}),t({"\t\tSeeSound \"fatso/attack\";"}),
t({"",""}),t({"\t\tDeathSound \"fatso/shotx\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tMANF AB 4 BRIGHT;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tMISL B 8 BRIGHT;"}),
t({"",""}),t({"\t\tMISL C 6 BRIGHT;"}),
t({"",""}),t({"\t\tMISL D 4 BRIGHT;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Fatso", {
t({"Fatso"}),
}),
s("Fatso", {
t({"class Fatso : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 600;"}),
t({"",""}),t({"\t\tRadius 48;"}),
t({"",""}),t({"\t\tHeight 64;"}),
t({"",""}),t({"\t\tMass 1000;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 80;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\t+MAP07BOSS1"}),
t({"",""}),t({"\t\tSeeSound \"fatso/sight\";"}),
t({"",""}),t({"\t\tPainSound \"fatso/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"fatso/death\";"}),
t({"",""}),t({"\t\tActiveSound \"fatso/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_FATSO\";"}),
t({"",""}),t({"\t\tTag \"$FN_MANCU\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tFATT AB 15 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tFATT AABBCCDDEEFF 4 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tFATT G 20 A_FatRaise;"}),
t({"",""}),t({"\t\tFATT H 10 BRIGHT A_FatAttack1;"}),
t({"",""}),t({"\t\tFATT IG 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tFATT H 10 BRIGHT A_FatAttack2;"}),
t({"",""}),t({"\t\tFATT IG 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tFATT H 10 BRIGHT A_FatAttack3;"}),
t({"",""}),t({"\t\tFATT IG 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tFATT J 3;"}),
t({"",""}),t({"\t\tFATT J 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"    Death:"}),
t({"",""}),t({"\t\tFATT K 6;"}),
t({"",""}),t({"\t\tFATT L 6 A_Scream;"}),
t({"",""}),t({"\t\tFATT M 6 A_NoBlocking;"}),
t({"",""}),t({"\t\tFATT NOPQRS 6;"}),
t({"",""}),t({"\t\tFATT T -1 A_BossDeath;"}),
t({"",""}),t({"\t    Stop;"}),
t({"",""}),t({"\t Raise:"}),
t({"",""}),t({"\t\tFATT R 5;"}),
t({"",""}),t({"\t\tFATT QPONMLK 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("FCheckPosition", {
t({"FCheckPosition"}),
}),
s("FindFloorCeiling", {
t({"FindFloorCeiling"}),
}),
s("FindInventory", {
t({"FindInventory"}),
}),
s("FindState", {
t({"FindState"}),
}),
s("FindStateByString", {
t({"FindStateByString"}),
}),
s("Fire", {
t({"A_Fire "}),i(1),t({"; //https://zdoom.org/wiki/A_Fire"}),
}),
s("FireAssaultGun", {
t({"A_FireAssaultGun "}),i(1),t({"; //https://zdoom.org/wiki/A_FireAssaultGun"}),
}),
s("FireBFG", {
t({"A_FireBFG "}),i(1),t({"; //https://zdoom.org/wiki/A_FireBFG"}),
}),
s("FireBullets", {
t({"A_FireBullets "}),i(1),t({"; //https://zdoom.org/wiki/A_FireBullets"}),
}),
s("FireCGun", {
t({"A_FireCGun "}),i(1),t({"; //https://zdoom.org/wiki/A_FireCGun"}),
}),
s("FireCrackle", {
t({"A_FireCrackle "}),i(1),t({"; //https://zdoom.org/wiki/A_FireCrackle"}),
}),
s("FireCustomMissile", {
t({"A_FireCustomMissile "}),i(1),t({"; //https://zdoom.org/wiki/A_FireCustomMissile"}),
}),
s("FireMissile", {
t({"A_FireMissile "}),i(1),t({"; //https://zdoom.org/wiki/A_FireMissile"}),
}),
s("FireOldBFG", {
t({"A_FireOldBFG "}),i(1),t({"; //https://zdoom.org/wiki/A_FireOldBFG"}),
}),
s("FirePistol", {
t({"A_FirePistol "}),i(1),t({"; //https://zdoom.org/wiki/A_FirePistol"}),
}),
s("FirePlasma", {
t({"A_FirePlasma "}),i(1),t({"; //https://zdoom.org/wiki/A_FirePlasma"}),
}),
s("FireProjectile", {
t({"A_FireProjectile "}),i(1),t({"; //https://zdoom.org/wiki/A_FireProjectile"}),
}),
s("FireShotgun", {
t({"A_FireShotgun "}),i(1),t({"; //https://zdoom.org/wiki/A_FireShotgun"}),
}),
s("FireShotgun2", {
t({"A_FireShotgun2 "}),i(1),t({"; //https://zdoom.org/wiki/A_FireShotgun2"}),
}),
s("FireSTGrenade", {
t({"A_FireSTGrenade "}),i(1),t({"; //https://zdoom.org/wiki/A_FireSTGrenade"}),
}),
s("FirstInv", {
t({"FirstInv"}),
}),
s("Fist", {
t({"Fist"}),
}),
s("Fist", {
t({"class Fist : Weapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 3700;"}),
t({"",""}),t({"\t\tWeapon.Kickback 100;"}),
t({"",""}),t({"\t\tObituary \"$OB_MPFIST\";"}),
t({"",""}),t({"\t\tTag \"$TAG_FIST\";"}),
t({"",""}),t({"\t\t+WEAPON.WIMPY_WEAPON"}),
t({"",""}),t({"\t\t+WEAPON.MELEEWEAPON"}),
t({"",""}),t({"\t\t+WEAPON.NOAUTOSWITCHTO"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tPUNG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tPUNG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tPUNG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tPUNG B 4;"}),
t({"",""}),t({"\t\tPUNG C 4 A_Punch;"}),
t({"",""}),t({"\t\tPUNG D 5;"}),
t({"",""}),t({"\t\tPUNG C 4;"}),
t({"",""}),t({"\t\tPUNG B 5 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("FLAG_NO_CHANGE", {
t({"FLAG_NO_CHANGE"}),
}),
s("flags", {
t({"Level.flags"}),
}),
s("FLineTraceData", {
t({"FLineTraceData"}),
}),
s("FloatingSkull", {
t({"FloatingSkull"}),
}),
s("FLoopActiveSound", {
t({"A_FLoopActiveSound "}),i(1),t({"; //https://zdoom.org/wiki/A_FLoopActiveSound"}),
}),
s("+FLOORCLIP", {
t({"+FLOORCLIP"}),
}),
s("FogDensity", {
t({"Level.FogDensity"}),
}),
s("force", {
t({"force"}),
}),
s("forcedPain", {
t({"forcedPain"}),
}),
s("for_loop", {
t({"for (int i = 0; i < "}),i(1),t({"; i++){"}),
t({"",""}),t({"  "}),i(2),t({""}),
t({"",""}),t({"}"}),
}),
s("FoundItems", {
t({"Level.FoundItems"}),
}),
s("FoundSecrets", {
t({"Level.FoundSecrets"}),
}),
s("FreezeDeath", {
t({"A_FreezeDeath "}),i(1),t({"; //https://zdoom.org/wiki/A_FreezeDeath"}),
}),
s("FreezeDeathChunks", {
t({"A_FreezeDeathChunks "}),i(1),t({"; //https://zdoom.org/wiki/A_FreezeDeathChunks"}),
}),
s("friender", {
t({"friender"}),
}),
s("from", {
t({"Level.from"}),
}),
s("fromdecorate", {
t({"fromdecorate"}),
}),
s("fromList", {
t({"Level.fromList"}),
}),
s("frontonly", {
t({"frontonly"}),
}),
s("Frozen", {
t({"Level.Frozen"}),
}),
s("FTranslatedLineTarget", {
t({"FTranslatedLineTarget"}),
}),
s("fulldmgdistance", {
t({"fulldmgdistance"}),
}),
s("Functions", {
t({"Functions"}),
}),
s("gameinfo", {
t({"GameInfo"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("gameinfo_", {
t({"gameinfo"}),
t({"",""}),t({"{"}),
t({"",""}),t({"    titlepage = \"TITLEPIC\""}),i(1),t({""}),
t({"",""}),t({"    creditpage = \"CREDIT\""}),
t({"",""}),t({"    titlemusic = \"$MUSIC_DM2TTL\""}),
t({"",""}),t({"    titletime = 11"}),
t({"",""}),t({"    advisorytime = 0"}),
t({"",""}),t({"    pagetime = 5"}),
t({"",""}),t({"    chatsound = \"misc/chat\""}),
t({"",""}),t({"    finalemusic = \"$MUSIC_READ_M\""}),
t({"",""}),t({"    finaleflat = \"SLIME16\""}),
t({"",""}),t({"    finalepage = \"CREDIT\""}),
t({"",""}),t({"    infopage = \"HELP\", \"CREDIT\""}),
t({"",""}),t({"    quitsound = \"menu/quit2\""}),
t({"",""}),t({"    borderflat = \"GRNROCK\""}),
t({"",""}),t({"    border = DoomBorder"}),
t({"",""}),t({"    telefogheight = 0"}),
t({"",""}),t({"    defkickback = 100"}),
t({"",""}),t({"    skyflatname = \"F_SKY1\""}),
t({"",""}),t({"    translator = \"xlat/doom.txt\""}),
t({"",""}),t({"    mapinfo = \"mapinfo/doom2.txt\""}),
t({"",""}),t({"    defaultbloodcolor = \"68 00 00\""}),
t({"",""}),t({"    defaultbloodparticlecolor = \"ff 00 00\""}),
t({"",""}),t({"    backpacktype = \"Backpack\""}),
t({"",""}),t({"    armoricons = \"ARM1A0\", 0.5, \"ARM2A0\""}),
t({"",""}),t({"    statusbar = \"sbarinfo/doom.txt\""}),
t({"",""}),t({"    intermissionmusic = \"$MUSIC_DM2INT\""}),
t({"",""}),t({"    intermissioncounter = true"}),
t({"",""}),t({"    weaponslot = 1, \"Fist\", \"Chainsaw\""}),
t({"",""}),t({"    weaponslot = 2, \"Pistol\""}),
t({"",""}),t({"    weaponslot = 3, \"Shotgun\", \"SuperShotgun\""}),
t({"",""}),t({"    weaponslot = 4, \"Chaingun\""}),
t({"",""}),t({"    weaponslot = 5, \"RocketLauncher\""}),
t({"",""}),t({"    weaponslot = 6, \"PlasmaRifle\""}),
t({"",""}),t({"    weaponslot = 7, \"BFG9000\""}),
t({"",""}),t({"    dimcolor = \"ff d7 00\""}),
t({"",""}),t({"    dimamount = 0.2"}),
t({"",""}),t({"    definventorymaxamount = 25"}),
t({"",""}),t({"    defaultrespawntime = 12"}),
t({"",""}),t({"    defaultdropstyle = 1"}),
t({"",""}),t({"    endoom = \"ENDOOM\""}),
t({"",""}),t({"    pickupcolor = \"d6 ba 45\""}),
t({"",""}),t({"    quitmessages = \"$QUITMSG\", \"$QUITMSG1\", \"$QUITMSG2\", \"$QUITMSG3\", \"$QUITMSG4\", \"$QUITMSG5\", \"$QUITMSG6\", \"$QUITMSG7\","}),
t({"",""}),t({"                   \"$QUITMSG8\", \"$QUITMSG9\", \"$QUITMSG10\", \"$QUITMSG11\", \"$QUITMSG12\", \"$QUITMSG13\", \"$QUITMSG14\""}),
t({"",""}),t({"                  "}),
t({"",""}),t({"    menufontcolor_title = \"RED\""}),
t({"",""}),t({"    menufontcolor_label = \"UNTRANSLATED\""}),
t({"",""}),t({"    menufontcolor_value = \"GRAY\""}),
t({"",""}),t({"    menufontcolor_action = \"GRAY\""}),
t({"",""}),t({"    menufontcolor_header = \"GOLD\""}),
t({"",""}),t({"    menufontcolor_highlight = \"YELLOW\""}),
t({"",""}),t({"    menufontcolor_selection = \"BRICK\""}),
t({"",""}),t({"    menubackbutton = \"M_BACK_D\""}),
t({"",""}),t({"    playerclasses = \"DoomPlayer\""}),
t({"",""}),t({"    pausesign = \"M_PAUSE\""}),
t({"",""}),t({"    gibfactor = 1"}),
t({"",""}),t({"}"}),
}),
s("GenericFreezeDeath", {
t({"A_GenericFreezeDeath "}),i(1),t({"; //https://zdoom.org/wiki/A_GenericFreezeDeath"}),
}),
s("GetAge", {
t({"GetAge"}),
}),
s("GetAmmoCapacity", {
t({"GetAmmoCapacity"}),
}),
s("GetBobOffset", {
t({"GetBobOffset"}),
}),
s("GetCameraHeight", {
t({"GetCameraHeight"}),
}),
s("GetCharacterName", {
t({"GetCharacterName"}),
}),
s("GetChecksum", {
t({"Level.GetChecksum"}),
}),
s("GetClass", {
t({"GetClass"}),
}),
s("GetClassName", {
t({"GetClassName"}),
}),
s("GetDefaultByType", {
t({"GetDefaultByType"}),
}),
s("GetDefaultSpeed", {
t({"GetDefaultSpeed"}),
}),
s("GetDropItems", {
t({"GetDropItems"}),
}),
s("GetFloorTerrain", {
t({"GetFloorTerrain"}),
}),
s("GetFriction", {
t({"GetFriction"}),
}),
s("GetGravity", {
t({"GetGravity"}),
}),
s("GetHurt", {
t({"A_GetHurt "}),i(1),t({"; //https://zdoom.org/wiki/A_GetHurt"}),
}),
s("GetLevelSpawnTime", {
t({"GetLevelSpawnTime"}),
}),
s("GetModifiedDamage", {
t({"GetModifiedDamage"}),
}),
s("GetPointer", {
t({"GetPointer"}),
}),
s("GetRadiusDamage", {
t({"GetRadiusDamage"}),
}),
s("GetRenderStyle", {
t({"GetRenderStyle"}),
}),
s("GetReplacee", {
t({"GetReplacee"}),
}),
s("GetReplacement", {
t({"GetReplacement"}),
}),
s("GetSkyBoxPortalActor", {
t({"Level.GetSkyBoxPortalActor"}),
}),
s("GetSpawnableType", {
t({"GetSpawnableType"}),
}),
s("GetSpecies", {
t({"GetSpecies"}),
}),
s("GetSpriteIndex", {
t({"GetSpriteIndex"}),
}),
s("GetTag", {
t({"GetTag"}),
}),
s("GetUdmfFloatEUdmf", {
t({"Level.GetUdmfFloatEUdmf"}),
}),
s("GetUdmfIntEUdmf", {
t({"Level.GetUdmfIntEUdmf"}),
}),
s("GetUdmfStringEUdmf", {
t({"Level.GetUdmfStringEUdmf"}),
}),
s("GibbedMarine", {
t({"GibbedMarine"}),
}),
s("GibbedMarineExtra", {
t({"GibbedMarineExtra"}),
}),
s("GibbedMarine", {
t({"class GibbedMarine : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLAY W -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Gibs", {
t({"Gibs"}),
}),
s("GiveAmmo", {
t({"GiveAmmo"}),
}),
s("GiveBody", {
t({"GiveBody"}),
}),
s("givecheat", {
t({"givecheat"}),
}),
s("GiveInventory", {
t({"GiveInventory"}),
}),
s("GiveInventoryType", {
t({"GiveInventoryType"}),
}),
s("GiveSecret", {
t({"GiveSecret"}),
}),
s("GiveToChildren", {
t({"A_GiveToChildren "}),i(1),t({"; //https://zdoom.org/wiki/A_GiveToChildren"}),
}),
s("GiveToSiblings", {
t({"A_GiveToSiblings "}),i(1),t({"; //https://zdoom.org/wiki/A_GiveToSiblings"}),
}),
s("GiveToTarget", {
t({"A_GiveToTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_GiveToTarget"}),
}),
s("Goto", {
t({"Goto"}),
}),
s("Gravity", {
t({"Level.Gravity"}),
}),
s("GreenArmor", {
t({"GreenArmor"}),
}),
s("GreenArmor", {
t({"class GreenArmor : BasicArmorPickup "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 16;"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTARMOR\";"}),
t({"",""}),t({"\t\tInventory.Icon \"ARM1A0\";"}),
t({"",""}),t({"\t\tArmor.SavePercent 33.335;"}),
t({"",""}),t({"\t\tArmor.SaveAmount 100;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tARM1 A 6;"}),
t({"",""}),t({"\t\tARM1 B 7 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("GreenTorch", {
t({"GreenTorch"}),
}),
s("Grenade", {
t({"Grenade"}),
}),
s("Grenade", {
t({"class Grenade : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 8;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 25;"}),
t({"",""}),t({"\t\tDamage 20;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t-NOGRAVITY"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+DEHEXPLOSION"}),
t({"",""}),t({"\t\t+GRENADETRAIL"}),
t({"",""}),t({"\t\tBounceType \"Doom\";"}),
t({"",""}),t({"\t\tGravity 0.25;"}),
t({"",""}),t({"\t\tSeeSound \"weapons/grenlf\";"}),
t({"",""}),t({"\t\tDeathSound \"weapons/grenlx\";"}),
t({"",""}),t({"\t\tBounceSound \"weapons/grbnce\";"}),
t({"",""}),t({"\t\tObituary \"$OB_GRENADE\";"}),
t({"",""}),t({"\t\tDamageType \"Grenade\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSGRN A 1 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tMISL B 8 Bright A_Explode;"}),
t({"",""}),t({"\t\tMISL C 6 Bright;"}),
t({"",""}),t({"\t\tMISL D 4 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tGrenade:"}),
t({"",""}),t({"\t\tMISL A 1000 A_Die;"}),
t({"",""}),t({"\t\tWait;"}),
t({"",""}),t({"\tDetonate:"}),
t({"",""}),t({"\t\tMISL B 4 A_Scream;"}),
t({"",""}),t({"\t\tMISL C 6 A_Detonate;"}),
t({"",""}),t({"\t\tMISL D 10;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tMushroom:"}),
t({"",""}),t({"\t\tMISL B 8 A_Mushroom;"}),
t({"",""}),t({"\t\tGoto Death+1;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("GunFlash", {
t({"A_GunFlash "}),i(1),t({"; //https://zdoom.org/wiki/A_GunFlash"}),
}),
s("HandleSpawnFlags", {
t({"HandleSpawnFlags"}),
}),
s("HangBNoBrain", {
t({"HangBNoBrain"}),
}),
s("HangNoGuts", {
t({"HangNoGuts"}),
}),
s("HangTLookingDown", {
t({"HangTLookingDown"}),
}),
s("HangTLookingUp", {
t({"HangTLookingUp"}),
}),
s("HangTNoBrain", {
t({"HangTNoBrain"}),
}),
s("HangTSkull", {
t({"HangTSkull"}),
}),
s("HeadAttack", {
t({"A_HeadAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_HeadAttack"}),
}),
s("HeadCandles", {
t({"HeadCandles"}),
}),
s("HeadOnAStick", {
t({"HeadOnAStick"}),
}),
s("HeadsOnAStick", {
t({"HeadsOnAStick"}),
}),
s("Health", {
t({"Health"}),
}),
s("HealthBonus", {
t({"HealthBonus"}),
}),
s("healthbonus_replace", {
t({"class HealthBonus2 : Health Replaces HealthBonus"}),i(1),t({" "}),
t({"",""}),t({"{"}),
t({"",""}),t({"  //https://github.com/ZDoom/gzdoom/blob/master/wadsrc/static/zscript/actors/doom/doomhealth.zs"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\tInventory.Amount 10;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 500;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTHTHBONUS\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBON1 ABCDCB 6;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({""}),
t({"",""}),t({"\t//==========================================================================="}),
t({"",""}),t({"\t//"}),
t({"",""}),t({"\t// TryPickup"}),
t({"",""}),t({"\t//"}),
t({"",""}),t({"\t//==========================================================================="}),
t({"",""}),t({""}),
t({"",""}),t({"\toverride bool TryPickup (in out Actor other)"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tPrevHealth = other.player != NULL ? other.player.health : other.health;"}),
t({"",""}),t({""}),
t({"",""}),t({"\t\t// Dehacked max health is compatibility dependent because Boom interpreted this value wrong."}),
t({"",""}),t({"\t\tlet maxamt = MaxAmount;"}),
t({"",""}),t({"\t\tif (maxamt < 0)"}),
t({"",""}),t({"\t\t{"}),
t({"",""}),t({"\t\t\tmaxamt = deh.MaxHealth;"}),
t({"",""}),t({"\t\t\tif (!(Level.compatflags & COMPATF_DEHHEALTH)) maxamt *= 2;"}),
t({"",""}),t({"\t\t}"}),
t({"",""}),t({""}),
t({"",""}),t({"\t\tif (other.GiveBody(Amount, maxamt))"}),
t({"",""}),t({"\t\t{"}),
t({"",""}),t({"\t\t\tGoAwayAndDie();"}),
t({"",""}),t({"\t\t\treturn true;"}),
t({"",""}),t({"\t\t}"}),
t({"",""}),t({"\t\treturn false;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({""}),
t({"",""}),t({"}"}),
}),
s("HealthBonus", {
t({"class HealthBonus : Health "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\tInventory.Amount 1;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 200;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTHTHBONUS\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBON1 ABCDCB 6;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({""}),
t({"",""}),t({"}"}),
}),
s("healthlarge_replace", {
t({"class Medikit2 : Health Replaces Medikit"}),i(0),t({" "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  //https://github.com/ZDoom/gzdoom/blob/master/wadsrc/static/zscript/actors/doom/doomhealth.zs"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Amount 25;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTMEDIKIT\";"}),
t({"",""}),t({"\t\tHealth.LowMessage 25, \"$GOTMEDINEED\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tMEDI A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("healthsmall_replace", {
t({"class Stimpack2 : Health Replaces Stimpack"}),i(1),t({" "}),
t({"",""}),t({"{"}),
t({"",""}),t({"  //https://github.com/ZDoom/gzdoom/blob/master/wadsrc/static/zscript/actors/doom/doomhealth.zs"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Amount 10;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSTIM\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSTIM A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("HeartColumn", {
t({"HeartColumn"}),
}),
s("height", {
t({"Level.height"}),
}),
s("Height", {
t({"Height"}),
}),
s("heightoffset", {
t({"heightoffset"}),
}),
s("HellKnight", {
t({"HellKnight"}),
}),
s("hellknight", {
t({"class "}),i(1),t({" : Actor Replaces HellKnight "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 500;"}),
t({"",""}),t({"\t\t-BOSSDEATH;"}),
t({"",""}),t({"\t\tSeeSound \"knight/sight\";"}),
t({"",""}),t({"\t\tActiveSound \"knight/active\";"}),
t({"",""}),t({"\t\tPainSound \"knight/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"knight/death\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_KNIGHTHIT\";"}),
t({"",""}),t({"\t\tObituary \"$OB_KNIGHT\";"}),
t({"",""}),t({"\t\tTag \"$FN_HELL\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBOS2 AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tBOS2 AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tBOS2 EF 8 A_FaceTarget;"}),
t({"",""}),t({"\t\tBOS2 G 8 A_BruisAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tBOS2 H  2;"}),
t({"",""}),t({"\t\tBOS2 H  2 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBOS2 I  8;"}),
t({"",""}),t({"\t\tBOS2 J  8 A_Scream;"}),
t({"",""}),t({"\t\tBOS2 K  8;"}),
t({"",""}),t({"\t\tBOS2 L  8 A_NoBlocking;"}),
t({"",""}),t({"\t\tBOS2 MN 8;"}),
t({"",""}),t({"\t\tBOS2 O -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tBOS2 O 8;"}),
t({"",""}),t({"\t\tBOS2 NMLKJI  8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({"extend class Actor"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tvoid A_BruisAttack()"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tlet targ = target;"}),
t({"",""}),t({"\t\tif (targ)"}),
t({"",""}),t({"\t\t{"}),
t({"",""}),t({"\t\t\tif (CheckMeleeRange())"}),
t({"",""}),t({"\t\t\t{"}),
t({"",""}),t({"\t\t\t\tint damage = random[pr_bruisattack](1, 8) * 10;"}),
t({"",""}),t({"\t\t\t\tA_StartSound (\"baron/melee\", CHAN_WEAPON);"}),
t({"",""}),t({"\t\t\t\tint newdam = target.DamageMobj (self, self, damage, \"Melee\");"}),
t({"",""}),t({"\t\t\t\ttarg.TraceBleed (newdam > 0 ? newdam : damage, self);"}),
t({"",""}),t({"\t\t\t}"}),
t({"",""}),t({"\t\t\telse"}),
t({"",""}),t({"\t\t\t{"}),
t({"",""}),t({"\t\t\t\t// launch a missile"}),
t({"",""}),t({"\t\t\t\tSpawnMissile (target, \"BaronBall\");"}),
t({"",""}),t({"\t\t\t}"}),
t({"",""}),t({"\t\t}"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("HellKnight", {
t({"class HellKnight : BaronOfHell "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 500;"}),
t({"",""}),t({"\t\t-BOSSDEATH;"}),
t({"",""}),t({"\t\tSeeSound \"knight/sight\";"}),
t({"",""}),t({"\t\tActiveSound \"knight/active\";"}),
t({"",""}),t({"\t\tPainSound \"knight/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"knight/death\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_KNIGHTHIT\";"}),
t({"",""}),t({"\t\tObituary \"$OB_KNIGHT\";"}),
t({"",""}),t({"\t\tTag \"$FN_HELL\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBOS2 AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tBOS2 AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tBOS2 EF 8 A_FaceTarget;"}),
t({"",""}),t({"\t\tBOS2 G 8 A_BruisAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tBOS2 H  2;"}),
t({"",""}),t({"\t\tBOS2 H  2 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tBOS2 I  8;"}),
t({"",""}),t({"\t\tBOS2 J  8 A_Scream;"}),
t({"",""}),t({"\t\tBOS2 K  8;"}),
t({"",""}),t({"\t\tBOS2 L  8 A_NoBlocking;"}),
t({"",""}),t({"\t\tBOS2 MN 8;"}),
t({"",""}),t({"\t\tBOS2 O -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tBOS2 O 8;"}),
t({"",""}),t({"\t\tBOS2 NMLKJI  8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("hereticLower", {
t({"Level.hereticLower"}),
}),
s("HideThing", {
t({"A_HideThing "}),i(1),t({"; //https://zdoom.org/wiki/A_HideThing"}),
}),
s("hitangle", {
t({"hitangle"}),
}),
s("hitdir", {
t({"hitdir"}),
}),
s("HitFloor", {
t({"HitFloor"}),
}),
s("HitFriend", {
t({"HitFriend"}),
}),
s("HitWater", {
t({"HitWater"}),
}),
s("Hoof", {
t({"A_Hoof "}),i(1),t({"; //https://zdoom.org/wiki/A_Hoof"}),
}),
s("IceGuyDie", {
t({"A_IceGuyDie "}),i(1),t({"; //https://zdoom.org/wiki/A_IceGuyDie"}),
}),
s("id", {
t({"Level.id"}),
}),
s("imp", {
t({"class "}),i(1),t({" : Actor Replaces DoomImp"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 60;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 200;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"imp/sight\";"}),
t({"",""}),t({"\t\tPainSound \"imp/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"imp/death\";"}),
t({"",""}),t({"\t\tActiveSound \"imp/active\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_IMPHIT\";"}),
t({"",""}),t({"\t\tObituary \"$OB_IMP\";"}),
t({"",""}),t({"\t\tTag \"$FN_IMP\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tTROO AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tTROO AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tTROO EF 8 A_FaceTarget;"}),
t({"",""}),t({"\t\tTROO G 6 A_TroopAttack ;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tTROO H 2;"}),
t({"",""}),t({"\t\tTROO H 2 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tTROO I 8;"}),
t({"",""}),t({"\t\tTROO J 8 A_Scream;"}),
t({"",""}),t({"\t\tTROO K 6;"}),
t({"",""}),t({"\t\tTROO L 6 A_NoBlocking;"}),
t({"",""}),t({"\t\tTROO M -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tTROO N 5;"}),
t({"",""}),t({"\t\tTROO O 5 A_XScream;"}),
t({"",""}),t({"\t\tTROO P 5;"}),
t({"",""}),t({"\t\tTROO Q 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tTROO RST 5;"}),
t({"",""}),t({"\t\tTROO U -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tTROO ML 8;"}),
t({"",""}),t({"\t\tTROO KJI 6;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("index", {
t({"Level.index"}),
}),
s("InfiniteFlight", {
t({"Level.InfiniteFlight"}),
}),
s("inflictor", {
t({"inflictor"}),
}),
s("Information", {
t({"Information"}),
}),
s("Infrared", {
t({"Infrared"}),
}),
s("Infrared", {
t({"class Infrared : PowerupGiver "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.AUTOACTIVATE"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\tInventory.MaxAmount 0;"}),
t({"",""}),t({"\t\tPowerup.Type \"PowerLightAmp\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTVISOR\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPVIS A 6 Bright;"}),
t({"",""}),t({"\t\tPVIS B 6;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({"\t"}),
}),
s("InitSpawnedItem", {
t({"InitSpawnedItem"}),
}),
s("InStateSequence", {
t({"InStateSequence"}),
}),
s("Inventory", {
t({"Inventory"}),
}),
s("InvulnerabilitySphere", {
t({"InvulnerabilitySphere"}),
}),
s("InvulnerabilitySphere", {
t({"class InvulnerabilitySphere : PowerupGiver "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.AUTOACTIVATE"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\t+INVENTORY.BIGPOWERUP"}),
t({"",""}),t({"\t\tInventory.MaxAmount 0;"}),
t({"",""}),t({"\t\tPowerup.Type \"PowerInvulnerable\";"}),
t({"",""}),t({"\t\tPowerup.Color \"InverseMap\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTINVUL\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPINV ABCD 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("IsActorPlayingSound", {
t({"IsActorPlayingSound"}),
}),
s("IsCrouchingAllowed", {
t({"Level.IsCrouchingAllowed"}),
}),
s("IsFreeLookAllowed", {
t({"Level.IsFreeLookAllowed"}),
}),
s("IsFriend", {
t({"IsFriend"}),
}),
s("isFrozen", {
t({"isFrozen"}),
}),
s("IsFrozen", {
t({"Level.IsFrozen"}),
}),
s("IsHostile", {
t({"IsHostile"}),
}),
s("IsJumpingAllowed", {
t({"Level.IsJumpingAllowed"}),
}),
s("IsPointerEqual", {
t({"IsPointerEqual"}),
}),
s("IsTeammate", {
t({"IsTeammate"}),
}),
s("IsVisible", {
t({"IsVisible"}),
}),
s("IsZeroDamage", {
t({"IsZeroDamage"}),
}),
s("itemamount", {
t({"itemamount"}),
}),
s("itemclass", {
t({"itemclass"}),
}),
s("itemcls", {
t({"itemcls"}),
}),
s("itemtype", {
t({"itemtype"}),
}),
s("JIH", {
t({"JIH"}),
}),
s("Jump", {
t({"A_Jump "}),i(1),t({"; //https://zdoom.org/wiki/A_Jump"}),
}),
s("JumpIf", {
t({"A_JumpIf "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIf"}),
}),
s("JumpIfArmorType", {
t({"A_JumpIfArmorType "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfArmorType"}),
}),
s("JumpIfCloser", {
t({"A_JumpIfCloser "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfCloser"}),
}),
s("JumpIfHealthLower", {
t({"A_JumpIfHealthLower "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfHealthLower"}),
}),
s("JumpIfHigherOrLower", {
t({"A_JumpIfHigherOrLower "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfHigherOrLower"}),
}),
s("JumpIfInTargetInventory", {
t({"A_JumpIfInTargetInventory "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfInTargetInventory"}),
}),
s("JumpIfInTargetLOS", {
t({"A_JumpIfInTargetLOS "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfInTargetLOS"}),
}),
s("JumpIfInventory", {
t({"A_JumpIfInventory "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfInventory"}),
}),
s("JumpIfMasterCloser", {
t({"A_JumpIfMasterCloser "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfMasterCloser"}),
}),
s("JumpIfNoAmmo", {
t({"A_JumpIfNoAmmo "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfNoAmmo"}),
}),
s("JumpIfTargetInLOS", {
t({"A_JumpIfTargetInLOS "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfTargetInLOS"}),
}),
s("JumpIfTargetInsideMeleeRange", {
t({"A_JumpIfTargetInsideMeleeRange "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfTargetInsideMeleeRange"}),
}),
s("JumpIfTargetOutsideMeleeRange", {
t({"A_JumpIfTargetOutsideMeleeRange "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfTargetOutsideMeleeRange"}),
}),
s("JumpIfTracerCloser", {
t({"A_JumpIfTracerCloser "}),i(1),t({"; //https://zdoom.org/wiki/A_JumpIfTracerCloser"}),
}),
s("KeenDie", {
t({"A_KeenDie "}),i(1),t({"; //https://zdoom.org/wiki/A_KeenDie"}),
}),
s("keepFacing", {
t({"Level.keepFacing"}),
}),
s("KeepFullInventory", {
t({"Level.KeepFullInventory"}),
}),
s("KillChildren", {
t({"A_KillChildren "}),i(1),t({"; //https://zdoom.org/wiki/A_KillChildren"}),
}),
s("KilledMonsters", {
t({"Level.Killed_Monsters"}),
}),
s("KillMaster", {
t({"A_KillMaster "}),i(1),t({"; //https://zdoom.org/wiki/A_KillMaster"}),
}),
s("KillSiblings", {
t({"A_KillSiblings "}),i(1),t({"; //https://zdoom.org/wiki/A_KillSiblings"}),
}),
s("KillTarget", {
t({"A_KillTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_KillTarget"}),
}),
s("KillTracer", {
t({"A_KillTracer "}),i(1),t({"; //https://zdoom.org/wiki/A_KillTracer"}),
}),
s("KlaxonBlare", {
t({"A_KlaxonBlare "}),i(1),t({"; //https://zdoom.org/wiki/A_KlaxonBlare"}),
}),
s("length", {
t({"length"}),
}),
s("levelName", {
t({"Level.levelName"}),
}),
s("LevelName", {
t({"Level.LevelName"}),
}),
s("LevelNum", {
t({"Level.LevelNum"}),
}),
s("LevelPostProcessor", {
t({"class MyLevelPostProcessor : LevelPostProcessor "}),
t({"",""}),t({"{"}),
t({"",""}),t({"  protected void Apply(Name checksum, String mapname)"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"    "}),i(1),t({""}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
}),
s("Light", {
t({"A_Light "}),i(1),t({"; //https://zdoom.org/wiki/A_Light"}),
}),
s("Light0", {
t({"A_Light0 "}),i(1),t({"; //https://zdoom.org/wiki/A_Light0"}),
}),
s("Light1", {
t({"A_Light1 "}),i(1),t({"; //https://zdoom.org/wiki/A_Light1"}),
}),
s("Light2", {
t({"A_Light2 "}),i(1),t({"; //https://zdoom.org/wiki/A_Light2"}),
}),
s("LightInverse", {
t({"A_LightInverse "}),i(1),t({"; //https://zdoom.org/wiki/A_LightInverse"}),
}),
s("LightLevel", {
t({"LightLevel"}),
}),
s("limit", {
t({"Level.limit"}),
}),
s("LineAttack", {
t({"LineAttack"}),
}),
s("lineSide", {
t({"Level.lineSide"}),
}),
s("LineTrace", {
t({"LineTrace"}),
}),
s("LinkContext", {
t({"LinkContext"}),
}),
s("linked", {
t({"linked"}),
}),
s("LinkToWorld", {
t({"LinkToWorld"}),
}),
s("LiveStick", {
t({"LiveStick"}),
}),
s("locknum", {
t({"locknum"}),
}),
s("Log", {
t({"A_Log "}),i(1),t({"; //https://zdoom.org/wiki/A_Log"}),
}),
s("LogFloat", {
t({"A_LogFloat "}),i(1),t({"; //https://zdoom.org/wiki/A_LogFloat"}),
}),
s("LogInt", {
t({"A_LogInt "}),i(1),t({"; //https://zdoom.org/wiki/A_LogInt"}),
}),
s("Look", {
t({"A_Look "}),i(1),t({"; //https://zdoom.org/wiki/A_Look"}),
}),
s("Look2", {
t({"A_Look2 "}),i(1),t({"; //https://zdoom.org/wiki/A_Look2"}),
}),
s("LookEx", {
t({"A_LookEx "}),i(1),t({"; //https://zdoom.org/wiki/A_LookEx"}),
}),
s("LookExParams", {
t({"LookExParams"}),
}),
s("LookForEnemies", {
t({"LookForEnemies"}),
}),
s("LookForMonsters", {
t({"LookForMonsters"}),
}),
s("LookForPlayers", {
t({"LookForPlayers"}),
}),
s("LookForTid", {
t({"LookForTid"}),
}),
s("Loop", {
t({"Loop"}),
}),
s("LoopActiveSound", {
t({"A_LoopActiveSound "}),i(1),t({"; //https://zdoom.org/wiki/A_LoopActiveSound"}),
}),
s("LostSoul", {
t({"LostSoul"}),
}),
s("lostsoul", {
t({"class "}),i(1),t({" : Actor Replaces LostSoul"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 100;"}),
t({"",""}),t({"\t\tRadius 16;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 50;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tDamage 3;"}),
t({"",""}),t({"\t\tPainChance 256;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOAT +NOGRAVITY +MISSILEMORE +DONTFALL +NOICEDEATH +ZDOOMTRANS +RETARGETAFTERSLAM"}),
t({"",""}),t({"\t\tAttackSound \"skull/melee\";"}),
t({"",""}),t({"\t\tPainSound \"skull/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"skull/death\";"}),
t({"",""}),t({"\t\tActiveSound \"skull/active\";"}),
t({"",""}),t({"\t\tRenderStyle \"SoulTrans\";"}),
t({"",""}),t({"\t\tObituary \"$OB_SKULL\";"}),
t({"",""}),t({"\t\tTag \"$FN_LOST\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSKUL AB 10 BRIGHT A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSKUL AB 6 BRIGHT A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSKUL C 10 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tSKUL D 4 BRIGHT A_SkullAttack;"}),
t({"",""}),t({"\t\tSKUL CD 4 BRIGHT;"}),
t({"",""}),t({"\t\tGoto Missile+2;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSKUL E 3 BRIGHT;"}),
t({"",""}),t({"\t\tSKUL E 3 BRIGHT A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSKUL F 6 BRIGHT;"}),
t({"",""}),t({"\t\tSKUL G 6 BRIGHT A_Scream;"}),
t({"",""}),t({"\t\tSKUL H 6 BRIGHT;"}),
t({"",""}),t({"\t\tSKUL I 6 BRIGHT A_NoBlocking;"}),
t({"",""}),t({"\t\tSKUL J 6;"}),
t({"",""}),t({"\t\tSKUL K 6;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("LostSoul", {
t({"class LostSoul : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 100;"}),
t({"",""}),t({"\t\tRadius 16;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 50;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tDamage 3;"}),
t({"",""}),t({"\t\tPainChance 256;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOAT +NOGRAVITY +MISSILEMORE +DONTFALL +NOICEDEATH +ZDOOMTRANS +RETARGETAFTERSLAM"}),
t({"",""}),t({"\t\tAttackSound \"skull/melee\";"}),
t({"",""}),t({"\t\tPainSound \"skull/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"skull/death\";"}),
t({"",""}),t({"\t\tActiveSound \"skull/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_SKULL\";"}),
t({"",""}),t({"\t\tTag \"$FN_LOST\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSKUL AB 10 BRIGHT A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSKUL AB 6 BRIGHT A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSKUL C 10 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tSKUL D 4 BRIGHT A_SkullAttack;"}),
t({"",""}),t({"\t\tSKUL CD 4 BRIGHT;"}),
t({"",""}),t({"\t\tGoto Missile+2;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSKUL E 3 BRIGHT;"}),
t({"",""}),t({"\t\tSKUL E 3 BRIGHT A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSKUL F 6 BRIGHT;"}),
t({"",""}),t({"\t\tSKUL G 6 BRIGHT A_Scream;"}),
t({"",""}),t({"\t\tSKUL H 6 BRIGHT;"}),
t({"",""}),t({"\t\tSKUL I 6 BRIGHT A_NoBlocking;"}),
t({"",""}),t({"\t\tSKUL J 6;"}),
t({"",""}),t({"\t\tSKUL K 6;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Lower", {
t({"A_Lower "}),i(1),t({"; //https://zdoom.org/wiki/A_Lower"}),
}),
s("LowGravity", {
t({"A_LowGravity "}),i(1),t({"; //https://zdoom.org/wiki/A_LowGravity"}),
}),
s("Mancubus", {
t({"Mancubus"}),
}),
s("Mancubus", {
t({"class Fatso : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 600;"}),
t({"",""}),t({"\t\tRadius 48;"}),
t({"",""}),t({"\t\tHeight 64;"}),
t({"",""}),t({"\t\tMass 1000;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 80;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\t+MAP07BOSS1"}),
t({"",""}),t({"\t\tSeeSound \"fatso/sight\";"}),
t({"",""}),t({"\t\tPainSound \"fatso/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"fatso/death\";"}),
t({"",""}),t({"\t\tActiveSound \"fatso/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_FATSO\";"}),
t({"",""}),t({"\t\tTag \"$FN_MANCU\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tFATT AB 15 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tFATT AABBCCDDEEFF 4 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tFATT G 20 A_FatRaise;"}),
t({"",""}),t({"\t\tFATT H 10 BRIGHT A_FatAttack1;"}),
t({"",""}),t({"\t\tFATT IG 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tFATT H 10 BRIGHT A_FatAttack2;"}),
t({"",""}),t({"\t\tFATT IG 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tFATT H 10 BRIGHT A_FatAttack3;"}),
t({"",""}),t({"\t\tFATT IG 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tFATT J 3;"}),
t({"",""}),t({"\t\tFATT J 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"    Death:"}),
t({"",""}),t({"\t\tFATT K 6;"}),
t({"",""}),t({"\t\tFATT L 6 A_Scream;"}),
t({"",""}),t({"\t\tFATT M 6 A_NoBlocking;"}),
t({"",""}),t({"\t\tFATT NOPQRS 6;"}),
t({"",""}),t({"\t\tFATT T -1 A_BossDeath;"}),
t({"",""}),t({"\t    Stop;"}),
t({"",""}),t({"\t Raise:"}),
t({"",""}),t({"\t\tFATT R 5;"}),
t({"",""}),t({"\t\tFATT QPONMLK 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("mapinfo", {
t({"MapInfo{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("MapName", {
t({"Level.MapName"}),
}),
s("mapNameColor", {
t({"Level.mapNameColor"}),
}),
s("MapTime", {
t({"Level.MapTime"}),
}),
s("MapType", {
t({"Level.MapType"}),
}),
s("map", {
t({"map MAP"}),i(1),t({" \""}),i(2),t({"\""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  titlepatch = \""}),rep(2),t({"\""}),
t({"",""}),t({"    sky1 = \"SKY1\", 0"}),
t({"",""}),t({"    music = \"$MUSIC_ROMERO\""}),
t({"",""}),t({"}"}),
}),
s("Marineberserk", {
t({"Marineberserk"}),
}),
s("MarineBerserk", {
t({"MarineBerserk"}),
}),
s("MarineBFG", {
t({"MarineBFG"}),
}),
s("Marinechaingun", {
t({"Marinechaingun"}),
}),
s("MarineChaingun", {
t({"MarineChaingun"}),
}),
s("Marinechainsaw", {
t({"Marinechainsaw"}),
}),
s("MarineChainsaw", {
t({"MarineChainsaw"}),
}),
s("Marinefist", {
t({"Marinefist"}),
}),
s("MarineFist", {
t({"MarineFist"}),
}),
s("Marinepistol", {
t({"Marinepistol"}),
}),
s("MarinePistol", {
t({"MarinePistol"}),
}),
s("Marineplasma", {
t({"Marineplasma"}),
}),
s("MarinePlasma", {
t({"MarinePlasma"}),
}),
s("MarineRailgun", {
t({"MarineRailgun"}),
}),
s("Marinerocket", {
t({"Marinerocket"}),
}),
s("MarineRocket", {
t({"MarineRocket"}),
}),
s("Marineshotgun", {
t({"Marineshotgun"}),
}),
s("MarineShotgun", {
t({"MarineShotgun"}),
}),
s("Marinessg", {
t({"Marinessg"}),
}),
s("MarineSSG", {
t({"MarineSSG"}),
}),
s("maxdist", {
t({"maxdist"}),
}),
s("max_pitch", {
t({"max_pitch"}),
}),
s("max_turn", {
t({"max_turn"}),
}),
s("MBFHelperDog", {
t({"MBFHelperDog"}),
}),
s("Meat2", {
t({"Meat2"}),
}),
s("Meat3", {
t({"Meat3"}),
}),
s("Meat4", {
t({"Meat4"}),
}),
s("Meat5", {
t({"Meat5"}),
}),
s("Medikit", {
t({"Medikit"}),
}),
s("Medikit", {
t({"class Medikit : Health "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Amount 25;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTMEDIKIT\";"}),
t({"",""}),t({"\t\tHealth.LowMessage 25, \"$GOTMEDINEED\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tMEDI A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("MedKit_Health", {
t({"class MedKit : HealthPickup "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"  Default"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"    Health 25;"}),
t({"",""}),t({"    Inventory.MaxAmount 15;"}),
t({"",""}),t({"    Inventory.Icon \"I_MDKT\";"}),
t({"",""}),t({"    Inventory.PickupMessage \"You picked up the Medkit.\"; // This is an example. It's recommended to use LANGUAGE for player-facing strings."}),
t({"",""}),t({"    +COUNTITEM"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"  States"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"  Spawn:"}),
t({"",""}),t({"    MEDK A -1;"}),
t({"",""}),t({"    stop;"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
}),
s("Megasphere", {
t({"Megasphere"}),
}),
s("MegasphereHealth", {
t({"class MegasphereHealth : Health\t// for manipulation by Dehacked "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Amount 200;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 200;"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Megasphere", {
t({"class Megasphere : CustomInventory "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\t+INVENTORY.ISHEALTH"}),
t({"",""}),t({"\t\t+INVENTORY.ISARMOR"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTMSPHERE\";"}),
t({"",""}),t({"\t\tInventory.PickupSound \"misc/p_pkup\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tMEGA ABCD 6 BRIGHT;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tPickup:"}),
t({"",""}),t({"\t\tTNT1 A 0 A_GiveInventory(\"BlueArmorForMegasphere\", 1);"}),
t({"",""}),t({"\t\tTNT1 A 0 A_GiveInventory(\"MegasphereHealth\", 1);"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}\t"}),
}),
s("megaspihere", {
t({"ACTOR Megasphere : CustomInventory"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  +COUNTITEM"}),
t({"",""}),t({"  +INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"  Inventory.PickupMessage \"$GOTMSPHERE\" // \"MegaSphere!\""}),
t({"",""}),t({"  Inventory.PickupSound \"misc/p_pkup\""}),
t({"",""}),t({"  States"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"  Spawn:"}),
t({"",""}),t({"    MEGA ABCD 6 Bright"}),
t({"",""}),t({"    Loop"}),
t({"",""}),t({"  Pickup:"}),
t({"",""}),t({"    TNT1 A 0 A_GiveInventory(\"BlueArmorForMegasphere\", 1)"}),
t({"",""}),t({"    TNT1 A 0 A_GiveInventory(\"MegasphereHealth\", 1)"}),
t({"",""}),t({"    Stop"}),
t({"",""}),t({"  }"}),
t({"",""}),t({"}"}),
}),
s("melee", {
t({"melee"}),
}),
s("MeleeAttack", {
t({"A_MeleeAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_MeleeAttack"}),
}),
s("Metal", {
t({"A_Metal "}),i(1),t({"; //https://zdoom.org/wiki/A_Metal"}),
}),
s("minrange", {
t({"minrange"}),
}),
s("missile", {
t({"missile"}),
}),
s("Missile", {
t({"Missile"}),
}),
s("MissileAttack", {
t({"A_MissileAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_MissileAttack"}),
}),
s("missilecheck", {
t({"missilecheck"}),
}),
s("MissilesActivateImpact", {
t({"Level.MissilesActivateImpact"}),
}),
s("modenum", {
t({"modenum"}),
}),
s("modifyactor", {
t({"modifyactor"}),
}),
s("Monster", {
t({"Monster"}),
}),
s("MonsterFallingDamage", {
t({"Level.MonsterFallingDamage"}),
}),
s("MonsterMove", {
t({"MonsterMove"}),
}),
s("MonsterRail", {
t({"A_MonsterRail "}),i(1),t({"; //https://zdoom.org/wiki/A_MonsterRail"}),
}),
s("MonsterRefire", {
t({"A_MonsterRefire "}),i(1),t({"; //https://zdoom.org/wiki/A_MonsterRefire"}),
}),
s("MonstersTeleFrag", {
t({"Level.MonstersTeleFrag"}),
}),
s("Morph", {
t({"A_Morph "}),i(1),t({"; //https://zdoom.org/wiki/A_Morph"}),
}),
s("Movement", {
t({"Movement"}),
}),
s("moving", {
t({"moving"}),
}),
s("Mushroom", {
t({"A_Mushroom "}),i(1),t({"; //https://zdoom.org/wiki/A_Mushroom"}),
}),
s("Music", {
t({"Level.Music"}),
}),
s("MUSIC_ADRIAN", {
t({"MUSIC_ADRIAN"}),
}),
s("MUSIC_AMPIE", {
t({"MUSIC_AMPIE"}),
}),
s("MUSIC_BETWEE", {
t({"MUSIC_BETWEE"}),
}),
s("MUSIC_COUNT2", {
t({"MUSIC_COUNT2"}),
}),
s("MUSIC_COUNTD", {
t({"MUSIC_COUNTD"}),
}),
s("MUSIC_DDTBL2", {
t({"MUSIC_DDTBL2"}),
}),
s("MUSIC_DDTBL3", {
t({"MUSIC_DDTBL3"}),
}),
s("MUSIC_DDTBLU", {
t({"MUSIC_DDTBLU"}),
}),
s("MUSIC_DEAD", {
t({"MUSIC_DEAD"}),
}),
s("MUSIC_DEAD2", {
t({"MUSIC_DEAD2"}),
}),
s("MUSIC_DOOM", {
t({"MUSIC_DOOM"}),
}),
s("MUSIC_DOOM2", {
t({"MUSIC_DOOM2"}),
}),
s("MUSIC_EVIL", {
t({"MUSIC_EVIL"}),
}),
s("MUSIC_IN_CIT", {
t({"MUSIC_IN_CIT"}),
}),
s("MUSIC_MESSAG", {
t({"MUSIC_MESSAG"}),
}),
s("MUSIC_MESSG2", {
t({"MUSIC_MESSG2"}),
}),
s("MUSIC_OPENIN", {
t({"MUSIC_OPENIN"}),
}),
s("MusicOrder", {
t({"Level.MusicOrder"}),
}),
s("MUSIC_READ_M", {
t({"MUSIC_READ_M"}),
}),
s("MUSIC_ROMER2", {
t({"MUSIC_ROMER2"}),
}),
s("MUSIC_ROMERO", {
t({"MUSIC_ROMERO"}),
}),
s("MUSIC_RUNNI2", {
t({"MUSIC_RUNNI2"}),
}),
s("MUSIC_RUNNIN", {
t({"MUSIC_RUNNIN"}),
}),
s("MUSIC_SHAWN", {
t({"MUSIC_SHAWN"}),
}),
s("MUSIC_SHAWN2", {
t({"MUSIC_SHAWN2"}),
}),
s("MUSIC_SHAWN3", {
t({"MUSIC_SHAWN3"}),
}),
s("MUSIC_STALKS", {
t({"MUSIC_STALKS"}),
}),
s("MUSIC_STLKS2", {
t({"MUSIC_STLKS2"}),
}),
s("MUSIC_STLKS3", {
t({"MUSIC_STLKS3"}),
}),
s("MUSIC_TENSE", {
t({"MUSIC_TENSE"}),
}),
s("MUSIC_THE_DA", {
t({"MUSIC_THE_DA"}),
}),
s("MUSIC_THEDA2", {
t({"MUSIC_THEDA2"}),
}),
s("MUSIC_THEDA3", {
t({"MUSIC_THEDA3"}),
}),
s("MUSIC_ULTIMA", {
t({"MUSIC_ULTIMA"}),
}),
s("MusicVolume", {
t({"Level.MusicVolume"}),
}),
s("NewChaseDir", {
t({"NewChaseDir"}),
}),
s("newpos", {
t({"newpos"}),
}),
s("newstate", {
t({"newstate"}),
}),
s("nextMap", {
t({"Level.nextMap"}),
}),
s("NextMap", {
t({"Level.NextMap"}),
}),
s("NextSecretMap", {
t({"Level.NextSecretMap"}),
}),
s("noautoaim", {
t({"noautoaim"}),
}),
s("NoBlocking", {
t({"A_NoBlocking "}),i(1),t({"; //https://zdoom.org/wiki/A_NoBlocking"}),
}),
s("NoDlgFreeze", {
t({"Level.NoDlgFreeze"}),
}),
s("nofreeaim", {
t({"nofreeaim"}),
}),
s("nofunction", {
t({"nofunction"}),
}),
s("NoGravity", {
t({"A_NoGravity "}),i(1),t({"; //https://zdoom.org/wiki/A_NoGravity"}),
}),
s("NoInventoryBar", {
t({"Level.NoInventoryBar"}),
}),
s("NoMonsters", {
t({"Level.NoMonsters"}),
}),
s("NonsolidMeat2", {
t({"NonsolidMeat2"}),
}),
s("NonsolidMeat3", {
t({"NonsolidMeat3"}),
}),
s("NonsolidMeat4", {
t({"NonsolidMeat4"}),
}),
s("NonsolidMeat5", {
t({"NonsolidMeat5"}),
}),
s("NonsolidTwitch", {
t({"NonsolidTwitch"}),
}),
s("Normalize180", {
t({"Normalize180"}),
}),
s("nostop", {
t({"nostop"}),
}),
s("notakeinfinite", {
t({"notakeinfinite"}),
}),
s("Obituary", {
t({"Obituary"}),
}),
s("ObtainInventory", {
t({"ObtainInventory"}),
}),
s("offsetforward", {
t({"offsetforward"}),
}),
s("offsetheight", {
t({"offsetheight"}),
}),
s("offsetside", {
t({"offsetside"}),
}),
s("offsetwidth", {
t({"offsetwidth"}),
}),
s("offsetz", {
t({"offsetz"}),
}),
s("oldradiusdmg", {
t({"oldradiusdmg"}),
}),
s("OldSpawnMissile", {
t({"OldSpawnMissile"}),
}),
s("oldvel", {
t({"oldvel"}),
}),
s("oldz_has_viewheight", {
t({"oldz_has_viewheight"}),
}),
s("on", {
t({"Level.on"}),
}),
s("onlyseekable", {
t({"onlyseekable"}),
}),
s("onsky", {
t({"onsky"}),
}),
s("orresult", {
t({"orresult"}),
}),
s("other", {
t({"other"}),
}),
s("OutsideFogDensity", {
t({"Level.OutsideFogDensity"}),
}),
s("Overlay", {
t({"A_Overlay "}),i(1),t({"; //https://zdoom.org/wiki/A_Overlay"}),
}),
s("OverlayAlpha", {
t({"A_OverlayAlpha "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayAlpha"}),
}),
s("OverlayFlags", {
t({"A_OverlayFlags "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayFlags"}),
}),
s("OverlayOffset", {
t({"A_OverlayOffset "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayOffset"}),
}),
s("OverlayPivot", {
t({"A_OverlayPivot "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayPivot"}),
}),
s("OverlayPivotAlign", {
t({"A_OverlayPivotAlign "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayPivotAlign"}),
}),
s("OverlayRenderstyle", {
t({"A_OverlayRenderstyle "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayRenderstyle"}),
}),
s("OverlayRotate", {
t({"A_OverlayRotate "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayRotate"}),
}),
s("OverlayScale", {
t({"A_OverlayScale "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayScale"}),
}),
s("OverlayTranslation", {
t({"A_OverlayTranslation "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayTranslation"}),
}),
s("OverlayVertexOffset", {
t({"A_OverlayVertexOffset "}),i(1),t({"; //https://zdoom.org/wiki/A_OverlayVertexOffset"}),
}),
s("owner", {
t({"owner"}),
}),
s("Pain", {
t({"A_Pain "}),i(1),t({"; //https://zdoom.org/wiki/A_Pain"}),
}),
s("PainAttack", {
t({"A_PainAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_PainAttack"}),
}),
s("PainChance", {
t({"PainChance"}),
}),
s("PainDie", {
t({"A_PainDie "}),i(1),t({"; //https://zdoom.org/wiki/A_PainDie"}),
}),
s("PainElemental", {
t({"PainElemental"}),
}),
s("PainElemental", {
t({"class PainElemental : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 400;"}),
t({"",""}),t({"\t\tRadius 31;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 400;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 128;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOAT "}),
t({"",""}),t({"\t\t+NOGRAVITY"}),
t({"",""}),t({"\t\tSeeSound \"pain/sight\";"}),
t({"",""}),t({"\t\tPainSound \"pain/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"pain/death\";"}),
t({"",""}),t({"\t\tActiveSound \"pain/active\";"}),
t({"",""}),t({"\t\tTag \"$FN_PAIN\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPAIN A 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tPAIN AABBCC 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tPAIN D 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tPAIN E 5 A_FaceTarget;"}),
t({"",""}),t({"\t\tPAIN F 5 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tPAIN F 0 BRIGHT A_PainAttack;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tPAIN G 6;"}),
t({"",""}),t({"\t\tPAIN G 6 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tPAIN H 8 BRIGHT;"}),
t({"",""}),t({"\t\tPAIN I 8 BRIGHT A_Scream;"}),
t({"",""}),t({"\t\tPAIN JK 8 BRIGHT;"}),
t({"",""}),t({"\t\tPAIN L 8 BRIGHT A_PainDie;"}),
t({"",""}),t({"\t\tPAIN M 8 BRIGHT;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tPAIN MLKJIH 8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("PainSound", {
t({"PainSound"}),
}),
s("params", {
t({"params"}),
}),
s("particledir", {
t({"particledir"}),
}),
s("ParTime", {
t({"Level.ParTime"}),
}),
s("passive", {
t({"passive"}),
}),
s("period", {
t({"period"}),
}),
s("PickDeathmatchStart", {
t({"Level.PickDeathmatchStart"}),
}),
s("Pistol", {
t({"Pistol"}),
}),
s("Pistol", {
t({"class Pistol : DoomWeapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 1900;"}),
t({"",""}),t({"\t\tWeapon.AmmoUse 1;"}),
t({"",""}),t({"\t\tWeapon.AmmoGive 20;"}),
t({"",""}),t({"\t\tWeapon.AmmoType \"Clip\";"}),
t({"",""}),t({"\t\tObituary \"$OB_MPPISTOL\";"}),
t({"",""}),t({"\t\t+WEAPON.WIMPY_WEAPON"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$PICKUP_PISTOL_DROPPED\";"}),
t({"",""}),t({"\t\tTag \"$TAG_PISTOL\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tPISG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tPISG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tPISG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tPISG A 4;"}),
t({"",""}),t({"\t\tPISG B 6 A_FirePistol;"}),
t({"",""}),t({"\t\tPISG C 4;"}),
t({"",""}),t({"\t\tPISG B 5 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tFlash:"}),
t({"",""}),t({"\t\tPISF A 7 Bright A_Light1;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\t\tPISF A 7 Bright A_Light1;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({" \tSpawn:"}),
t({"",""}),t({"\t\tPIST A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({"\t\t"}),
}),
s("pitch", {
t({"pitch"}),
}),
s("pitch_offset", {
t({"pitch_offset"}),
}),
s("PitchTo", {
t({"PitchTo"}),
}),
s("PixelStretch", {
t({"Level.PixelStretch"}),
}),
s("PlasmaBall", {
t({"PlasmaBall"}),
}),
s("PlasmaBall1", {
t({"PlasmaBall1"}),
}),
s("PlasmaBall1", {
t({"class PlasmaBall1 : PlasmaBall "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tDamage 4;"}),
t({"",""}),t({"\t\tBounceType \"Classic\";"}),
t({"",""}),t({"\t\tBounceFactor 1.0;"}),
t({"",""}),t({"\t\tObituary \"$OB_MPBFG_MBF\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLS1 AB 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tPLS1 CDEFG 4 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({"\t"}),
}),
s("PlasmaBall2", {
t({"PlasmaBall2"}),
}),
s("PlasmaBall2", {
t({"class PlasmaBall2 : PlasmaBall1 "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLS2 AB 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tPLS2 CDE 4 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("PlasmaBall", {
t({"class PlasmaBall : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 13;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 25;"}),
t({"",""}),t({"\t\tDamage 5;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t\tAlpha 0.75;"}),
t({"",""}),t({"\t\tSeeSound \"weapons/plasmaf\";"}),
t({"",""}),t({"\t\tDeathSound \"weapons/plasmax\";"}),
t({"",""}),t({"\t\tObituary \"$OB_MPPLASMARIFLE\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLSS AB 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tPLSE ABCDE 4 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("PlasmaRifle", {
t({"PlasmaRifle"}),
}),
s("PlasmaRifle", {
t({"class PlasmaRifle : DoomWeapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 100;"}),
t({"",""}),t({"\t\tWeapon.AmmoUse 1;"}),
t({"",""}),t({"\t\tWeapon.AmmoGive 40;"}),
t({"",""}),t({"\t\tWeapon.AmmoType \"Cell\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTPLASMA\";"}),
t({"",""}),t({"\t\tTag \"$TAG_PLASMARIFLE\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tPLSG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tPLSG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tPLSG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tPLSG A 3 A_FirePlasma;"}),
t({"",""}),t({"\t\tPLSG B 20 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tFlash:"}),
t({"",""}),t({"\t\tPLSF A 4 Bright A_Light1;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\t\tPLSF B 4 Bright A_Light1;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPLAS A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("PlayActiveSound", {
t({"PlayActiveSound"}),
}),
s("PlayBounceSound", {
t({"PlayBounceSound"}),
}),
s("Player", {
t({"Player"}),
}),
s("PlayerClasses", {
t({"PlayerClasses = \""}),i(1),t({"\""}),
}),
s("PlayerEvent", {
t({"PlayerEvent"}),
}),
s("PlayerNumber", {
t({"PlayerNumber"}),
}),
s("PlayerPawn", {
t({"PlayerPawn"}),
}),
s("PlayerPawn", {
t({"class "}),i(1),t({" : DoomPlayer"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(2),t({""}),
t({"",""}),t({"}"}),
}),
s("PlayerScream", {
t({"A_PlayerScream "}),i(1),t({"; //https://zdoom.org/wiki/A_PlayerScream"}),
}),
s("PlayerSkinCheck", {
t({"PlayerSkinCheck"}),
}),
s("PlayerSpawned", {
t({"PlayerSpawned"}),
}),
s("PlayerSpawned", {
t({"override void PlayerSpawned(PlayerEvent e)"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  PlayerPawn pmo = players[e.PlayerNumber].mo;"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("player_spawn", {
t({"  override void PlayerSpawned(PlayerEvent e)"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"    PlayerPawn pmo = players[e.PlayerNumber].mo;"}),
t({"",""}),t({"    if (pmo)"}),
t({"",""}),t({"    {"}),
t({"",""}),t({"      "}),i(1),t({""}),
t({"",""}),t({"    }"}),
t({"",""}),t({"  }"}),
}),
s("Player_StartItem", {
t({"Player.StartItem \""}),i(1),t({"\";"}),
}),
s("Player_WeaponSlot", {
t({"Player.WeaponSlot "}),i(1),t({", \""}),i(2),t({"\";"}),
}),
s("PlayPushSound", {
t({"PlayPushSound"}),
}),
s("playsound", {
t({"playsound"}),
}),
s("playSound", {
t({"Level.playSound"}),
}),
s("PlaySound", {
t({"A_PlaySound "}),i(1),t({"; //https://zdoom.org/wiki/A_PlaySound"}),
}),
s("PlaySoundEx", {
t({"A_PlaySoundEx "}),i(1),t({"; //https://zdoom.org/wiki/A_PlaySoundEx"}),
}),
s("PlaySpawnSound", {
t({"PlaySpawnSound"}),
}),
s("PlayWeaponSound", {
t({"A_PlayWeaponSound "}),i(1),t({"; //https://zdoom.org/wiki/A_PlayWeaponSound"}),
}),
s("pLineTarget", {
t({"pLineTarget"}),
}),
s("pNum", {
t({"Level.pNum"}),
}),
s("PoisonMobj", {
t({"PoisonMobj"}),
}),
s("PolyGrind", {
t({"Level.PolyGrind"}),
}),
s("PosAttack", {
t({"A_PosAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_PosAttack"}),
}),
s("position", {
t({"Level.position"}),
}),
s("PosRelative", {
t({"PosRelative"}),
}),
s("POSS", {
t({"POSS"}),
}),
s("PQRST", {
t({"PQRST"}),
}),
s("Print", {
t({"A_Print "}),i(1),t({"; //https://zdoom.org/wiki/A_Print"}),
}),
s("PrintBold", {
t({"A_PrintBold "}),i(1),t({"; //https://zdoom.org/wiki/A_PrintBold"}),
}),
s("printMid", {
t({"console.MidPrint(null, \"\\c["}),i(1),t({"] "}),i(2),t({"\", false);"}),
}),
s("print_msg", {
t({"GTIK A 0 A_Print(\""}),i(1),t({"\");"}),
}),
s("printmsg", {
t({"printmsg"}),
}),
s("protected", {
t({"protected"}),
}),
s("ptr_select", {
t({"ptr_select"}),
}),
s("ptr_select1", {
t({"ptr_select1"}),
}),
s("ptr_target", {
t({"ptr_target"}),
}),
s("pufftype", {
t({"pufftype"}),
}),
s("Punch", {
t({"A_Punch "}),i(1),t({"; //https://zdoom.org/wiki/A_Punch"}),
}),
s("Quake", {
t({"A_Quake "}),i(1),t({"; //https://zdoom.org/wiki/A_Quake"}),
}),
s("QuakeEx", {
t({"A_QuakeEx "}),i(1),t({"; //https://zdoom.org/wiki/A_QuakeEx"}),
}),
s("QueueCorpse", {
t({"A_QueueCorpse "}),i(1),t({"; //https://zdoom.org/wiki/A_QueueCorpse"}),
}),
s("quick", {
t({"quick"}),
}),
s("quiet", {
t({"quiet"}),
}),
s("Radius", {
t({"Radius"}),
}),
s("RadiusDamageSelf", {
t({"A_RadiusDamageSelf "}),i(1),t({"; //https://zdoom.org/wiki/A_RadiusDamageSelf"}),
}),
s("RadiusGive", {
t({"A_RadiusGive "}),i(1),t({"; //https://zdoom.org/wiki/A_RadiusGive"}),
}),
s("radiusoffset", {
t({"radiusoffset"}),
}),
s("RadiusThrust", {
t({"A_RadiusThrust "}),i(1),t({"; //https://zdoom.org/wiki/A_RadiusThrust"}),
}),
s("RadSuit", {
t({"RadSuit"}),
}),
s("RadSuit", {
t({"class RadSuit : PowerupGiver "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHeight 46;"}),
t({"",""}),t({"\t\t+INVENTORY.AUTOACTIVATE"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP"}),
t({"",""}),t({"\t\tInventory.MaxAmount 0;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSUIT\";"}),
t({"",""}),t({"\t\tPowerup.Type \"PowerIronFeet\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSUIT A -1 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("RailAttack", {
t({"RailAttack"}),
}),
s("Raise", {
t({"A_Raise "}),i(1),t({"; //https://zdoom.org/wiki/A_Raise"}),
}),
s("RaiseActor", {
t({"RaiseActor"}),
}),
s("RaiseChildren", {
t({"A_RaiseChildren "}),i(1),t({"; //https://zdoom.org/wiki/A_RaiseChildren"}),
}),
s("RaiseMaster", {
t({"A_RaiseMaster "}),i(1),t({"; //https://zdoom.org/wiki/A_RaiseMaster"}),
}),
s("RaiseMobj", {
t({"RaiseMobj"}),
}),
s("RaiseSelf", {
t({"A_RaiseSelf "}),i(1),t({"; //https://zdoom.org/wiki/A_RaiseSelf"}),
}),
s("RaiseSiblings", {
t({"A_RaiseSiblings "}),i(1),t({"; //https://zdoom.org/wiki/A_RaiseSiblings"}),
}),
s("RandomChaseDir", {
t({"RandomChaseDir"}),
}),
s("range", {
t({"range"}),
}),
s("readonly<Actor>", {
t({"readonly<Actor>"}),
}),
s("RearrangePointers", {
t({"A_RearrangePointers "}),i(1),t({"; //https://zdoom.org/wiki/A_RearrangePointers"}),
}),
s("receiver", {
t({"receiver"}),
}),
s("Recoil", {
t({"A_Recoil "}),i(1),t({"; //https://zdoom.org/wiki/A_Recoil"}),
}),
s("RedCard", {
t({"RedCard"}),
}),
s("RedCard", {
t({"class RedCard : DoomKey "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTREDCARD\";"}),
t({"",""}),t({"\t\tInventory.Icon \"STKEYS2\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tRKEY A 10;"}),
t({"",""}),t({"\t\tRKEY B 10 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("RedSkull", {
t({"RedSkull"}),
}),
s("RedSkull", {
t({"class RedSkull : DoomKey "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTREDSKUL\";"}),
t({"",""}),t({"\t\tInventory.Icon \"STKEYS5\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tRSKU A 10;"}),
t({"",""}),t({"\t\tRSKU B 10 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("RedTorch", {
t({"RedTorch"}),
}),
s("ReFire", {
t({"A_ReFire "}),i(1),t({"; //https://zdoom.org/wiki/A_ReFire"}),
}),
s("ReflectOffActor", {
t({"ReflectOffActor"}),
}),
s("remote", {
t({"remote"}),
}),
s("Remove", {
t({"A_Remove "}),i(1),t({"; //https://zdoom.org/wiki/A_Remove"}),
}),
s("RemoveChildren", {
t({"A_RemoveChildren "}),i(1),t({"; //https://zdoom.org/wiki/A_RemoveChildren"}),
}),
s("RemoveFromHash", {
t({"RemoveFromHash"}),
}),
s("RemoveItems", {
t({"Level.RemoveItems"}),
}),
s("RemoveLight", {
t({"A_RemoveLight "}),i(1),t({"; //https://zdoom.org/wiki/A_RemoveLight"}),
}),
s("RemoveMaster", {
t({"A_RemoveMaster "}),i(1),t({"; //https://zdoom.org/wiki/A_RemoveMaster"}),
}),
s("RemoveSiblings", {
t({"A_RemoveSiblings "}),i(1),t({"; //https://zdoom.org/wiki/A_RemoveSiblings"}),
}),
s("RemoveTarget", {
t({"A_RemoveTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_RemoveTarget"}),
}),
s("RemoveTracer", {
t({"A_RemoveTracer "}),i(1),t({"; //https://zdoom.org/wiki/A_RemoveTracer"}),
}),
s("Replaces", {
t({"Replaces"}),
}),
s("Replaces", {
t({"class "}),i(1),t({" : Actor Replaces "}),i(2),t({" "}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(3),t({""}),
t({"",""}),t({"}"}),
}),
s("resetHealth", {
t({"resetHealth"}),
}),
s("ResetHealth", {
t({"A_ResetHealth "}),i(1),t({"; //https://zdoom.org/wiki/A_ResetHealth"}),
}),
s("ResetModelFlags", {
t({"ResetModelFlags"}),
}),
s("ResetReloadCounter", {
t({"A_ResetReloadCounter "}),i(1),t({"; //https://zdoom.org/wiki/A_ResetReloadCounter"}),
}),
s("ResolveState", {
t({"ResolveState"}),
}),
s("Respawn", {
t({"A_Respawn "}),i(1),t({"; //https://zdoom.org/wiki/A_Respawn"}),
}),
s("RestoreDamage", {
t({"RestoreDamage"}),
}),
s("RestoreRenderStyle", {
t({"RestoreRenderStyle"}),
}),
s("return", {
t({"return"}),
}),
s("Revenant", {
t({"Revenant"}),
}),
s("RevenantTracer", {
t({"RevenantTracer"}),
}),
s("RevenantTracerSmoke", {
t({"RevenantTracerSmoke"}),
}),
s("RevenantTracerSmoke", {
t({"class RevenantTracerSmoke : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+NOBLOCKMAP"}),
t({"",""}),t({"\t\t+NOGRAVITY"}),
t({"",""}),t({"\t\t+NOTELEPORT"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Translucent\";"}),
t({"",""}),t({"\t\tAlpha 0.5;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{\t"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPUFF ABABC 4;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("RevenantTracer", {
t({"class RevenantTracer : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 11;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 10;"}),
t({"",""}),t({"\t\tDamage 10;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+SEEKERMISSILE "}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tSeeSound \"skeleton/attack\";"}),
t({"",""}),t({"\t\tDeathSound \"skeleton/tracex\";"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tFATB AB 2 BRIGHT A_Tracer;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tFBXP A 8 BRIGHT;"}),
t({"",""}),t({"\t\tFBXP B 6 BRIGHT;"}),
t({"",""}),t({"\t\tFBXP C 4 BRIGHT;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Revenant", {
t({"class Revenant : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 300;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 500;"}),
t({"",""}),t({"\t\tSpeed 10;"}),
t({"",""}),t({"\t\tPainChance 100;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\tMeleeThreshold 196;"}),
t({"",""}),t({"\t\t+MISSILEMORE "}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"skeleton/sight\";"}),
t({"",""}),t({"\t\tPainSound \"skeleton/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"skeleton/death\";"}),
t({"",""}),t({"\t\tActiveSound \"skeleton/active\";"}),
t({"",""}),t({"\t\tMeleeSound \"skeleton/melee\";"}),
t({"",""}),t({"\t\tHitObituary \"$OB_UNDEADHIT\";"}),
t({"",""}),t({"\t\tObituary \"$OB_UNDEAD\";"}),
t({"",""}),t({"\t\tTag \"$FN_REVEN\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSKEL AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSKEL AABBCCDDEEFF 2 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMelee:"}),
t({"",""}),t({"\t\tSKEL G 0 A_FaceTarget;"}),
t({"",""}),t({"\t\tSKEL G 6 A_SkelWhoosh;"}),
t({"",""}),t({"\t\tSKEL H 6 A_FaceTarget;"}),
t({"",""}),t({"\t\tSKEL I 6 A_SkelFist;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSKEL J 0 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tSKEL J 10 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tSKEL K 10 A_SkelMissile;"}),
t({"",""}),t({"\t\tSKEL K 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSKEL L 5;"}),
t({"",""}),t({"\t\tSKEL L 5 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSKEL LM 7;"}),
t({"",""}),t({"\t\tSKEL N 7 A_Scream;"}),
t({"",""}),t({"\t\tSKEL O 7 A_NoBlocking;"}),
t({"",""}),t({"\t\tSKEL P 7;"}),
t({"",""}),t({"\t\tSKEL Q -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tSKEL Q 5;"}),
t({"",""}),t({"\t\tSKEL PONML 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("revert", {
t({"revert"}),
}),
s("Revive", {
t({"Revive"}),
}),
s("Rocket", {
t({"Rocket"}),
}),
s("RocketAmmo", {
t({"RocketAmmo"}),
}),
s("RocketAmmo", {
t({" "}),i(1),t({""}),
t({"",""}),t({"class RocketAmmo : Ammo"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTROCKET\";"}),
t({"",""}),t({"\t\tInventory.Amount 1;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 50;"}),
t({"",""}),t({"\t\tAmmo.BackpackAmount 1;"}),
t({"",""}),t({"\t\tAmmo.BackpackMaxAmount 100;"}),
t({"",""}),t({"\t\tInventory.Icon \"ROCKA0\";"}),
t({"",""}),t({"\t\tTag \"$AMMO_ROCKETS\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tROCK A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("RocketBox", {
t({"RocketBox"}),
}),
s("RocketBox", {
t({"class RocketBox : RocketAmmo "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTROCKBOX\";"}),
t({"",""}),t({"\t\tInventory.Amount 5;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBROK A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("RocketLauncher", {
t({"RocketLauncher"}),
}),
s("RocketLauncher", {
t({"class RocketLauncher : DoomWeapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 2500;"}),
t({"",""}),t({"\t\tWeapon.AmmoUse 1;"}),
t({"",""}),t({"\t\tWeapon.AmmoGive 2;"}),
t({"",""}),t({"\t\tWeapon.AmmoType \"RocketAmmo\";"}),
t({"",""}),t({"\t\t+WEAPON.NOAUTOFIRE"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTLAUNCHER\";"}),
t({"",""}),t({"\t\tTag \"$TAG_ROCKETLAUNCHER\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tMISG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tMISG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tMISG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tMISG B 8 A_GunFlash;"}),
t({"",""}),t({"\t\tMISG B 12 A_FireMissile;"}),
t({"",""}),t({"\t\tMISG B 0 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tFlash:"}),
t({"",""}),t({"\t\tMISF A 3 Bright A_Light1;"}),
t({"",""}),t({"\t\tMISF B 4 Bright;"}),
t({"",""}),t({"\t\tMISF CD 4 Bright A_Light2;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tLAUN A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Rocket", {
t({"class Rocket : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 11;"}),
t({"",""}),t({"\t\tHeight 8;"}),
t({"",""}),t({"\t\tSpeed 20;"}),
t({"",""}),t({"\t\tDamage 20;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\t+DEHEXPLOSION"}),
t({"",""}),t({"\t\t+ROCKETTRAIL"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tSeeSound \"weapons/rocklf\";"}),
t({"",""}),t({"\t\tDeathSound \"weapons/rocklx\";"}),
t({"",""}),t({"\t\tObituary \"$OB_MPROCKET\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tMISL A 1 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tMISL B 8 Bright A_Explode;"}),
t({"",""}),t({"\t\tMISL C 6 Bright;"}),
t({"",""}),t({"\t\tMISL D 4 Bright;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tBrainExplode:"}),
t({"",""}),t({"\t\tMISL BC 10 Bright;"}),
t({"",""}),t({"\t\tMISL D 10 A_BrainExplode;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("RotateVector", {
t({"RotateVector"}),
}),
s("RoughMonsterSearch", {
t({"RoughMonsterSearch"}),
}),
s("round", {
t({"round"}),
}),
s("SargAttack", {
t({"A_SargAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_SargAttack"}),
}),
s("Saw", {
t({"A_Saw "}),i(1),t({"; //https://zdoom.org/wiki/A_Saw"}),
}),
s("ScaleVelocity", {
t({"A_ScaleVelocity "}),i(1),t({"; //https://zdoom.org/wiki/A_ScaleVelocity"}),
}),
s("Scream", {
t({"A_Scream "}),i(1),t({"; //https://zdoom.org/wiki/A_Scream"}),
}),
s("ScreamAndUnblock", {
t({"A_ScreamAndUnblock "}),i(1),t({"; //https://zdoom.org/wiki/A_ScreamAndUnblock"}),
}),
s("ScriptedMarine", {
t({"ScriptedMarine"}),
}),
s("sector", {
t({"sector"}),
}),
s("sectors", {
t({"Level.sectors["}),i(1),t({"]"}),
}),
s("sector_size", {
t({"Level.sectors.size();"}),
}),
s("See", {
t({"See"}),
}),
s("SeekerMissile", {
t({"A_SeekerMissile "}),i(1),t({"; //https://zdoom.org/wiki/A_SeekerMissile"}),
}),
s("SeeSound", {
t({"SeeSound"}),
}),
s("SelectWeapon", {
t({"A_SelectWeapon "}),i(1),t({"; //https://zdoom.org/wiki/A_SelectWeapon"}),
}),
s("SentinelBob", {
t({"A_SentinelBob "}),i(1),t({"; //https://zdoom.org/wiki/A_SentinelBob"}),
}),
s("SentinelRefire", {
t({"A_SentinelRefire "}),i(1),t({"; //https://zdoom.org/wiki/A_SentinelRefire"}),
}),
s("seqname", {
t({"seqname"}),
}),
s("SeqNode", {
t({"SeqNode"}),
}),
s("sequence", {
t({"sequence"}),
}),
s("SetAmmoCapacity", {
t({"SetAmmoCapacity"}),
}),
s("SetAngle", {
t({"A_SetAngle "}),i(1),t({"; //https://zdoom.org/wiki/A_SetAngle"}),
}),
s("SetArg", {
t({"A_SetArg "}),i(1),t({"; //https://zdoom.org/wiki/A_SetArg"}),
}),
s("SetBlend", {
t({"A_SetBlend "}),i(1),t({"; //https://zdoom.org/wiki/A_SetBlend"}),
}),
s("SetCamera", {
t({"SetCamera"}),
}),
s("SetChaseThreshold", {
t({"A_SetChaseThreshold "}),i(1),t({"; //https://zdoom.org/wiki/A_SetChaseThreshold"}),
}),
s("SetCrosshair", {
t({"A_SetCrosshair "}),i(1),t({"; //https://zdoom.org/wiki/A_SetCrosshair"}),
}),
s("SetDamage", {
t({"SetDamage"}),
}),
s("SetDamageType", {
t({"A_SetDamageType "}),i(1),t({"; //https://zdoom.org/wiki/A_SetDamageType"}),
}),
s("SetFloat", {
t({"A_SetFloat "}),i(1),t({"; //https://zdoom.org/wiki/A_SetFloat"}),
}),
s("SetFloatBobPhase", {
t({"A_SetFloatBobPhase "}),i(1),t({"; //https://zdoom.org/wiki/A_SetFloatBobPhase"}),
}),
s("SetFloatSpeed", {
t({"A_SetFloatSpeed "}),i(1),t({"; //https://zdoom.org/wiki/A_SetFloatSpeed"}),
}),
s("SetFloorClip", {
t({"A_SetFloorClip "}),i(1),t({"; //https://zdoom.org/wiki/A_SetFloorClip"}),
}),
s("SetFriendly", {
t({"A_SetFriendly "}),i(1),t({"; //https://zdoom.org/wiki/A_SetFriendly"}),
}),
s("SetFriendPlayer", {
t({"SetFriendPlayer"}),
}),
s("SetGravity", {
t({"A_SetGravity "}),i(1),t({"; //https://zdoom.org/wiki/A_SetGravity"}),
}),
s("SetHealth", {
t({"A_SetHealth "}),i(1),t({"; //https://zdoom.org/wiki/A_SetHealth"}),
}),
s("SetIdle", {
t({"SetIdle"}),
}),
s("SetInventory", {
t({"SetInventory"}),
}),
s("SetInvulnerable", {
t({"A_SetInvulnerable "}),i(1),t({"; //https://zdoom.org/wiki/A_SetInvulnerable"}),
}),
s("SetMass", {
t({"A_SetMass "}),i(1),t({"; //https://zdoom.org/wiki/A_SetMass"}),
}),
s("SetModelFlag", {
t({"SetModelFlag"}),
}),
s("SetMugshotState", {
t({"A_SetMugshotState "}),i(1),t({"; //https://zdoom.org/wiki/A_SetMugshotState"}),
}),
s("SetOrigin", {
t({"SetOrigin"}),
}),
s("SetPainThreshold", {
t({"A_SetPainThreshold "}),i(1),t({"; //https://zdoom.org/wiki/A_SetPainThreshold"}),
}),
s("SetPitch", {
t({"A_SetPitch "}),i(1),t({"; //https://zdoom.org/wiki/A_SetPitch"}),
}),
s("setreceiver", {
t({"setreceiver"}),
}),
s("SetReflective", {
t({"A_SetReflective "}),i(1),t({"; //https://zdoom.org/wiki/A_SetReflective"}),
}),
s("SetReflectiveInvulnerable", {
t({"A_SetReflectiveInvulnerable "}),i(1),t({"; //https://zdoom.org/wiki/A_SetReflectiveInvulnerable"}),
}),
s("SetRenderStyle", {
t({"A_SetRenderStyle "}),i(1),t({"; //https://zdoom.org/wiki/A_SetRenderStyle"}),
}),
s("SetRipMax", {
t({"A_SetRipMax "}),i(1),t({"; //https://zdoom.org/wiki/A_SetRipMax"}),
}),
s("SetRipMin", {
t({"A_SetRipMin "}),i(1),t({"; //https://zdoom.org/wiki/A_SetRipMin"}),
}),
s("SetRipperLevel", {
t({"A_SetRipperLevel "}),i(1),t({"; //https://zdoom.org/wiki/A_SetRipperLevel"}),
}),
s("SetRoll", {
t({"A_SetRoll "}),i(1),t({"; //https://zdoom.org/wiki/A_SetRoll"}),
}),
s("SetScale", {
t({"A_SetScale "}),i(1),t({"; //https://zdoom.org/wiki/A_SetScale"}),
}),
s("set_sector_light", {
t({"SetSectorLight("}),i(1),t({", "}),i(2),t({"); // SetSectorLight(<sector id>,<value 0-256>)"}),
}),
s("SetShade", {
t({"SetShade"}),
}),
s("SetShadow", {
t({"A_SetShadow "}),i(1),t({"; //https://zdoom.org/wiki/A_SetShadow"}),
}),
s("SetShootable", {
t({"A_SetShootable "}),i(1),t({"; //https://zdoom.org/wiki/A_SetShootable"}),
}),
s("SetSize", {
t({"A_SetSize "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSize"}),
}),
s("SetSolid", {
t({"A_SetSolid "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSolid"}),
}),
s("SetSpecial", {
t({"A_SetSpecial "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSpecial"}),
}),
s("SetSpecies", {
t({"A_SetSpecies "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSpecies"}),
}),
s("SetSpeed", {
t({"A_SetSpeed "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSpeed"}),
}),
s("SetSpriteAngle", {
t({"A_SetSpriteAngle "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSpriteAngle"}),
}),
s("SetSpriteRotation", {
t({"A_SetSpriteRotation "}),i(1),t({"; //https://zdoom.org/wiki/A_SetSpriteRotation"}),
}),
s("SetState", {
t({"SetState"}),
}),
s("SetStateLabel", {
t({"SetStateLabel"}),
}),
s("SetTag", {
t({"SetTag"}),
}),
s("SetTeleFog", {
t({"A_SetTeleFog "}),i(1),t({"; //https://zdoom.org/wiki/A_SetTeleFog"}),
}),
s("SetTics", {
t({"A_SetTics "}),i(1),t({"; //https://zdoom.org/wiki/A_SetTics"}),
}),
s("SetTranslation", {
t({"A_SetTranslation "}),i(1),t({"; //https://zdoom.org/wiki/A_SetTranslation"}),
}),
s("SetTranslucent", {
t({"A_SetTranslucent "}),i(1),t({"; //https://zdoom.org/wiki/A_SetTranslucent"}),
}),
s("SetUserArray", {
t({"A_SetUserArray "}),i(1),t({"; //https://zdoom.org/wiki/A_SetUserArray"}),
}),
s("SetUserArrayFloat", {
t({"A_SetUserArrayFloat "}),i(1),t({"; //https://zdoom.org/wiki/A_SetUserArrayFloat"}),
}),
s("SetUserVar", {
t({"A_SetUserVar "}),i(1),t({"; //https://zdoom.org/wiki/A_SetUserVar"}),
}),
s("SetUserVarFloat", {
t({"A_SetUserVarFloat "}),i(1),t({"; //https://zdoom.org/wiki/A_SetUserVarFloat"}),
}),
s("SetViewAngle", {
t({"A_SetViewAngle "}),i(1),t({"; //https://zdoom.org/wiki/A_SetViewAngle"}),
}),
s("SetViewPitch", {
t({"A_SetViewPitch "}),i(1),t({"; //https://zdoom.org/wiki/A_SetViewPitch"}),
}),
s("SetViewRoll", {
t({"A_SetViewRoll "}),i(1),t({"; //https://zdoom.org/wiki/A_SetViewRoll"}),
}),
s("SetVisibleRotation", {
t({"A_SetVisibleRotation "}),i(1),t({"; //https://zdoom.org/wiki/A_SetVisibleRotation"}),
}),
s("SetXYZ", {
t({"SetXYZ"}),
}),
s("Shell", {
t({"Shell"}),
}),
s("ShellBox", {
t({"ShellBox"}),
}),
s("ShellBox", {
t({"class ShellBox : Shell "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSHELLBOX\";"}),
t({"",""}),t({"\t\tInventory.Amount 20;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSBOX A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Shell", {
t({"class Shell : Ammo "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSHELLS\";"}),
t({"",""}),t({"\t\tInventory.Amount 4;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 50;"}),
t({"",""}),t({"\t\tAmmo.BackpackAmount 4;"}),
t({"",""}),t({"\t\tAmmo.BackpackMaxAmount 100;"}),
t({"",""}),t({"\t\tInventory.Icon \"SHELA0\";"}),
t({"",""}),t({"\t\tTag \"$AMMO_SHELLS\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSHEL A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("ShortBlueTorch", {
t({"ShortBlueTorch"}),
}),
s("ShortGreenColumn", {
t({"ShortGreenColumn"}),
}),
s("ShortGreenTorch", {
t({"ShortGreenTorch"}),
}),
s("ShortRedColumn", {
t({"ShortRedColumn"}),
}),
s("ShortRedTorch", {
t({"ShortRedTorch"}),
}),
s("Shotgun", {
t({"Shotgun"}),
}),
s("ShotgunGuy", {
t({"ShotgunGuy"}),
}),
s("shotgunguy", {
t({"class "}),i(1),t({" : Actor Replaces ShotgunGuy "}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 30;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 170;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"shotguy/sight\";"}),
t({"",""}),t({"\t\tAttackSound \"shotguy/attack\";"}),
t({"",""}),t({"\t\tPainSound \"shotguy/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"shotguy/death\";"}),
t({"",""}),t({"\t\tActiveSound \"shotguy/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_SHOTGUY\";"}),
t({"",""}),t({"\t\tTag \"$FN_SHOTGUN\";"}),
t({"",""}),t({"\t\tDropItem \"Shotgun\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSPOS AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSPOS AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSPOS E 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tSPOS F 10 BRIGHT A_SposAttackUseAtkSound;"}),
t({"",""}),t({"\t\tSPOS E 10;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSPOS G 3;"}),
t({"",""}),t({"\t\tSPOS G 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSPOS H 5;"}),
t({"",""}),t({"\t\tSPOS I 5 A_Scream;"}),
t({"",""}),t({"\t\tSPOS J 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tSPOS K 5;"}),
t({"",""}),t({"\t\tSPOS L -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tSPOS M 5;"}),
t({"",""}),t({"\t\tSPOS N 5 A_XScream;"}),
t({"",""}),t({"\t\tSPOS O 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tSPOS PQRST 5;"}),
t({"",""}),t({"\t\tSPOS U -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tSPOS L 5;"}),
t({"",""}),t({"\t\tSPOS KJIH 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("ShotgunGuy", {
t({"class ShotgunGuy : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 30;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tMass 100;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 170;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"shotguy/sight\";"}),
t({"",""}),t({"\t\tAttackSound \"shotguy/attack\";"}),
t({"",""}),t({"\t\tPainSound \"shotguy/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"shotguy/death\";"}),
t({"",""}),t({"\t\tActiveSound \"shotguy/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_SHOTGUY\";"}),
t({"",""}),t({"\t\tTag \"$FN_SHOTGUN\";"}),
t({"",""}),t({"\t\tDropItem \"Shotgun\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSPOS AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSPOS AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSPOS E 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tSPOS F 10 BRIGHT A_SposAttackUseAtkSound;"}),
t({"",""}),t({"\t\tSPOS E 10;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSPOS G 3;"}),
t({"",""}),t({"\t\tSPOS G 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSPOS H 5;"}),
t({"",""}),t({"\t\tSPOS I 5 A_Scream;"}),
t({"",""}),t({"\t\tSPOS J 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tSPOS K 5;"}),
t({"",""}),t({"\t\tSPOS L -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tSPOS M 5;"}),
t({"",""}),t({"\t\tSPOS N 5 A_XScream;"}),
t({"",""}),t({"\t\tSPOS O 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tSPOS PQRST 5;"}),
t({"",""}),t({"\t\tSPOS U -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tSPOS L 5;"}),
t({"",""}),t({"\t\tSPOS KJIH 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Shotgun", {
t({"class Shotgun : DoomWeapon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tWeapon.SelectionOrder 1300;"}),
t({"",""}),t({"\t\tWeapon.AmmoUse 1;"}),
t({"",""}),t({"\t\tWeapon.AmmoGive 8;"}),
t({"",""}),t({"\t\tWeapon.AmmoType \"Shell\";"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSHOTGUN\";"}),
t({"",""}),t({"\t\tObituary \"$OB_MPSHOTGUN\";"}),
t({"",""}),t({"\t\tTag \"$TAG_SHOTGUN\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tReady:"}),
t({"",""}),t({"\t\tSHTG A 1 A_WeaponReady;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tDeselect:"}),
t({"",""}),t({"\t\tSHTG A 1 A_Lower;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSelect:"}),
t({"",""}),t({"\t\tSHTG A 1 A_Raise;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tFire:"}),
t({"",""}),t({"\t\tSHTG A 3;"}),
t({"",""}),t({"\t\tSHTG A 7 A_FireShotgun;"}),
t({"",""}),t({"\t\tSHTG BC 5;"}),
t({"",""}),t({"\t\tSHTG D 4;"}),
t({"",""}),t({"\t\tSHTG CB 5;"}),
t({"",""}),t({"\t\tSHTG A 3;"}),
t({"",""}),t({"\t\tSHTG A 7 A_ReFire;"}),
t({"",""}),t({"\t\tGoto Ready;"}),
t({"",""}),t({"\tFlash:"}),
t({"",""}),t({"\t\tSHTF A 4 Bright A_Light1;"}),
t({"",""}),t({"\t\tSHTF B 3 Bright A_Light2;"}),
t({"",""}),t({"\t\tGoto LightDone;"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSHOT A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("silent", {
t({"Level.silent"}),
}),
s("SinkMobj", {
t({"SinkMobj"}),
}),
s("SkelFist", {
t({"A_SkelFist "}),i(1),t({"; //https://zdoom.org/wiki/A_SkelFist"}),
}),
s("SkelMissile", {
t({"A_SkelMissile "}),i(1),t({"; //https://zdoom.org/wiki/A_SkelMissile"}),
}),
s("SkelWhoosh", {
t({"A_SkelWhoosh "}),i(1),t({"; //https://zdoom.org/wiki/A_SkelWhoosh"}),
}),
s("skill", {
t({"Level.skill"}),
}),
s("SkullAttack", {
t({"A_SkullAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_SkullAttack"}),
}),
s("SkullColumn", {
t({"SkullColumn"}),
}),
s("SkullPop", {
t({"A_SkullPop "}),i(1),t({"; //https://zdoom.org/wiki/A_SkullPop"}),
}),
s("sky1", {
t({"Level.sky1"}),
}),
s("sky2", {
t({"Level.sky2"}),
}),
s("SkyFog", {
t({"Level.SkyFog"}),
}),
s("SkySpeed1", {
t({"Level.SkySpeed1"}),
}),
s("SkySpeed2", {
t({"Level.SkySpeed2"}),
}),
s("SkyTexture1", {
t({"Level.SkyTexture1"}),
}),
s("SkyTexture2", {
t({"Level.SkyTexture2"}),
}),
s("SmallBloodPool", {
t({"SmallBloodPool"}),
}),
s("SndSeqTotalCtrl", {
t({"Level.SndSeqTotalCtrl"}),
}),
s("Soulsphere", {
t({"Soulsphere"}),
}),
s("Soulsphere", {
t({"class Soulsphere : Health "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+COUNTITEM;"}),
t({"",""}),t({"\t\t+INVENTORY.AUTOACTIVATE;"}),
t({"",""}),t({"\t\t+INVENTORY.ALWAYSPICKUP;"}),
t({"",""}),t({"\t\t+INVENTORY.FANCYPICKUPSOUND;"}),
t({"",""}),t({"\t\tInventory.Amount 100;"}),
t({"",""}),t({"\t\tInventory.MaxAmount 200;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSUPER\";"}),
t({"",""}),t({"\t\tInventory.PickupSound \"misc/p_pkup\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSOUL ABCDCB 6 Bright;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("Sound", {
t({"Sound"}),
}),
s("SoundAlert", {
t({"SoundAlert"}),
}),
s("SoundPitch", {
t({"A_SoundPitch "}),i(1),t({"; //https://zdoom.org/wiki/A_SoundPitch"}),
}),
s("SoundVolume", {
t({"A_SoundVolume "}),i(1),t({"; //https://zdoom.org/wiki/A_SoundVolume"}),
}),
s("source", {
t({"source"}),
}),
s("Spawn", {
t({"Spawn"}),
}),
s("SpawnBlood", {
t({"SpawnBlood"}),
}),
s("SpawnDebris", {
t({"A_SpawnDebris "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnDebris"}),
}),
s("SpawnDirt", {
t({"SpawnDirt"}),
}),
s("SpawnFire", {
t({"SpawnFire"}),
}),
s("SpawnFire", {
t({"class SpawnFire : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHeight 78;"}),
t({"",""}),t({"\t\t+NOBLOCKMAP"}),
t({"",""}),t({"\t\t+NOGRAVITY"}),
t({"",""}),t({"\t\t+ZDOOMTRANS"}),
t({"",""}),t({"\t\tRenderStyle \"Add\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tFIRE ABCDEFGH 4 Bright A_Fire;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("SpawnFly", {
t({"A_SpawnFly "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnFly"}),
}),
s("SpawnHealth", {
t({"SpawnHealth"}),
}),
s("SpawnItem", {
t({"A_SpawnItem "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnItem"}),
}),
s("SpawnItemEx", {
t({"A_SpawnItemEx "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnItemEx"}),
}),
s("SpawnMissile", {
t({"SpawnMissile"}),
}),
s("SpawnMissileAngle", {
t({"SpawnMissileAngle"}),
}),
s("SpawnMissileAngleZ", {
t({"SpawnMissileAngleZ"}),
}),
s("SpawnMissileAngleZSpeed", {
t({"SpawnMissileAngleZSpeed"}),
}),
s("SpawnMissileXYZ", {
t({"SpawnMissileXYZ"}),
}),
s("SpawnMissileZ", {
t({"SpawnMissileZ"}),
}),
s("SpawnMissileZAimed", {
t({"SpawnMissileZAimed"}),
}),
s("SpawnParticle", {
t({"A_SpawnParticle "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnParticle"}),
}),
s("SpawnParticleEx", {
t({"A_SpawnParticleEx "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnParticleEx"}),
}),
s("SpawnPlayerMissile", {
t({"SpawnPlayerMissile"}),
}),
s("SpawnProjectile", {
t({"A_SpawnProjectile "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnProjectile"}),
}),
s("SpawnPuff", {
t({"SpawnPuff"}),
}),
s("SpawnShot", {
t({"SpawnShot"}),
}),
s("SpawnShot", {
t({"class SpawnShot : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tRadius 6;"}),
t({"",""}),t({"\t\tHeight 32;"}),
t({"",""}),t({"\t\tSpeed 10;"}),
t({"",""}),t({"\t\tDamage 3;"}),
t({"",""}),t({"\t\tProjectile;"}),
t({"",""}),t({"\t\t+NOCLIP"}),
t({"",""}),t({"\t\t-ACTIVATEPCROSS"}),
t({"",""}),t({"\t\t+RANDOMIZE"}),
t({"",""}),t({"\t\tSeeSound \"brain/spit\";"}),
t({"",""}),t({"\t\tDeathSound \"brain/cubeboom\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tBOSF A 3 BRIGHT A_SpawnSound;"}),
t({"",""}),t({"\t\tBOSF BCD 3 BRIGHT A_SpawnFly;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("SpawnSound", {
t({"A_SpawnSound "}),i(1),t({"; //https://zdoom.org/wiki/A_SpawnSound"}),
}),
s("SpawnSubMissile", {
t({"SpawnSubMissile"}),
}),
s("SpawnTeleportFog", {
t({"SpawnTeleportFog"}),
}),
s("special", {
t({"Level.special"}),
}),
s("SpecialDamage", {
t({"override int DoSpecialDamage (Actor victim, int damage, Name damagetype)"}),
t({"",""}),t({"  {"}),
t({"",""}),t({"    if (victim != NULL && target != NULL)"}),
t({"",""}),t({"    {"}),
t({"",""}),t({"      "}),i(1),t({""}),
t({"",""}),t({"    }"}),
t({"",""}),t({"    return damage; // 0 damage so shrink ray doesn't hurt monster"}),
t({"",""}),t({"  }"}),
}),
s("Spectre", {
t({"Spectre"}),
}),
s("Spectre", {
t({"class Spectre : Demon "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+SHADOW"}),
t({"",""}),t({"\t\tRenderStyle \"OptFuzzy\";"}),
t({"",""}),t({"\t\tAlpha 0.5;"}),
t({"",""}),t({"\t\tSeeSound \"spectre/sight\";"}),
t({"",""}),t({"\t\tAttackSound \"spectre/melee\";"}),
t({"",""}),t({"\t\tPainSound \"spectre/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"spectre/death\";"}),
t({"",""}),t({"\t\tActiveSound \"spectre/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_SPECTREHIT\";"}),
t({"",""}),t({"\t\tTag \"$FN_SPECTRE\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("speed", {
t({"Level.speed"}),
}),
s("Speed", {
t({"Speed"}),
}),
s("speed2", {
t({"Level.speed2"}),
}),
s("SpiderMastermind", {
t({"SpiderMastermind"}),
}),
s("SpiderMastermind", {
t({"class SpiderMastermind : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 3000;"}),
t({"",""}),t({"\t\tRadius 128;"}),
t({"",""}),t({"\t\tHeight 100;"}),
t({"",""}),t({"\t\tMass 1000;"}),
t({"",""}),t({"\t\tSpeed 12;"}),
t({"",""}),t({"\t\tPainChance 40;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+BOSS"}),
t({"",""}),t({"\t\t+MISSILEMORE"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\t+NORADIUSDMG"}),
t({"",""}),t({"\t\t+DONTMORPH"}),
t({"",""}),t({"\t\t+BOSSDEATH"}),
t({"",""}),t({"\t\t+E3M8BOSS"}),
t({"",""}),t({"\t\t+E4M8BOSS"}),
t({"",""}),t({"\t\tSeeSound \"spider/sight\";"}),
t({"",""}),t({"\t\tAttackSound \"spider/attack\";"}),
t({"",""}),t({"\t\tPainSound \"spider/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"spider/death\";"}),
t({"",""}),t({"\t\tActiveSound \"spider/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_SPIDER\";"}),
t({"",""}),t({"\t\tTag \"$FN_SPIDER\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSPID AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSPID A 3 A_Metal;"}),
t({"",""}),t({"\t\tSPID ABB 3 A_Chase;"}),
t({"",""}),t({"\t\tSPID C 3 A_Metal;"}),
t({"",""}),t({"\t\tSPID CDD 3 A_Chase;"}),
t({"",""}),t({"\t\tSPID E 3 A_Metal;"}),
t({"",""}),t({"\t\tSPID EFF 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSPID A 20 BRIGHT A_FaceTarget;"}),
t({"",""}),t({"\t\tSPID G 4 BRIGHT A_SPosAttackUseAtkSound;"}),
t({"",""}),t({"\t\tSPID H 4 BRIGHT A_SposAttackUseAtkSound;"}),
t({"",""}),t({"\t\tSPID H 1 BRIGHT A_SpidRefire;"}),
t({"",""}),t({"\t\tGoto Missile+1;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSPID I 3;"}),
t({"",""}),t({"\t\tSPID I 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSPID J 20 A_Scream;"}),
t({"",""}),t({"\t\tSPID K 10 A_NoBlocking;"}),
t({"",""}),t({"\t\tSPID LMNOPQR 10;"}),
t({"",""}),t({"\t\tSPID S 30;"}),
t({"",""}),t({"\t\tSPID S -1 A_BossDeath;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("SpidRefire", {
t({"A_SpidRefire "}),i(1),t({"; //https://zdoom.org/wiki/A_SpidRefire"}),
}),
s("splash", {
t({"splash"}),
}),
s("SPosAttack", {
t({"A_SPosAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_SPosAttack"}),
}),
s("SprayDecal", {
t({"A_SprayDecal "}),i(1),t({"; //https://zdoom.org/wiki/A_SprayDecal"}),
}),
s("SpriteOffset", {
t({"A_SpriteOffset "}),i(1),t({"; //https://zdoom.org/wiki/A_SpriteOffset"}),
}),
s("Stalagmite", {
t({"Stalagmite"}),
}),
s("Stalagtite", {
t({"Stalagtite"}),
}),
s("start", {
t({"Level.start"}),
}),
s("StartFire", {
t({"A_StartFire "}),i(1),t({"; //https://zdoom.org/wiki/A_StartFire"}),
}),
s("StartItem", {
t({"StartItem"}),
}),
s("StartSound", {
t({"A_StartSound "}),i(1),t({"; //https://zdoom.org/wiki/A_StartSound"}),
}),
s("StartSoundIfNotSame", {
t({"A_StartSoundIfNotSame "}),i(1),t({"; //https://zdoom.org/wiki/A_StartSoundIfNotSame"}),
}),
s("StartSoundSequence", {
t({"StartSoundSequence"}),
}),
s("StartSoundSequenceID", {
t({"StartSoundSequenceID"}),
}),
s("StartTime", {
t({"Level.StartTime"}),
}),
s("state", {
t({"Level.state"}),
}),
s("State", {
t({"State"}),
}),
s("States", {
t({"States"}),
}),
s("States", {
t({"States"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({":"}),
t({"",""}),t({"    "}),i(2),t({";"}),
t({"",""}),t({"}"}),
}),
s("static", {
t({"static"}),
}),
s("StealthArachnotron", {
t({"StealthArachnotron"}),
}),
s("StealthArchvile", {
t({"StealthArchvile"}),
}),
s("StealthArchVile", {
t({"StealthArchVile"}),
}),
s("StealthBaron", {
t({"StealthBaron"}),
}),
s("StealthCacodemon", {
t({"StealthCacodemon"}),
}),
s("StealthChaingunGuy", {
t({"StealthChaingunGuy"}),
}),
s("StealthDemon", {
t({"StealthDemon"}),
}),
s("StealthDoomImp", {
t({"StealthDoomImp"}),
}),
s("StealthFatso", {
t({"StealthFatso"}),
}),
s("StealthHellKnight", {
t({"StealthHellKnight"}),
}),
s("StealthRevenant", {
t({"StealthRevenant"}),
}),
s("StealthShotgunGuy", {
t({"StealthShotgunGuy"}),
}),
s("StealthZombieman", {
t({"StealthZombieman"}),
}),
s("StealthZombieMan", {
t({"StealthZombieMan"}),
}),
s("StealthZombieMan", {
t({" "}),i(1),t({""}),
t({"",""}),t({"class StealthZombieMan : ZombieMan"}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\t+STEALTH"}),
t({"",""}),t({"\t\tRenderStyle \"Translucent\";"}),
t({"",""}),t({"\t\tAlpha 0;"}),
t({"",""}),t({"\t\tObituary \"$OB_STEALTHZOMBIE\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
t({"",""}),t({""}),
}),
s("Stimpack", {
t({"Stimpack"}),
}),
s("Stimpack", {
t({"class Stimpack : Health "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Amount 10;"}),
t({"",""}),t({"\t\tInventory.PickupMessage \"$GOTSTIM\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSTIM A -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
t({"",""}),t({""}),
}),
s("Stop", {
t({"A_Stop "}),i(1),t({"; //https://zdoom.org/wiki/A_Stop"}),
}),
s("StopAllSounds", {
t({"A_StopAllSounds "}),i(1),t({"; //https://zdoom.org/wiki/A_StopAllSounds"}),
}),
s("StopSound", {
t({"A_StopSound "}),i(1),t({"; //https://zdoom.org/wiki/A_StopSound"}),
}),
s("StopSoundEx", {
t({"A_StopSoundEx "}),i(1),t({"; //https://zdoom.org/wiki/A_StopSoundEx"}),
}),
s("StopSounds", {
t({"A_StopSounds "}),i(1),t({"; //https://zdoom.org/wiki/A_StopSounds"}),
}),
s("StopSoundSequence", {
t({"StopSoundSequence"}),
}),
s("string", {
t({"string"}),
}),
s("subclass", {
t({"subclass"}),
}),
s("SuckTime", {
t({"Level.SuckTime"}),
}),
s("SuperShotgun", {
t({"SuperShotgun"}),
}),
s("SuperShotgun", {
t({"sed \"1 s|$| "}),i(1),t({"|\" "}),
}),
s("SwapTeleFog", {
t({"A_SwapTeleFog "}),i(1),t({"; //https://zdoom.org/wiki/A_SwapTeleFog"}),
}),
s("tag", {
t({"Level.tag"}),
}),
s("Tag", {
t({"Tag"}),
}),
s("TakeFromChildren", {
t({"A_TakeFromChildren "}),i(1),t({"; //https://zdoom.org/wiki/A_TakeFromChildren"}),
}),
s("TakeFromSiblings", {
t({"A_TakeFromSiblings "}),i(1),t({"; //https://zdoom.org/wiki/A_TakeFromSiblings"}),
}),
s("TakeFromTarget", {
t({"A_TakeFromTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_TakeFromTarget"}),
}),
s("TakeInventory", {
t({"TakeInventory"}),
}),
s("TallGreenColumn", {
t({"TallGreenColumn"}),
}),
s("TallRedColumn", {
t({"TallRedColumn"}),
}),
s("target", {
t({"target"}),
}),
s("targZOfs", {
t({"targZOfs"}),
}),
s("TeamDamage", {
t({"Level.TeamDamage"}),
}),
s("TechLamp", {
t({"TechLamp"}),
}),
s("TechLamp2", {
t({"TechLamp2"}),
}),
s("TechPillar", {
t({"TechPillar"}),
}),
s("telefrag", {
t({"telefrag"}),
}),
s("Teleport", {
t({"Teleport"}),
}),
s("TeleportMove", {
t({"TeleportMove"}),
}),
s("TerrainDef", {
t({"TerrainDef"}),
}),
s("TestMobjLocation", {
t({"TestMobjLocation"}),
}),
s("TestMobjZ", {
t({"TestMobjZ"}),
}),
s("TEXTCOLOR_BLACK", {
t({"TEXTCOLOR_BLACK"}),
}),
s("TEXTCOLOR_BLUE", {
t({"TEXTCOLOR_BLUE"}),
}),
s("TEXTCOLOR_BOLD", {
t({"TEXTCOLOR_BOLD"}),
}),
s("TEXTCOLOR_BRICK", {
t({"TEXTCOLOR_BRICK"}),
}),
s("TEXTCOLOR_BROWN", {
t({"TEXTCOLOR_BROWN"}),
}),
s("TEXTCOLOR_CHAT", {
t({"TEXTCOLOR_CHAT"}),
}),
s("TEXTCOLOR_CREAM", {
t({"TEXTCOLOR_CREAM"}),
}),
s("TEXTCOLOR_CYAN", {
t({"TEXTCOLOR_CYAN"}),
}),
s("TEXTCOLOR_DARKBROWN", {
t({"TEXTCOLOR_DARKBROWN"}),
}),
s("TEXTCOLOR_DARKGRAY", {
t({"TEXTCOLOR_DARKGRAY"}),
}),
s("TEXTCOLOR_DARKGREEN", {
t({"TEXTCOLOR_DARKGREEN"}),
}),
s("TEXTCOLOR_DARKRED", {
t({"TEXTCOLOR_DARKRED"}),
}),
s("TEXTCOLOR_FIRE", {
t({"TEXTCOLOR_FIRE"}),
}),
s("TEXTCOLOR_GOLD", {
t({"TEXTCOLOR_GOLD"}),
}),
s("TEXTCOLOR_GRAY", {
t({"TEXTCOLOR_GRAY"}),
}),
s("TEXTCOLOR_GREEN", {
t({"TEXTCOLOR_GREEN"}),
}),
s("TEXTCOLOR_GREY", {
t({"TEXTCOLOR_GREY"}),
}),
s("TEXTCOLOR_ICE", {
t({"TEXTCOLOR_ICE"}),
}),
s("TEXTCOLOR_LIGHTBLUE", {
t({"TEXTCOLOR_LIGHTBLUE"}),
}),
s("TEXTCOLOR_NORMAL", {
t({"TEXTCOLOR_NORMAL"}),
}),
s("TEXTCOLOR_OLIVE", {
t({"TEXTCOLOR_OLIVE"}),
}),
s("TEXTCOLOR_ORANGE", {
t({"TEXTCOLOR_ORANGE"}),
}),
s("TEXTCOLOR_PURPLE", {
t({"TEXTCOLOR_PURPLE"}),
}),
s("TEXTCOLOR_RED", {
t({"TEXTCOLOR_RED"}),
}),
s("TEXTCOLOR_SAPPHIRE", {
t({"TEXTCOLOR_SAPPHIRE"}),
}),
s("TEXTCOLOR_TAN", {
t({"TEXTCOLOR_TAN"}),
}),
s("TEXTCOLOR_TEAL", {
t({"TEXTCOLOR_TEAL"}),
}),
s("TEXTCOLOR_TEAMCHAT", {
t({"TEXTCOLOR_TEAMCHAT"}),
}),
s("TEXTCOLOR_UNTRANSLATED", {
t({"TEXTCOLOR_UNTRANSLATED"}),
}),
s("TEXTCOLOR_WHITE", {
t({"TEXTCOLOR_WHITE"}),
}),
s("TEXTCOLOR_YELLOW", {
t({"TEXTCOLOR_YELLOW"}),
}),
s("thing", {
t({"thing"}),
}),
s("ThrowGrenade", {
t({"A_ThrowGrenade "}),i(1),t({"; //https://zdoom.org/wiki/A_ThrowGrenade"}),
}),
s("thrust", {
t({"thrust"}),
}),
s("Thrust", {
t({"Thrust"}),
}),
s("tid", {
t({"Level.tid"}),
}),
s("Time", {
t({"Level.Time"}),
}),
s("TNT1", {
t({"TNT1 A 0 "}),
}),
s("TorchTree", {
t({"TorchTree"}),
}),
s("TossGib", {
t({"A_TossGib "}),i(1),t({"; //https://zdoom.org/wiki/A_TossGib"}),
}),
s("TossItem", {
t({"TossItem"}),
}),
s("TotalItems", {
t({"Level.TotalItems"}),
}),
s("TotalMonsters", {
t({"Level.TotalMonsters"}),
}),
s("totals", {
t({"Level.totals"}),
}),
s("TotalSecrets", {
t({"Level.TotalSecrets"}),
}),
s("TotalTime", {
t({"Level.TotalTime"}),
}),
s("TraceBleed", {
t({"TraceBleed"}),
}),
s("TraceBleedAngle", {
t({"TraceBleedAngle"}),
}),
s("Tracer", {
t({"A_Tracer "}),i(1),t({"; //https://zdoom.org/wiki/A_Tracer"}),
}),
s("Tracer2", {
t({"A_Tracer2 "}),i(1),t({"; //https://zdoom.org/wiki/A_Tracer2"}),
}),
s("TransferPointer", {
t({"A_TransferPointer "}),i(1),t({"; //https://zdoom.org/wiki/A_TransferPointer"}),
}),
s("Translation", {
t({"Translation"}),
}),
s("Translation", {
t({"Translation \""}),i(1),t({":"}),i(2),t({"="}),i(3),t({":"}),i(4),t({"\";"}),
}),
s("TriggerPainChance", {
t({"TriggerPainChance"}),
}),
s("TroopAttack", {
t({"A_TroopAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_TroopAttack"}),
}),
s("TryMove", {
t({"TryMove"}),
}),
s("TryWalk", {
t({"TryWalk"}),
}),
s("TurretLook", {
t({"A_TurretLook "}),i(1),t({"; //https://zdoom.org/wiki/A_TurretLook"}),
}),
s("two_dimension", {
t({"two_dimension"}),
}),
s("UnHideThing", {
t({"A_UnHideThing "}),i(1),t({"; //https://zdoom.org/wiki/A_UnHideThing"}),
}),
s("UnlinkFromWorld", {
t({"UnlinkFromWorld"}),
}),
s("UnsetFloat", {
t({"A_UnsetFloat "}),i(1),t({"; //https://zdoom.org/wiki/A_UnsetFloat"}),
}),
s("UnSetFloorClip", {
t({"A_UnSetFloorClip "}),i(1),t({"; //https://zdoom.org/wiki/A_UnSetFloorClip"}),
}),
s("UnSetInvulnerable", {
t({"A_UnSetInvulnerable "}),i(1),t({"; //https://zdoom.org/wiki/A_UnSetInvulnerable"}),
}),
s("UnSetReflective", {
t({"A_UnSetReflective "}),i(1),t({"; //https://zdoom.org/wiki/A_UnSetReflective"}),
}),
s("UnSetReflectiveInvulnerable", {
t({"A_UnSetReflectiveInvulnerable "}),i(1),t({"; //https://zdoom.org/wiki/A_UnSetReflectiveInvulnerable"}),
}),
s("UnSetShootable", {
t({"A_UnSetShootable "}),i(1),t({"; //https://zdoom.org/wiki/A_UnSetShootable"}),
}),
s("UnsetSolid", {
t({"A_UnsetSolid "}),i(1),t({"; //https://zdoom.org/wiki/A_UnsetSolid"}),
}),
s("UpdateWaterLevel", {
t({"UpdateWaterLevel"}),
}),
s("updown", {
t({"updown"}),
}),
s("UsePuzzleItem", {
t({"UsePuzzleItem"}),
}),
s("Vec2Angle", {
t({"Vec2Angle"}),
}),
s("Vec2Offset", {
t({"Vec2Offset"}),
}),
s("Vec2OffsetZ", {
t({"Vec2OffsetZ"}),
}),
s("Vec2To", {
t({"Vec2To"}),
}),
s("Vec3Angle", {
t({"Vec3Angle"}),
}),
s("Vec3Offset", {
t({"Vec3Offset"}),
}),
s("Vec3To", {
t({"Vec3To"}),
}),
s("Vector2", {
t({"Vector2"}),
}),
s("vector3", {
t({"Level.vector3"}),
}),
s("Vector3", {
t({"Vector3"}),
}),
s("Vel3DFromAngle", {
t({"Vel3DFromAngle"}),
}),
s("VelFromAngle", {
t({"VelFromAngle"}),
}),
s("VelIntercept", {
t({"VelIntercept"}),
}),
s("victim", {
t({"victim"}),
}),
s("VileAttack", {
t({"A_VileAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_VileAttack"}),
}),
s("VileChase", {
t({"A_VileChase "}),i(1),t({"; //https://zdoom.org/wiki/A_VileChase"}),
}),
s("VileStart", {
t({"A_VileStart "}),i(1),t({"; //https://zdoom.org/wiki/A_VileStart"}),
}),
s("VileTarget", {
t({"A_VileTarget "}),i(1),t({"; //https://zdoom.org/wiki/A_VileTarget"}),
}),
s("volume", {
t({"volume"}),
}),
s("vrange", {
t({"vrange"}),
}),
s("Wander", {
t({"A_Wander "}),i(1),t({"; //https://zdoom.org/wiki/A_Wander"}),
}),
s("Warp", {
t({"A_Warp "}),i(1),t({"; //https://zdoom.org/wiki/A_Warp"}),
}),
s("Weapon_AmmoGive1", {
t({"Weapon.AmmoGive1 "}),i(1),t({";"}),
}),
s("Weapon_AmmoGive2", {
t({"Weapon.AmmoGive2 "}),i(1),t({";"}),
}),
s("Weapon_AmmoGive", {
t({"Weapon.AmmoGive "}),i(1),t({";"}),
}),
s("Weapon_AmmoType1", {
t({"Weapon.AmmoType1 "}),i(1),t({";"}),
}),
s("Weapon_AmmoType2", {
t({"Weapon.AmmoType2 "}),i(1),t({";"}),
}),
s("Weapon_AmmoType", {
t({"Weapon.AmmoType "}),i(1),t({";"}),
}),
s("Weapon_AmmoUse1", {
t({"Weapon.AmmoUse1 "}),i(1),t({";"}),
}),
s("Weapon_AmmoUse2", {
t({"Weapon.AmmoUse2 "}),i(1),t({";"}),
}),
s("Weapon_AmmoUse", {
t({"Weapon.AmmoUse "}),i(1),t({";"}),
}),
s("Weapon_BobPivot3D", {
t({"Weapon.BobPivot3D "}),i(1),t({";"}),
}),
s("Weapon_BobRangeX", {
t({"Weapon.BobRangeX "}),i(1),t({";"}),
}),
s("Weapon_BobRangeY", {
t({"Weapon.BobRangeY "}),i(1),t({";"}),
}),
s("Weapon_BobSpeed", {
t({"Weapon.BobSpeed "}),i(1),t({";"}),
}),
s("Weapon_BobStyle", {
t({"Weapon.BobStyle "}),i(1),t({";"}),
}),
s("Weapon_DefaultKickBack", {
t({"Weapon.DefaultKickBack "}),i(1),t({";"}),
}),
s("Weapon_KickBack", {
t({"Weapon.KickBack "}),i(1),t({";"}),
}),
s("Weapon_LookScale", {
t({"Weapon.LookScale "}),i(1),t({";"}),
}),
s("Weapon_MinSelectionAmmo1", {
t({"Weapon.MinSelectionAmmo1 "}),i(1),t({";"}),
}),
s("Weapon_MinSelectionAmmo2", {
t({"Weapon.MinSelectionAmmo2 "}),i(1),t({";"}),
}),
s("WeaponOffset", {
t({"A_WeaponOffset "}),i(1),t({"; //https://zdoom.org/wiki/A_WeaponOffset"}),
}),
s("WeaponReady", {
t({"A_WeaponReady "}),i(1),t({"; //https://zdoom.org/wiki/A_WeaponReady"}),
}),
s("Weapon_ReadySound", {
t({"Weapon.ReadySound "}),i(1),t({";"}),
}),
s("Weapon_SelectionOrder", {
t({"Weapon.SelectionOrder "}),i(1),t({";"}),
}),
s("Weapon_SisterWeapon", {
t({"Weapon.SisterWeapon "}),i(1),t({";"}),
}),
s("WeaponSlot", {
t({"WeaponSlot"}),
}),
s("Weapon_SlotNumber", {
t({"Weapon.SlotNumber "}),i(1),t({";"}),
}),
s("Weapon_SlotPriority", {
t({"Weapon.SlotPriority "}),i(1),t({";"}),
}),
s("WeaponSlotPriority", {
t({"Weapon.SlotPriority "}),i(1),t({";"}),
}),
s("weapon_slot", {
t({"Weapon.slotnumber "}),i(1),t({";"}),
}),
s("Weapons", {
t({"Weapons "}),i(1),t({";"}),
}),
s("Weapon_UpSound", {
t({"Weapon.UpSound "}),i(1),t({";"}),
}),
s("Weapon_WeaponScaleX", {
t({"Weapon.WeaponScaleX "}),i(1),t({";"}),
}),
s("Weapon_WeaponScaleY", {
t({"Weapon.WeaponScaleY "}),i(1),t({";"}),
}),
s("Weapon_YAdjust", {
t({"Weapon.YAdjust "}),i(1),t({";"}),
}),
s("Weave", {
t({"A_Weave "}),i(1),t({"; //https://zdoom.org/wiki/A_Weave"}),
}),
s("whattoplay", {
t({"whattoplay"}),
}),
s("WolfAttack", {
t({"A_WolfAttack "}),i(1),t({"; //https://zdoom.org/wiki/A_WolfAttack"}),
}),
s("WolfensteinSS", {
t({"WolfensteinSS"}),
}),
s("WolfensteinSS", {
t({"class WolfensteinSS : Actor "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 50;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 170;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"wolfss/sight\";"}),
t({"",""}),t({"\t\tPainSound \"wolfss/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"wolfss/death\";"}),
t({"",""}),t({"\t\tActiveSound \"wolfss/active\";"}),
t({"",""}),t({"\t\tAttackSound \"wolfss/attack\";"}),
t({"",""}),t({"\t\tObituary \"$OB_WOLFSS\";"}),
t({"",""}),t({"\t\tTag \"$FN_WOLFSS\";"}),
t({"",""}),t({"\t\tDropitem \"Clip\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tSSWV AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tSSWV AABBCCDD 3 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tSSWV E 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tSSWV F 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tSSWV G 4 BRIGHT A_CPosAttack;"}),
t({"",""}),t({"\t\tSSWV F 6 A_FaceTarget;"}),
t({"",""}),t({"\t\tSSWV G 4 BRIGHT A_CPosAttack;"}),
t({"",""}),t({"\t\tSSWV F 1 A_CPosRefire;"}),
t({"",""}),t({"\t\tGoto Missile+1;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tSSWV H 3;"}),
t({"",""}),t({"\t\tSSWV H 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tSSWV I 5;"}),
t({"",""}),t({"\t\tSSWV J 5 A_Scream;"}),
t({"",""}),t({"\t\tSSWV K 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tSSWV L 5;"}),
t({"",""}),t({"\t\tSSWV M -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tSSWV N 5 ;"}),
t({"",""}),t({"\t\tSSWV O 5 A_XScream;"}),
t({"",""}),t({"\t\tSSWV P 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tSSWV QRSTU 5;"}),
t({"",""}),t({"\t\tSSWV V -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tSSWV M 5;"}),
t({"",""}),t({"\t\tSSWV LKJI 5;"}),
t({"",""}),t({"\t\tGoto See ;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("WorldLoaded", {
t({"WorldLoaded"}),
}),
s("WorldLoaded", {
t({"override void WorldLoaded (WorldEvent e)"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("WorldThingDied", {
t({"WorldThingDied"}),
}),
s("WorldThingDied", {
t({"override void WorldThingDied(WorldEvent e) "}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("WorldThingSpawned", {
t({"WorldThingSpawned"}),
}),
s("WorldThingSpawned", {
t({"override void WorldThingSpawned (WorldEvent e)"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("WorldTick", {
t({"WorldTick"}),
}),
s("WorldTick", {
t({"override void WorldTick()"}),
t({"",""}),t({"{"}),
t({"",""}),t({"  "}),i(1),t({""}),
t({"",""}),t({"}"}),
}),
s("XDeath", {
t({"XDeath"}),
}),
s("XScream", {
t({"A_XScream "}),i(1),t({"; //https://zdoom.org/wiki/A_XScream"}),
}),
s("YellowCard", {
t({"YellowCard"}),
}),
s("YellowCard", {
t({"class YellowCard : DoomKey "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTYELWCARD\";"}),
t({"",""}),t({"\t\tInventory.Icon \"STKEYS1\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tYKEY A 10;"}),
t({"",""}),t({"\t\tYKEY B 10 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("YellowSkull", {
t({"YellowSkull"}),
}),
s("YellowSkull", {
t({"class YellowSkull : DoomKey "}),i(1),t({""}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tInventory.Pickupmessage \"$GOTYELWSKUL\";"}),
t({"",""}),t({"\t\tInventory.Icon \"STKEYS4\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"\tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tYSKU A 10;"}),
t({"",""}),t({"\t\tYSKU B 10 bright;"}),
t({"",""}),t({"\t\tloop;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("z_ofs", {
t({"z_ofs"}),
}),
s("Zombieman", {
t({"Zombieman"}),
}),
s("ZombieMan", {
t({"ZombieMan"}),
}),
s("zombie", {
t({"class "}),i(1),t({" : Actor Replaces ZombieMan "}),
t({"",""}),t({"{"}),
t({"",""}),t({"\tDefault"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\t\tHealth 20;"}),
t({"",""}),t({"\t\tRadius 20;"}),
t({"",""}),t({"\t\tHeight 56;"}),
t({"",""}),t({"\t\tSpeed 8;"}),
t({"",""}),t({"\t\tPainChance 200;"}),
t({"",""}),t({"\t\tMonster;"}),
t({"",""}),t({"\t\t+FLOORCLIP"}),
t({"",""}),t({"\t\tSeeSound \"grunt/sight\";"}),
t({"",""}),t({"\t\tAttackSound \"grunt/attack\";"}),
t({"",""}),t({"\t\tPainSound \"grunt/pain\";"}),
t({"",""}),t({"\t\tDeathSound \"grunt/death\";"}),
t({"",""}),t({"\t\tActiveSound \"grunt/active\";"}),
t({"",""}),t({"\t\tObituary \"$OB_ZOMBIE\";"}),
t({"",""}),t({"\t\tTag \"$FN_ZOMBIE\";"}),
t({"",""}),t({"\t\tDropItem \"Clip\";"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({" \tStates"}),
t({"",""}),t({"\t{"}),
t({"",""}),t({"\tSpawn:"}),
t({"",""}),t({"\t\tPOSS AB 10 A_Look;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tSee:"}),
t({"",""}),t({"\t\tPOSS AABBCCDD 4 A_Chase;"}),
t({"",""}),t({"\t\tLoop;"}),
t({"",""}),t({"\tMissile:"}),
t({"",""}),t({"\t\tPOSS E 10 A_FaceTarget;"}),
t({"",""}),t({"\t\tPOSS F 8 A_PosAttack;"}),
t({"",""}),t({"\t\tPOSS E 8;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tPain:"}),
t({"",""}),t({"\t\tPOSS G 3;"}),
t({"",""}),t({"\t\tPOSS G 3 A_Pain;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\tDeath:"}),
t({"",""}),t({"\t\tPOSS H 5;"}),
t({"",""}),t({"\t\tPOSS I 5 A_Scream;"}),
t({"",""}),t({"\t\tPOSS J 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tPOSS K 5;"}),
t({"",""}),t({"\t\tPOSS L -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tXDeath:"}),
t({"",""}),t({"\t\tPOSS M 5;"}),
t({"",""}),t({"\t\tPOSS N 5 A_XScream;"}),
t({"",""}),t({"\t\tPOSS O 5 A_NoBlocking;"}),
t({"",""}),t({"\t\tPOSS PQRST 5;"}),
t({"",""}),t({"\t\tPOSS U -1;"}),
t({"",""}),t({"\t\tStop;"}),
t({"",""}),t({"\tRaise:"}),
t({"",""}),t({"\t\tPOSS K 5;"}),
t({"",""}),t({"\t\tPOSS JIH 5;"}),
t({"",""}),t({"\t\tGoto See;"}),
t({"",""}),t({"\t}"}),
t({"",""}),t({"}"}),
}),
s("ZoomFactor", {
t({"A_ZoomFactor "}),i(1),t({"; //https://zdoom.org/wiki/A_ZoomFactor"}),
}),
})
