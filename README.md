
# fbk-neovim

Copyright Kris Occhipinti 2024-03-22

(https://filmsbykris.com)

License GPLv3

# Dependencies
```bash 
sudo apt install -y git jq wget unzip npm tmux
```

# install - recommended you run this in a tmux session
```bash
bash -c "$(wget 'https://gitlab.com/metalx1000/fbk-neovim/-/raw/master/setup.sh' -O -)"
```

# Optional

```bash
#open instance
nvim --listen /tmp/pipe
```

```bash
#open second instance
nvim --server /tmp/pipe --remote-send ":Mason<CR>"
lsps=(html-lsp json-lsp cpptools prettierd css-lsp)

# removed bash-language-server because it was executing code while typing
for lsp in "${lsps[@]}"
do
  echo "$lsp"
  nvim --server /tmp/pipe --remote-send ":MasonInstall $lsp<CR>"
done
```
