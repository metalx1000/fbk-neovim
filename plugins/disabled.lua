--- Copy this to ~/.config/nvim/lua/plugins/disabled.lua
return {
  --disable mini.pairs
  { "echasnovski/mini.pairs", enabled = false },
  { "rcarriga/nvim-notify", enabled = false },
}
