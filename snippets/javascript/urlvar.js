function urlvar(v){
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  let value = urlParams.get(v);
  return value;
}
