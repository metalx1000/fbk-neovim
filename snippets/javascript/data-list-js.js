<label for="[[1]]">[[2]]:</label>
<input list="[[3]]" id="[-1-]" name="[-1-]" />

<datalist id="[-3-]">
</datalist>

<script>
  function builddata(){
    let list=document.querySelector("#[-3-]");
    let dataitems = [ "[[4]]","[[5]]" ];
    dataitems.forEach(function(item){
      list.innerHTML += `<option value="${item}">${item}</option>`;
    });
  }

  builddata();
</script>
