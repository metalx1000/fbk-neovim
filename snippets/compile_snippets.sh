#!/bin/bash
######################################################################
#Copyright (C) 2024 Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see
#<http: //www.gnu.org/licenses / >.
######################################################################

output="$HOME/.config/nvim/lua/fbk/snippets.lua"

function get_scripts() {
  for dir in */; do
    d="$(echo $dir | tr -d '/')"
    lsdir
    # add javascript and css to html - there is probably a better way then this
    [[ "$d" == "javascript" ]] && d="html" && lsdir
    [[ "$d" == "css" ]] && d="html" && lsdir
  done
}

function lsdir() {
  echo "ls.add_snippets(\"$d\", {"
  ls $dir | while read snippet; do
    s="$(echo $snippet | cut -d\. -f1)"
    echo "s(\"$s\", {"
    #cat "${dir}${snippet}" | sed 's|"|\\\"|g' | while IFS= read -r l; do echo "t({\"\",\"$l\"}),"; done
    #cat "${dir}${snippet}" | jq -Ra . | while IFS= read -r l; do echo "t({$l,\"\"}),"; done
    cat "${dir}${snippet}" | jq -Ra . | sed 's/^/t({/g;s/$/}),/g' | sed ':a;N;$!ba;s/\n/\nt({"",""}),/g'
    echo "}),"
  done
  echo "})"
}

function compile() {
  cat head.lua
  echo -e "\n\n"
  #replace variables with lua script variables
  #get_scripts | sed "s/\[\[[0-9]\]]/\"}),i(&),t({\"/g"
  get_scripts | sed 's/\[\[0\]]/"}),i(0),t({"/g' |
    sed 's/\[\[1\]]/"}),i(1),t({"/g' |
    sed 's/\[\[2\]]/"}),i(2),t({"/g' |
    sed 's/\[\[3\]]/"}),i(3),t({"/g' |
    sed 's/\[\[4\]]/"}),i(4),t({"/g' |
    sed 's/\[\[5\]]/"}),i(5),t({"/g' |
    sed 's/\[-0-]/"}),rep(0),t({"/g' |
    sed 's/\[-1-]/"}),rep(1),t({"/g' |
    sed 's/\[-2-]/"}),rep(2),t({"/g' |
    sed 's/\[-3-]/"}),rep(3),t({"/g' |
    sed 's/\[-4-]/"}),rep(4),t({"/g' |
    sed 's/\[-5-]/"}),rep(5),t({"/g' |
    sed 's/\[\[year\]]/"}),f(year),t({"/g'
}

compile >"$output"
#nvim "$output"
