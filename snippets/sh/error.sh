# fatal uses SIGUSR1 to allow clean fatal errors
trap "exit 1" 10
PROC=$$

function error() {
	red=$(echo -en "\e[31m")
	normal=$(echo -en "\e[0m")

	echo -e "${red}$@${normal}" >&2
	#  exit 1
	kill -10 $PROC
}
