function line() {
	if [[ $# < 1 ]]; then
		l="="
	else
		l="${1:0:1}"
	fi

	eval printf %.0s$l '{1..'"${COLUMNS:-$(tput cols)}"\}
	echo ""
}
