#!/bin/bash
######################################################################
#Copyright (C) 2024 Kris Occhipinti
#https://filmsbykris.com
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################
# fbk-neovim

# Dependencies
deps=(git jq unzip npm fzf tmux)
for d in ${deps[@]}; do
  which "$d" || dep="install"
done
[[ "$dep" ]] && sudo apt install -y git jq wget unzip npm fzf tmux ripgrep

function get_neovim() {
  #wget "https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz" -O /tmp/nvim-linux64.tar.gz
  #wget "https://github.com/neovim/neovim/releases/download/v0.9.5/nvim-linux64.tar.gz" -O /tmp/nvim-linux64.tar.gz
  wget "https://github.com/neovim/neovim/releases/latest/download/nvim-linux-x86_64.tar.gz" -O /tmp/nvim-linux64.tar.gz
  sudo tar xzvf /tmp/nvim-linux64.tar.gz -C /usr/share
  sudo ln -s "/usr/share/nvim-linux-x86_64/bin/nvim" /usr/local/bin/nvim

  read -p "Would you like to link vim to nvim? [Y/n]" link
  [[ "$link" != "n" ]] && sudo ln -s "/usr/share/nvim-linux-x86_64/bin/nvim" /usr/local/bin/vim
}

[[ $(which nvim) ]] || get_neovim
# BACKUP Current vim config

fresh="$(echo -e "Do Nothing\nBackup\nRemove" | fzf --prompt="Backup or Remove Current Configs? ")"

if [[ "$fresh" == "Backup" ]]; then
  # required
  mv ~/.config/nvim{,.bak}

  # optional but recommended
  mv ~/.local/share/nvim{,.bak}
  mv ~/.local/state/nvim{,.bak}
  mv ~/.cache/nvim{,.bak}
elif [[ "$fresh" == "Remove" ]]; then
  # required
  rm -fr ~/.config/nvim

  # optional but recommended
  rm -fr ~/.local/share/nvim
  rm -fr ~/.local/state/nvim
  rm -fr ~/.cache/nvim
fi
# Install
# Lazy Vim
git clone https://github.com/LazyVim/starter ~/.config/nvim
rm -rf ~/.config/nvim/.git

echo "Hello $USER"

# FBK setup
[[ "$USER" == "metalx1000" ]] && git clone git@gitlab.com:metalx1000/fbk-neovim.git ~/.config/nvim/lua/fbk
[[ ! -d ~/.config/nvim/lua/fbk ]] && git clone https://gitlab.com/metalx1000/fbk-neovim.git ~/.config/nvim/lua/fbk

#disable start checks
sed -i 's/checker = { enabled = true },/checker = { enabled = false },/g' ~/.config/nvim/lua/config/lazy.lua

grep 'require("fbk")' ~/.config/nvim/init.lua || echo 'require("fbk")' >>~/.config/nvim/init.lua
#grep 'require("fbk.snippets")' ~/.config/nvim/init.lua || echo 'require("fbk.snippets")' >>~/.config/nvim/init.lua

if [[ ! -f "$HOME/.local/share/fonts/SymbolsNerdFont-Regular.ttf" ]]; then
  # fix symbol fonts
  mkdir -p $HOME/.local/share/fonts
  wget -c "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/NerdFontsSymbolsOnly.zip" -P /tmp
  unzip /tmp/NerdFontsSymbolsOnly.zip -d $HOME/.local/share/fonts
  fc-cache -f -v
  # Restart Terminal after installing fonts
fi

skip="true"
source "$HOME/.config/nvim/lua/fbk/fbkvim_update.sh"

# cp -v ~/.config/nvim/lua/fbk/plugins/* ~/.config/nvim/lua/plugins/
# mkdir -p ~/.config/nvim/syntax/
# cp -v ~/.config/nvim/lua/fbk/syntax/* ~/.config/nvim/syntax/
# cp -v ~/.config/nvim/lua/fbk/options.lua ~/.config/nvim/lua/config/
# ln -s ~/.config/nvim/lua/fbk/spell ~/.config/nvim/spell

# Add update alias
a="fbkvim_update"
c="$HOME/.config/nvim/lua/fbk/fbkvim_update.sh"

sh=(zsh bash)
for shell in "${sh[@]}"; do
  rc="$HOME/.${shell}rc"
  [[ -f $rc ]] && (
    sed -i "/$a/d" "$rc"
    echo "alias $a='$c'" >>"$rc"
  )
done

cp -v $HOME/.config/nvim/lua/fbk/lazyvim.json $HOME/.config/nvim/lazyvim.json
nvim
