#!/bin/bash
######################################################################
#Copyright (C) 2024  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

[[ $skip ]] || git -C ~/.config/nvim/lua/fbk pull || exit 1

#cp -v ~/.config/nvim/lua/fbk/plugins/* ~/.config/nvim/lua/plugins/

rm $HOME/.config/nvim/lua/plugins -fr
ln -s $HOME/.config/nvim/lua/fbk/plugins $HOME/.config/nvim/lua/

rm ~/.config/nvim/syntax -fr
ln -s ~/.config/nvim/lua/fbk/syntax ~/.config/nvim/syntax

rm ~/.config/nvim/lua/config/options.lua
ln -s ~/.config/nvim/lua/fbk/options.lua ~/.config/nvim/lua/config/options.lua

ln -s ~/.config/nvim/lua/fbk/spell ~/.config/nvim/spell
