-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
vim.opt.clipboard = ""

local noice_status, noice = pcall(require, "noice")

if not noice_status then
  return
end

noice.setup({
  lsp = {
    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
  },
  -- have messages (e.g. file saved)
  presets = {
    inc_rename = true,
    command_palette = true,
  },
  messages = { enabled = false },
  views = {
    confirm = {
      position = {
        row = "50%",
      },
    },
  },
})
